#ifndef GAMECLASES_H
#define GAMECLASES_H
#include "BaseDBClases.h"

class Gaction;
class GAI;
class Gattack;
class Gbuild;
class GbuiList;
class GcityInf;
class GDynUpgr;
class Gimmu;
class GimmuC;
class GItem;
class GLabi;
class GleaUpg;
class GLmark;
class Glord;
class GMabi;
class Gmodif;
class GmodifL;
class Grace;
class GspellR;
class GSpell;
class GSubRace;
class GTileDBI;
class Gtransf;
class Gunit;
class GVars;
class Laction;
class LaiAtt;
class LAiMsg;
class LAiSpell;
class LattC;
class LAttR;
class LattS;
class Lbuild;
class Ldiff;
class LDthAnim;
class LEvCond;
class LEvEffct;
class LFort;
class Lground;
class LImmune;
class LleadA;
class LleadC;
class LLMCat;
class Llord;
class LmagItm;
class LmodifE;
class LModifS;
class LOrder;
class Lrace;
class Lres;
class Lsite;
class Lspell;
class LSplPhas;
class LSubRace;
class Lterrain;
class Ltreaty;
class LunitB;
class LunitC;
class TAiMsg;
class Tglobal;
class Tleader;
class Tplayer;


class Gaction: public GameData
{
public:
    static QString typeName() {return "Gaction";}
    QString key() const {return action_id;}

    QString action_id;
    int category;
    int chance;
    StringLink<Tglobal> name_txt;
    QString desc_txt;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(action_id, "ACTION_ID", adapter, model, row);
        readValue(category, "CATEGORY", adapter, model, row);
        readValue(chance, "CHANCE", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(action_id, "ACTION_ID", adapter, model, row);
        writeValue(category, "CATEGORY", adapter, model, row);
        writeValue(chance, "CHANCE", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
    }
};

class GAI: public GameData
{
public:
    static QString typeName() {return "GAI";}
    QString key() const {return QString::number(category);}

    int category;
    QString name_txt;
    QString desc_txt;
    int b_units;
    int b_leaders;
    int b_cap;
    int b_cities;
    int b_spell_c;
    int b_spell_r;
    int b_iso;
    int l_min;
    int l_max;
    int pct_fightr;
    int t_min;
    int t_max;
    int pct_thief;
    int r_min;
    int r_max;
    int pct_rod;
    int i_cities_n;
    int i_cities_r;
    int i_cities_p;
    int i_stacks_n;
    int i_stacks_r;
    int i_stacks_p;
    int i_bags;
    int i_ruins;
    int i_towers;
    int i_merchant;
    int i_h_units;
    int i_h_mercs;
    int i_heal;
    int i_p_rod;
    int i_a_rod;
    int i_n_cities;
    int i_n_stack;
    int i_trainer;
    int i_obj_m;
    int d_easy_m;
    int d_avg_m;
    int d_hard_m;
    int d_vhard_m;
    int d_easy_xp;
    int d_avg_xp;
    int d_hard_xp;
    int d_vhard_xp;
    int hp_staymin;
    int hp_healmin;
    int cstchnce;
    int cstchnce_2;
    int cstdbl_min;
    int spl1_chnce;
    int spl2_chnce;
    int spl3_chnce;
    int spl_h_w;
    int spl_h_c;
    int spl_m_w;
    int spl_m_c;
    int spl_l_w;
    int spl_l_c;
    int spl_l5_pow;
    int spl_l4_pow;
    int spl_l3_pow;
    int spl_l2_pow;
    int critspl_w1;
    int critspl_c1;
    int critspl_w2;
    int critspl_c2;
    int critspl_w3;
    int critspl_c3;
    int critspl_w4;
    int critspl_c4;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(category, "CATEGORY", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(b_units, "B_UNITS", adapter, model, row);
        readValue(b_leaders, "B_LEADERS", adapter, model, row);
        readValue(b_cap, "B_CAP", adapter, model, row);
        readValue(b_cities, "B_CITIES", adapter, model, row);
        readValue(b_spell_c, "B_SPELL_C", adapter, model, row);
        readValue(b_spell_r, "B_SPELL_R", adapter, model, row);
        readValue(b_iso, "B_ISO", adapter, model, row);
        readValue(l_min, "L_MIN", adapter, model, row);
        readValue(l_max, "L_MAX", adapter, model, row);
        readValue(pct_fightr, "PCT_FIGHTR", adapter, model, row);
        readValue(t_min, "T_MIN", adapter, model, row);
        readValue(t_max, "T_MAX", adapter, model, row);
        readValue(pct_thief, "PCT_THIEF", adapter, model, row);
        readValue(r_min, "R_MIN", adapter, model, row);
        readValue(r_max, "R_MAX", adapter, model, row);
        readValue(pct_rod, "PCT_ROD", adapter, model, row);
        readValue(i_cities_n, "I_CITIES_N", adapter, model, row);
        readValue(i_cities_r, "I_CITIES_R", adapter, model, row);
        readValue(i_cities_p, "I_CITIES_P", adapter, model, row);
        readValue(i_stacks_n, "I_STACKS_N", adapter, model, row);
        readValue(i_stacks_r, "I_STACKS_R", adapter, model, row);
        readValue(i_stacks_p, "I_STACKS_P", adapter, model, row);
        readValue(i_bags, "I_BAGS", adapter, model, row);
        readValue(i_ruins, "I_RUINS", adapter, model, row);
        readValue(i_towers, "I_TOWERS", adapter, model, row);
        readValue(i_merchant, "I_MERCHANT", adapter, model, row);
        readValue(i_h_units, "I_H_UNITS", adapter, model, row);
        readValue(i_h_mercs, "I_H_MERCS", adapter, model, row);
        readValue(i_heal, "I_HEAL", adapter, model, row);
        readValue(i_p_rod, "I_P_ROD", adapter, model, row);
        readValue(i_a_rod, "I_A_ROD", adapter, model, row);
        readValue(i_n_cities, "I_N_CITIES", adapter, model, row);
        readValue(i_n_stack, "I_N_STACK", adapter, model, row);
        readValue(i_trainer, "I_TRAINER", adapter, model, row);
        readValue(i_obj_m, "I_OBJ_M", adapter, model, row);
        readValue(d_easy_m, "D_EASY_M", adapter, model, row);
        readValue(d_avg_m, "D_AVG_M", adapter, model, row);
        readValue(d_hard_m, "D_HARD_M", adapter, model, row);
        readValue(d_vhard_m, "D_VHARD_M", adapter, model, row);
        readValue(d_easy_xp, "D_EASY_XP", adapter, model, row);
        readValue(d_avg_xp, "D_AVG_XP", adapter, model, row);
        readValue(d_hard_xp, "D_HARD_XP", adapter, model, row);
        readValue(d_vhard_xp, "D_VHARD_XP", adapter, model, row);
        readValue(hp_staymin, "HP_STAYMIN", adapter, model, row);
        readValue(hp_healmin, "HP_HEALMIN", adapter, model, row);
        readValue(cstchnce, "CSTCHNCE", adapter, model, row);
        readValue(cstchnce_2, "CSTCHNCE_2", adapter, model, row);
        readValue(cstdbl_min, "CSTDBL_MIN", adapter, model, row);
        readValue(spl1_chnce, "SPL1_CHNCE", adapter, model, row);
        readValue(spl2_chnce, "SPL2_CHNCE", adapter, model, row);
        readValue(spl3_chnce, "SPL3_CHNCE", adapter, model, row);
        readValue(spl_h_w, "SPL_H_W", adapter, model, row);
        readValue(spl_h_c, "SPL_H_C", adapter, model, row);
        readValue(spl_m_w, "SPL_M_W", adapter, model, row);
        readValue(spl_m_c, "SPL_M_C", adapter, model, row);
        readValue(spl_l_w, "SPL_L_W", adapter, model, row);
        readValue(spl_l_c, "SPL_L_C", adapter, model, row);
        readValue(spl_l5_pow, "SPL_L5_POW", adapter, model, row);
        readValue(spl_l4_pow, "SPL_L4_POW", adapter, model, row);
        readValue(spl_l3_pow, "SPL_L3_POW", adapter, model, row);
        readValue(spl_l2_pow, "SPL_L2_POW", adapter, model, row);
        readValue(critspl_w1, "CRITSPL_W1", adapter, model, row);
        readValue(critspl_c1, "CRITSPL_C1", adapter, model, row);
        readValue(critspl_w2, "CRITSPL_W2", adapter, model, row);
        readValue(critspl_c2, "CRITSPL_C2", adapter, model, row);
        readValue(critspl_w3, "CRITSPL_W3", adapter, model, row);
        readValue(critspl_c3, "CRITSPL_C3", adapter, model, row);
        readValue(critspl_w4, "CRITSPL_W4", adapter, model, row);
        readValue(critspl_c4, "CRITSPL_C4", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(category, "CATEGORY", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(b_units, "B_UNITS", adapter, model, row);
        writeValue(b_leaders, "B_LEADERS", adapter, model, row);
        writeValue(b_cap, "B_CAP", adapter, model, row);
        writeValue(b_cities, "B_CITIES", adapter, model, row);
        writeValue(b_spell_c, "B_SPELL_C", adapter, model, row);
        writeValue(b_spell_r, "B_SPELL_R", adapter, model, row);
        writeValue(b_iso, "B_ISO", adapter, model, row);
        writeValue(l_min, "L_MIN", adapter, model, row);
        writeValue(l_max, "L_MAX", adapter, model, row);
        writeValue(pct_fightr, "PCT_FIGHTR", adapter, model, row);
        writeValue(t_min, "T_MIN", adapter, model, row);
        writeValue(t_max, "T_MAX", adapter, model, row);
        writeValue(pct_thief, "PCT_THIEF", adapter, model, row);
        writeValue(r_min, "R_MIN", adapter, model, row);
        writeValue(r_max, "R_MAX", adapter, model, row);
        writeValue(pct_rod, "PCT_ROD", adapter, model, row);
        writeValue(i_cities_n, "I_CITIES_N", adapter, model, row);
        writeValue(i_cities_r, "I_CITIES_R", adapter, model, row);
        writeValue(i_cities_p, "I_CITIES_P", adapter, model, row);
        writeValue(i_stacks_n, "I_STACKS_N", adapter, model, row);
        writeValue(i_stacks_r, "I_STACKS_R", adapter, model, row);
        writeValue(i_stacks_p, "I_STACKS_P", adapter, model, row);
        writeValue(i_bags, "I_BAGS", adapter, model, row);
        writeValue(i_ruins, "I_RUINS", adapter, model, row);
        writeValue(i_towers, "I_TOWERS", adapter, model, row);
        writeValue(i_merchant, "I_MERCHANT", adapter, model, row);
        writeValue(i_h_units, "I_H_UNITS", adapter, model, row);
        writeValue(i_h_mercs, "I_H_MERCS", adapter, model, row);
        writeValue(i_heal, "I_HEAL", adapter, model, row);
        writeValue(i_p_rod, "I_P_ROD", adapter, model, row);
        writeValue(i_a_rod, "I_A_ROD", adapter, model, row);
        writeValue(i_n_cities, "I_N_CITIES", adapter, model, row);
        writeValue(i_n_stack, "I_N_STACK", adapter, model, row);
        writeValue(i_trainer, "I_TRAINER", adapter, model, row);
        writeValue(i_obj_m, "I_OBJ_M", adapter, model, row);
        writeValue(d_easy_m, "D_EASY_M", adapter, model, row);
        writeValue(d_avg_m, "D_AVG_M", adapter, model, row);
        writeValue(d_hard_m, "D_HARD_M", adapter, model, row);
        writeValue(d_vhard_m, "D_VHARD_M", adapter, model, row);
        writeValue(d_easy_xp, "D_EASY_XP", adapter, model, row);
        writeValue(d_avg_xp, "D_AVG_XP", adapter, model, row);
        writeValue(d_hard_xp, "D_HARD_XP", adapter, model, row);
        writeValue(d_vhard_xp, "D_VHARD_XP", adapter, model, row);
        writeValue(hp_staymin, "HP_STAYMIN", adapter, model, row);
        writeValue(hp_healmin, "HP_HEALMIN", adapter, model, row);
        writeValue(cstchnce, "CSTCHNCE", adapter, model, row);
        writeValue(cstchnce_2, "CSTCHNCE_2", adapter, model, row);
        writeValue(cstdbl_min, "CSTDBL_MIN", adapter, model, row);
        writeValue(spl1_chnce, "SPL1_CHNCE", adapter, model, row);
        writeValue(spl2_chnce, "SPL2_CHNCE", adapter, model, row);
        writeValue(spl3_chnce, "SPL3_CHNCE", adapter, model, row);
        writeValue(spl_h_w, "SPL_H_W", adapter, model, row);
        writeValue(spl_h_c, "SPL_H_C", adapter, model, row);
        writeValue(spl_m_w, "SPL_M_W", adapter, model, row);
        writeValue(spl_m_c, "SPL_M_C", adapter, model, row);
        writeValue(spl_l_w, "SPL_L_W", adapter, model, row);
        writeValue(spl_l_c, "SPL_L_C", adapter, model, row);
        writeValue(spl_l5_pow, "SPL_L5_POW", adapter, model, row);
        writeValue(spl_l4_pow, "SPL_L4_POW", adapter, model, row);
        writeValue(spl_l3_pow, "SPL_L3_POW", adapter, model, row);
        writeValue(spl_l2_pow, "SPL_L2_POW", adapter, model, row);
        writeValue(critspl_w1, "CRITSPL_W1", adapter, model, row);
        writeValue(critspl_c1, "CRITSPL_C1", adapter, model, row);
        writeValue(critspl_w2, "CRITSPL_W2", adapter, model, row);
        writeValue(critspl_c2, "CRITSPL_C2", adapter, model, row);
        writeValue(critspl_w3, "CRITSPL_W3", adapter, model, row);
        writeValue(critspl_c3, "CRITSPL_C3", adapter, model, row);
        writeValue(critspl_w4, "CRITSPL_W4", adapter, model, row);
        writeValue(critspl_c4, "CRITSPL_C4", adapter, model, row);
    }
};

class Gattack: public GameData
{
public:
    static QString typeName() {return "Gattacks";}
    QString key() const {return att_id;}

    QString att_id;
    StringLink<Tglobal> name_txt;
    StringLink<Tglobal> desc_txt;
    int initiative;
    IntLink<LattS> source;
    IntLink<LattC> clas;
    int power;//accuracy
    IntLink<LAttR> reach;
    int qty_heal;
    int qty_dam;
    int level;
    StringLink<Gattack> alt_attack;
    bool infinite;
    int qty_wards;
    QString ward1;
    QString ward2;
    QString ward3;
    QString ward4;
    bool crit_hit;
    StringLinkList<Gtransf> transf;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(att_id, "ATT_ID", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(initiative, "INITIATIVE", adapter, model, row);
        readValue(source, "SOURCE", adapter, model, row);
        readValue(clas, "CLASS", adapter, model, row);
        readValue(power, "POWER", adapter, model, row);
        readValue(reach, "REACH", adapter, model, row);
        readValue(qty_heal, "QTY_HEAL", adapter, model, row);
        readValue(qty_dam, "QTY_DAM", adapter, model, row);
        readValue(level, "LEVEL", adapter, model, row);
        readValue(alt_attack, "ALT_ATTACK", adapter, model, row);
        readValue(infinite, "INFINITE", adapter, model, row);
        readValue(qty_wards, "QTY_WARDS", adapter, model, row);
        readValue(ward1, "WARD1", adapter, model, row);
        readValue(ward2, "WARD2", adapter, model, row);
        readValue(ward3, "WARD3", adapter, model, row);
        readValue(ward4, "WARD4", adapter, model, row);
        readValue(crit_hit, "CRIT_HIT", adapter, model, row);
        readValue(transf, "ATT_ID", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(att_id, "ATT_ID", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(initiative, "INITIATIVE", adapter, model, row);
        writeValue(source, "SOURCE", adapter, model, row);
        writeValue(clas, "CLASS", adapter, model, row);
        writeValue(power, "POWER", adapter, model, row);
        writeValue(reach, "REACH", adapter, model, row);
        writeValue(qty_heal, "QTY_HEAL", adapter, model, row);
        writeValue(qty_dam, "QTY_DAM", adapter, model, row);
        writeValue(level, "LEVEL", adapter, model, row);
        writeValue(alt_attack, "ALT_ATTACK", adapter, model, row);
        writeValue(infinite, "INFINITE", adapter, model, row);
        writeValue(qty_wards, "QTY_WARDS", adapter, model, row);
        writeValue(ward1, "WARD1", adapter, model, row);
        writeValue(ward2, "WARD2", adapter, model, row);
        writeValue(ward3, "WARD3", adapter, model, row);
        writeValue(ward4, "WARD4", adapter, model, row);
        writeValue(crit_hit, "CRIT_HIT", adapter, model, row);
        writeValue(transf, "ATT_ID", adapter, model, row);
    }
};

class Gbuild: public GameData
{
public:
    static QString typeName() {return "Gbuild";}
    QString key() const {return build_id;}

    QString build_id;
    QString name_txt;
    QString desc_txt;
    QString required;
    QString cost;
    int level;
    int category;
    int branch;
    QString pic_1;
    int frame_1;
    int frame_2;
    int pos_x;
    int pos_y;
    int pos_cx;
    int pos_cy;
    int pos_z;
    QString comments;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(build_id, "BUILD_ID", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(required, "REQUIRED", adapter, model, row);
        readValue(cost, "COST", adapter, model, row);
        readValue(level, "LEVEL", adapter, model, row);
        readValue(category, "CATEGORY", adapter, model, row);
        readValue(branch, "BRANCH", adapter, model, row);
        readValue(pic_1, "PIC_1", adapter, model, row);
        readValue(frame_1, "FRAME_1", adapter, model, row);
        readValue(frame_2, "FRAME_2", adapter, model, row);
        readValue(pos_x, "POS_X", adapter, model, row);
        readValue(pos_y, "POS_Y", adapter, model, row);
        readValue(pos_cx, "POS_CX", adapter, model, row);
        readValue(pos_cy, "POS_CY", adapter, model, row);
        readValue(pos_z, "POS_Z", adapter, model, row);
        readValue(comments, "COMMENTS", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(build_id, "BUILD_ID", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(required, "REQUIRED", adapter, model, row);
        writeValue(cost, "COST", adapter, model, row);
        writeValue(level, "LEVEL", adapter, model, row);
        writeValue(category, "CATEGORY", adapter, model, row);
        writeValue(branch, "BRANCH", adapter, model, row);
        writeValue(pic_1, "PIC_1", adapter, model, row);
        writeValue(frame_1, "FRAME_1", adapter, model, row);
        writeValue(frame_2, "FRAME_2", adapter, model, row);
        writeValue(pos_x, "POS_X", adapter, model, row);
        writeValue(pos_y, "POS_Y", adapter, model, row);
        writeValue(pos_cx, "POS_CX", adapter, model, row);
        writeValue(pos_cy, "POS_CY", adapter, model, row);
        writeValue(pos_z, "POS_Z", adapter, model, row);
        writeValue(comments, "COMMENTS", adapter, model, row);
    }
};

class GbuiList: public GameData
{
public:
    static QString typeName() {return "GbuiList";}
    QString key() const {return lord_id;}

    QString lord_id;
    QString build_id;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(lord_id, "LORD_ID", adapter, model, row);
        readValue(build_id, "BUILD_ID", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(lord_id, "LORD_ID", adapter, model, row);
        writeValue(build_id, "BUILD_ID", adapter, model, row);
    }
};

class GcityInf: public GameData
{
public:
    static QString typeName() {return "GcityInf";}
    QString key() const {return race_id;}

    QString race_id;
    int size;
    int scout;
    int regen_f;
    QString protection;
    QString growth;
    int res_m;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(size, "SIZE", adapter, model, row);
        readValue(scout, "SCOUT", adapter, model, row);
        readValue(regen_f, "REGEN_F", adapter, model, row);
        readValue(protection, "PROTECTION", adapter, model, row);
        readValue(growth, "GROWTH", adapter, model, row);
        readValue(res_m, "RES_M", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(size, "SIZE", adapter, model, row);
        writeValue(scout, "SCOUT", adapter, model, row);
        writeValue(regen_f, "REGEN_F", adapter, model, row);
        writeValue(protection, "PROTECTION", adapter, model, row);
        writeValue(growth, "GROWTH", adapter, model, row);
        writeValue(res_m, "RES_M", adapter, model, row);
    }
};

class GDynUpgr: public GameData
{
public:
    static QString typeName() {return "GDynUpgr";}
    QString key() const {return upgrade_id;}

    QString upgrade_id;
    QString enroll_c;
    int hit_point;
    int armor;
    int regen;
    QString revive_c;
    QString heal_c;
    QString training_c;
    int xp_killed;
    int xp_next;
    int move;
    int negotiate;
    int damage;
    int heal;
    int initiative;
    int power;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(upgrade_id, "UPGRADE_ID", adapter, model, row);
        readValue(enroll_c, "ENROLL_C", adapter, model, row);
        readValue(hit_point, "HIT_POINT", adapter, model, row);
        readValue(armor, "ARMOR", adapter, model, row);
        readValue(regen, "REGEN", adapter, model, row);
        readValue(revive_c, "REVIVE_C", adapter, model, row);
        readValue(heal_c, "HEAL_C", adapter, model, row);
        readValue(training_c, "TRAINING_C", adapter, model, row);
        readValue(xp_killed, "XP_KILLED", adapter, model, row);
        readValue(xp_next, "XP_NEXT", adapter, model, row);
        readValue(move, "MOVE", adapter, model, row);
        readValue(negotiate, "NEGOTIATE", adapter, model, row);
        readValue(damage, "DAMAGE", adapter, model, row);
        readValue(heal, "HEAL", adapter, model, row);
        readValue(initiative, "INITIATIVE", adapter, model, row);
        readValue(power, "POWER", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(upgrade_id, "UPGRADE_ID", adapter, model, row);
        writeValue(enroll_c, "ENROLL_C", adapter, model, row);
        writeValue(hit_point, "HIT_POINT", adapter, model, row);
        writeValue(armor, "ARMOR", adapter, model, row);
        writeValue(regen, "REGEN", adapter, model, row);
        writeValue(revive_c, "REVIVE_C", adapter, model, row);
        writeValue(heal_c, "HEAL_C", adapter, model, row);
        writeValue(training_c, "TRAINING_C", adapter, model, row);
        writeValue(xp_killed, "XP_KILLED", adapter, model, row);
        writeValue(xp_next, "XP_NEXT", adapter, model, row);
        writeValue(move, "MOVE", adapter, model, row);
        writeValue(negotiate, "NEGOTIATE", adapter, model, row);
        writeValue(damage, "DAMAGE", adapter, model, row);
        writeValue(heal, "HEAL", adapter, model, row);
        writeValue(initiative, "INITIATIVE", adapter, model, row);
        writeValue(power, "POWER", adapter, model, row);
    }
};

class Gimmu: public GameData
{
public:
    static QString typeName() {return "Gimmu";}
    QString key() const {return unit_id;}

    QString unit_id;
    IntLink<LattS> immunity;
    IntLink<LImmune> immunecat;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(unit_id, "UNIT_ID", adapter, model, row);
        readValue(immunity, "IMMUNITY", adapter, model, row);
        readValue(immunecat, "IMMUNECAT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(unit_id, "UNIT_ID", adapter, model, row);
        writeValue(immunity, "IMMUNITY", adapter, model, row);
        writeValue(immunecat, "IMMUNECAT", adapter, model, row);
    }
};

class GimmuC: public GameData
{
public:
    static QString typeName() {return "GimmuC";}
    QString key() const {return unit_id;}

    QString unit_id;
    IntLink<LattC> immunity;
    IntLink<LImmune> immunecat;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(unit_id, "UNIT_ID", adapter, model, row);
        readValue(immunity, "IMMUNITY", adapter, model, row);
        readValue(immunecat, "IMMUNECAT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(unit_id, "UNIT_ID", adapter, model, row);
        writeValue(immunity, "IMMUNITY", adapter, model, row);
        writeValue(immunecat, "IMMUNECAT", adapter, model, row);
    }
};

class GItem: public GameData
{
public:
    enum Category
    {
        Armor = 0,
        Relict = 1,
        Weapon = 2,
        Banner = 3,
        OneTurnBuff = 4,
        Heal = 5,
        Resurrect = 6,
        EternalBuf = 7,
        Scroll = 8,
        Staff = 9,
        Valuable = 10,
        Sphere = 11,
        Talisman = 12,
        Boots = 13,
        Quest = 14,

        CategoryCount
    };

    static QString typeName() {return "GItem";}
    QString key() const {return item_id;}

    int item_cat;
    QString item_id;
    StringLink<Tglobal> name_txt;
    StringLink<Tglobal> desc_txt;
    QString image_id;
    QString value;
    QString mod_equip;
    QString mod_potion;
    int hp_potion;
    QString spell_id;
    QString casting;
    StringLink<Gattack> attack_id;
    StringLink<Gunit> unit_id;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(item_cat, "ITEM_CAT", adapter, model, row);
        readValue(item_id, "ITEM_ID", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(image_id, "IMAGE_ID", adapter, model, row);
        readValue(value, "VALUE", adapter, model, row);
        readValue(mod_equip, "MOD_EQUIP", adapter, model, row);
        readValue(mod_potion, "MOD_POTION", adapter, model, row);
        readValue(hp_potion, "HP_POTION", adapter, model, row);
        readValue(spell_id, "SPELL_ID", adapter, model, row);
        readValue(casting, "CASTING", adapter, model, row);
        readValue(attack_id, "ATTACK_ID", adapter, model, row);
        readValue(unit_id, "UNIT_ID", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(item_cat, "ITEM_CAT", adapter, model, row);
        writeValue(item_id, "ITEM_ID", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(image_id, "IMAGE_ID", adapter, model, row);
        writeValue(value, "VALUE", adapter, model, row);
        writeValue(mod_equip, "MOD_EQUIP", adapter, model, row);
        writeValue(mod_potion, "MOD_POTION", adapter, model, row);
        writeValue(hp_potion, "HP_POTION", adapter, model, row);
        writeValue(spell_id, "SPELL_ID", adapter, model, row);
        writeValue(casting, "CASTING", adapter, model, row);
        writeValue(attack_id, "ATTACK_ID", adapter, model, row);
        writeValue(unit_id, "UNIT_ID", adapter, model, row);
    }
};

class GLabi: public GameData
{
public:
    static QString typeName() {return "GLabi";}
    QString key() const {return unit_id;}

    QString unit_id;
    int l_ability;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(unit_id, "UNIT_ID", adapter, model, row);
        readValue(l_ability, "L_ABILITY", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(unit_id, "UNIT_ID", adapter, model, row);
        writeValue(l_ability, "L_ABILITY", adapter, model, row);
    }
};

class GleaUpg: public GameData
{
public:
    static QString typeName() {return "GleaUpg";}
    QString key() const {return belongs_to;}

    QString belongs_to;
    QString modif_id;
    int min_level;
    QString name_txt;
    QString desc_txt;
    int priority;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(belongs_to, "BELONGS_TO", adapter, model, row);
        readValue(modif_id, "MODIF_ID", adapter, model, row);
        readValue(min_level, "MIN_LEVEL", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(priority, "PRIORITY", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(belongs_to, "BELONGS_TO", adapter, model, row);
        writeValue(modif_id, "MODIF_ID", adapter, model, row);
        writeValue(min_level, "MIN_LEVEL", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(priority, "PRIORITY", adapter, model, row);
    }
};

class GLmark: public GameData
{
public:
    static QString typeName() {return "GLmark";}
    QString key() const {return lmark_id;}

    QString lmark_id;
    StringLink<Tglobal> name_txt;
    StringLink<Tglobal> desc_txt;
    int cx;
    int cy;
    bool mountain;
    IntLink<LLMCat> category;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(lmark_id, "LMARK_ID", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(cx, "CX", adapter, model, row);
        readValue(cy, "CY", adapter, model, row);
        readValue(mountain, "MOUNTAIN", adapter, model, row);
        readValue(category, "CATEGORY", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(lmark_id, "LMARK_ID", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(cx, "CX", adapter, model, row);
        writeValue(cy, "CY", adapter, model, row);
        writeValue(mountain, "MOUNTAIN", adapter, model, row);
        writeValue(category, "CATEGORY", adapter, model, row);
    }
};

class Glord: public GameData
{
public:
    enum Category
    {
        Mage = 0,
        Warrior,
        Diplomat
    };
    static QString typeName() {return "Glord";}
    QString key() const {return lord_id;}

    QString lord_id;
    StringLink<Grace> race_id;
    IntLink<Llord> category;
    QString name_txt;
    QString desc_txt;
    QString pic;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(lord_id, "LORD_ID", adapter, model, row);
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(category, "CATEGORY", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(pic, "PIC", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(lord_id, "LORD_ID", adapter, model, row);
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(category, "CATEGORY", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(pic, "PIC", adapter, model, row);
    }
};

class GMabi: public GameData
{
public:
    static QString typeName() {return "GMabi";}
    QString key() const {return unit_id;}

    QString unit_id;
    int m_ability;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(unit_id, "UNIT_ID", adapter, model, row);
        readValue(m_ability, "M_ABILITY", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(unit_id, "UNIT_ID", adapter, model, row);
        writeValue(m_ability, "M_ABILITY", adapter, model, row);
    }
};

class Gmodif: public GameData
{
public:
    static QString typeName() {return "Gmodif";}
    QString key() const {return modif_id;}

    QString modif_id;
    int source;
    QString comments;
    QString script;
    bool dislay;
    StringLink<Tglobal> desc_txt;
    StringLinkList<GmodifL> modifiers;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(modif_id, "MODIF_ID", adapter, model, row);
        readValue(source, "SOURCE", adapter, model, row);
        readValue(comments, "COMMENTS", adapter, model, row);
        readValue(script, "SCRIPT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(dislay, "DISLAY", adapter, model, row);
        readValue(modifiers, "MODIF_ID", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(modif_id, "MODIF_ID", adapter, model, row);
        writeValue(source, "SOURCE", adapter, model, row);
        writeValue(comments, "COMMENTS", adapter, model, row);
        writeValue(script, "SCRIPT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(dislay, "DISLAY", adapter, model, row);
    }
};

class GmodifL: public GameData
{
public:
    static QString typeName() {return "GmodifL";}
    QString key() const {return belongs_to;}

    QString belongs_to;
    QString desc;
    int type;
    int percent;
    int number;
    int ability;
    int immunity;
    int immunecat;
    int move;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(belongs_to, "BELONGS_TO", adapter, model, row);
        readValue(desc, "DESC", adapter, model, row);
        readValue(type, "TYPE", adapter, model, row);
        readValue(percent, "PERCENT", adapter, model, row);
        readValue(number, "NUMBER", adapter, model, row);
        readValue(ability, "ABILITY", adapter, model, row);
        readValue(immunity, "IMMUNITY", adapter, model, row);
        readValue(immunecat, "IMMUNECAT", adapter, model, row);
        readValue(move, "MOVE", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(belongs_to, "BELONGS_TO", adapter, model, row);
        writeValue(desc, "DESC", adapter, model, row);
        writeValue(type, "TYPE", adapter, model, row);
        writeValue(percent, "PERCENT", adapter, model, row);
        writeValue(number, "NUMBER", adapter, model, row);
        writeValue(ability, "ABILITY", adapter, model, row);
        writeValue(immunity, "IMMUNITY", adapter, model, row);
        writeValue(immunecat, "IMMUNECAT", adapter, model, row);
        writeValue(move, "MOVE", adapter, model, row);
    }
};

class Grace: public GameData
{
public:
    static QString typeName() {return "Grace";}
    QString key() const {return race_id;}

    QString race_id;
    QString guardian;
    QString noble;
    QString leader_1;
    QString leader_2;
    QString leader_3;
    QString leader_4;
    QString soldier_1;
    QString soldier_2;
    QString soldier_3;
    QString soldier_4;
    QString soldier_5;
    QString soldier_6;
    int scout;
    int regen_h;
    QString income;
    QString protection;
    StringLink<Tglobal> name_txt;
    IntLink<Lrace> race_type;
    bool playable;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(guardian, "GUARDIAN", adapter, model, row);
        readValue(noble, "NOBLE", adapter, model, row);
        readValue(leader_1, "LEADER_1", adapter, model, row);
        readValue(leader_2, "LEADER_2", adapter, model, row);
        readValue(leader_3, "LEADER_3", adapter, model, row);
        readValue(leader_4, "LEADER_4", adapter, model, row);
        readValue(soldier_1, "SOLDIER_1", adapter, model, row);
        readValue(soldier_2, "SOLDIER_2", adapter, model, row);
        readValue(soldier_3, "SOLDIER_3", adapter, model, row);
        readValue(soldier_4, "SOLDIER_4", adapter, model, row);
        readValue(soldier_5, "SOLDIER_5", adapter, model, row);
        readValue(soldier_6, "SOLDIER_6", adapter, model, row);
        readValue(scout, "SCOUT", adapter, model, row);
        readValue(regen_h, "REGEN_H", adapter, model, row);
        readValue(income, "INCOME", adapter, model, row);
        readValue(protection, "PROTECTION", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(race_type, "RACE_TYPE", adapter, model, row);
        readValue(playable, "PLAYABLE", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(guardian, "GUARDIAN", adapter, model, row);
        writeValue(noble, "NOBLE", adapter, model, row);
        writeValue(leader_1, "LEADER_1", adapter, model, row);
        writeValue(leader_2, "LEADER_2", adapter, model, row);
        writeValue(leader_3, "LEADER_3", adapter, model, row);
        writeValue(leader_4, "LEADER_4", adapter, model, row);
        writeValue(soldier_1, "SOLDIER_1", adapter, model, row);
        writeValue(soldier_2, "SOLDIER_2", adapter, model, row);
        writeValue(soldier_3, "SOLDIER_3", adapter, model, row);
        writeValue(soldier_4, "SOLDIER_4", adapter, model, row);
        writeValue(soldier_5, "SOLDIER_5", adapter, model, row);
        writeValue(soldier_6, "SOLDIER_6", adapter, model, row);
        writeValue(scout, "SCOUT", adapter, model, row);
        writeValue(regen_h, "REGEN_H", adapter, model, row);
        writeValue(income, "INCOME", adapter, model, row);
        writeValue(protection, "PROTECTION", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(race_type, "RACE_TYPE", adapter, model, row);
        writeValue(playable, "PLAYABLE", adapter, model, row);
    }
};

class GspellR: public GameData
{
public:
    static QString typeName() {return "GspellR";}
    QString key() const {return spell_id.key;}

    QString lord_id;
    StringLink<GSpell> spell_id;
    QString research;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(lord_id, "LORD_ID", adapter, model, row);
        readValue(spell_id, "SPELL_ID", adapter, model, row);
        readValue(research, "RESEARCH", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(lord_id, "LORD_ID", adapter, model, row);
        writeValue(spell_id, "SPELL_ID", adapter, model, row);
        writeValue(research, "RESEARCH", adapter, model, row);
    }
};

class GSpell: public GameData
{
public:
    static QString typeName() {return "Gspells";}
    QString key() const {return spell_id;}

    QString spell_id;
    int category;
    int level;
    QString casting_c;
    QString buy_c;
    StringLink<Tglobal> name_txt;
    StringLink<Tglobal> desc_txt;
    QString unit_id;
    int restore_m;
    int area;
    QString modif_id;
    StringLink<Tglobal> modif_txt;
    int damage_src;
    int damage_qty;
    int heal_qty;
    int ai_cat;
    int ground_cat;
    bool chngtercat;
    int qty_wards;
    QString ward1;
    QString ward2;
    QString ward3;
    QString ward4;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(spell_id, "SPELL_ID", adapter, model, row);
        readValue(category, "CATEGORY", adapter, model, row);
        readValue(level, "LEVEL", adapter, model, row);
        readValue(casting_c, "CASTING_C", adapter, model, row);
        readValue(buy_c, "BUY_C", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(unit_id, "UNIT_ID", adapter, model, row);
        readValue(restore_m, "RESTORE_M", adapter, model, row);
        readValue(area, "AREA", adapter, model, row);
        readValue(modif_id, "MODIF_ID", adapter, model, row);
        readValue(modif_txt, "MODIF_TXT", adapter, model, row);
        readValue(damage_src, "DAMAGE_SRC", adapter, model, row);
        readValue(damage_qty, "DAMAGE_QTY", adapter, model, row);
        readValue(heal_qty, "HEAL_QTY", adapter, model, row);
        readValue(ai_cat, "AI_CAT", adapter, model, row);
        readValue(ground_cat, "GROUND_CAT", adapter, model, row);
        readValue(chngtercat, "CHNGTERCAT", adapter, model, row);
        readValue(qty_wards, "QTY_WARDS", adapter, model, row);
        readValue(ward1, "WARD1", adapter, model, row);
        readValue(ward2, "WARD2", adapter, model, row);
        readValue(ward3, "WARD3", adapter, model, row);
        readValue(ward4, "WARD4", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(spell_id, "SPELL_ID", adapter, model, row);
        writeValue(category, "CATEGORY", adapter, model, row);
        writeValue(level, "LEVEL", adapter, model, row);
        writeValue(casting_c, "CASTING_C", adapter, model, row);
        writeValue(buy_c, "BUY_C", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(unit_id, "UNIT_ID", adapter, model, row);
        writeValue(restore_m, "RESTORE_M", adapter, model, row);
        writeValue(area, "AREA", adapter, model, row);
        writeValue(modif_id, "MODIF_ID", adapter, model, row);
        writeValue(modif_txt, "MODIF_TXT", adapter, model, row);
        writeValue(damage_src, "DAMAGE_SRC", adapter, model, row);
        writeValue(damage_qty, "DAMAGE_QTY", adapter, model, row);
        writeValue(heal_qty, "HEAL_QTY", adapter, model, row);
        writeValue(ai_cat, "AI_CAT", adapter, model, row);
        writeValue(ground_cat, "GROUND_CAT", adapter, model, row);
        writeValue(chngtercat, "CHNGTERCAT", adapter, model, row);
        writeValue(qty_wards, "QTY_WARDS", adapter, model, row);
        writeValue(ward1, "WARD1", adapter, model, row);
        writeValue(ward2, "WARD2", adapter, model, row);
        writeValue(ward3, "WARD3", adapter, model, row);
        writeValue(ward4, "WARD4", adapter, model, row);
    }
};

class GSubRace: public GameData
{
public:
    static QString typeName() {return "GSubRace";}
    QString key() const {return race_id;}

    QString race_id;
    StringLink<Tglobal> name_txt;
    IntLink<LSubRace> race_type;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(race_type, "RACE_TYPE", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(race_type, "RACE_TYPE", adapter, model, row);
    }
};

class GTileDBI: public GameData
{
public:
    static QString typeName() {return "GTileDBI";}
    QString key() const {return terrain;}

    QString terrain;
    QString ground;
    int ndx;
    int qty;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(terrain, "TERRAIN", adapter, model, row);
        readValue(ground, "GROUND", adapter, model, row);
        readValue(ndx, "NDX", adapter, model, row);
        readValue(qty, "QTY", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(terrain, "TERRAIN", adapter, model, row);
        writeValue(ground, "GROUND", adapter, model, row);
        writeValue(ndx, "NDX", adapter, model, row);
        writeValue(qty, "QTY", adapter, model, row);
    }
};

class Gtransf: public GameData
{
public:
    static QString typeName() {return "Gtransf";}
    QString key() const {return attack_id;}

    QString attack_id;
    StringLink<Gunit> transf_id;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(attack_id, "ATTACK_ID", adapter, model, row);
        readValue(transf_id, "TRANSF_ID", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(attack_id, "ATTACK_ID", adapter, model, row);
        writeValue(transf_id, "TRANSF_ID", adapter, model, row);
    }
};

class Gunit: public GameData
{
public:
    static QString typeName() {return "Gunit";}
    QString key() const {return unit_id;}

    QString unit_id;
    IntLink<LunitC> unit_cat;
    int level;
    StringLink<Gunit> prev_id;
    StringLink<Grace> race_id;
    IntLink<LSubRace> subrace;
    IntLink<LunitB> branch;
    bool size_small;
    bool sex_m;
    QString enroll_c;
    StringLink<Gbuild> enroll_b;
    StringLink<Tglobal> name_txt;
    StringLink<Tglobal> desc_txt;
    StringLink<Tglobal> abil_txt;
    StringLink<Gattack> attack_id;
    StringLink<Gattack> attack2_id;
    bool atck_twice;
    int hit_point;
    StringLink<Gunit> base_unit;
    int armor;
    int regen;
    QString revive_c;
    QString heal_c;
    QString training_c;
    int xp_killed;
    StringLink<Gbuild> upgrade_b;
    int xp_next;
    int move;
    int scout;
    int life_time;
    int leadership;
    int negotiate;
    IntLink<LleadC> leader_cat;
    StringLink<GDynUpgr> dyn_upg1;
    int dyn_upg_lv;
    StringLink<GDynUpgr> dyn_upg2;
    bool water_only;
    int death_anim;
    StringLinkList<Gimmu> immun;
    StringLinkList<GimmuC> immunCategory;
    StringLinkList<GMabi> mobilAbil;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(unit_id, "UNIT_ID", adapter, model, row);
        readValue(unit_cat, "UNIT_CAT", adapter, model, row);
        readValue(level, "LEVEL", adapter, model, row);
        readValue(prev_id, "PREV_ID", adapter, model, row);
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(subrace, "SUBRACE", adapter, model, row);
        readValue(branch, "BRANCH", adapter, model, row);
        readValue(size_small, "SIZE_SMALL", adapter, model, row);
        readValue(sex_m, "SEX_M", adapter, model, row);
        readValue(enroll_c, "ENROLL_C", adapter, model, row);
        readValue(enroll_b, "ENROLL_B", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(desc_txt, "DESC_TXT", adapter, model, row);
        readValue(abil_txt, "ABIL_TXT", adapter, model, row);
        readValue(attack_id, "ATTACK_ID", adapter, model, row);
        readValue(attack2_id, "ATTACK2_ID", adapter, model, row);
        readValue(atck_twice, "ATCK_TWICE", adapter, model, row);
        readValue(hit_point, "HIT_POINT", adapter, model, row);
        readValue(base_unit, "BASE_UNIT", adapter, model, row);
        readValue(armor, "ARMOR", adapter, model, row);
        readValue(regen, "REGEN", adapter, model, row);
        readValue(revive_c, "REVIVE_C", adapter, model, row);
        readValue(heal_c, "HEAL_C", adapter, model, row);
        readValue(training_c, "TRAINING_C", adapter, model, row);
        readValue(xp_killed, "XP_KILLED", adapter, model, row);
        readValue(upgrade_b, "UPGRADE_B", adapter, model, row);
        readValue(xp_next, "XP_NEXT", adapter, model, row);
        readValue(move, "MOVE", adapter, model, row);
        readValue(scout, "SCOUT", adapter, model, row);
        readValue(life_time, "LIFE_TIME", adapter, model, row);
        readValue(leadership, "LEADERSHIP", adapter, model, row);
        readValue(negotiate, "NEGOTIATE", adapter, model, row);
        readValue(leader_cat, "LEADER_CAT", adapter, model, row);
        readValue(dyn_upg1, "DYN_UPG1", adapter, model, row);
        readValue(dyn_upg_lv, "DYN_UPG_LV", adapter, model, row);
        readValue(dyn_upg2, "DYN_UPG2", adapter, model, row);
        readValue(water_only, "WATER_ONLY", adapter, model, row);
        readValue(death_anim, "DEATH_ANIM", adapter, model, row);
        readValue(immun, "UNIT_ID", adapter, model, row);
        readValue(immunCategory, "UNIT_ID", adapter, model, row);
        readValue(mobilAbil, "UNIT_ID", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(unit_id, "UNIT_ID", adapter, model, row);
        writeValue(unit_cat, "UNIT_CAT", adapter, model, row);
        writeValue(level, "LEVEL", adapter, model, row);
        writeValue(prev_id, "PREV_ID", adapter, model, row);
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(subrace, "SUBRACE", adapter, model, row);
        writeValue(branch, "BRANCH", adapter, model, row);
        writeValue(size_small, "SIZE_SMALL", adapter, model, row);
        writeValue(sex_m, "SEX_M", adapter, model, row);
        writeValue(enroll_c, "ENROLL_C", adapter, model, row);
        writeValue(enroll_b, "ENROLL_B", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(desc_txt, "DESC_TXT", adapter, model, row);
        writeValue(abil_txt, "ABIL_TXT", adapter, model, row);
        writeValue(attack_id, "ATTACK_ID", adapter, model, row);
        writeValue(attack2_id, "ATTACK2_ID", adapter, model, row);
        writeValue(atck_twice, "ATCK_TWICE", adapter, model, row);
        writeValue(hit_point, "HIT_POINT", adapter, model, row);
        writeValue(base_unit, "BASE_UNIT", adapter, model, row);
        writeValue(armor, "ARMOR", adapter, model, row);
        writeValue(regen, "REGEN", adapter, model, row);
        writeValue(revive_c, "REVIVE_C", adapter, model, row);
        writeValue(heal_c, "HEAL_C", adapter, model, row);
        writeValue(training_c, "TRAINING_C", adapter, model, row);
        writeValue(xp_killed, "XP_KILLED", adapter, model, row);
        writeValue(upgrade_b, "UPGRADE_B", adapter, model, row);
        writeValue(xp_next, "XP_NEXT", adapter, model, row);
        writeValue(move, "MOVE", adapter, model, row);
        writeValue(scout, "SCOUT", adapter, model, row);
        writeValue(life_time, "LIFE_TIME", adapter, model, row);
        writeValue(leadership, "LEADERSHIP", adapter, model, row);
        writeValue(negotiate, "NEGOTIATE", adapter, model, row);
        writeValue(leader_cat, "LEADER_CAT", adapter, model, row);
        writeValue(dyn_upg1, "DYN_UPG1", adapter, model, row);
        writeValue(dyn_upg_lv, "DYN_UPG_LV", adapter, model, row);
        writeValue(dyn_upg2, "DYN_UPG2", adapter, model, row);
        writeValue(water_only, "WATER_ONLY", adapter, model, row);
        writeValue(death_anim, "DEATH_ANIM", adapter, model, row);
        writeValue(immun, "UNIT_ID", adapter, model, row);
        writeValue(immunCategory, "UNIT_ID", adapter, model, row);
        writeValue(mobilAbil, "UNIT_ID", adapter, model, row);
    }
};

class GVars: public GameData
{
public:
    static QString typeName() {return "GVars";}
    QString key() const {return "";}

    int morale_1;
    int morale_2;
    int morale_3;
    int morale_4;
    int morale_5;
    int morale_6;
    int weapn_mstr;
    int bat_init;
    int bat_damage;
    int bat_round;
    int bat_break;
    int bat_bmodif;
    int batboostd1;
    int batboostd2;
    int batboostd3;
    int batboostd4;
    int batlowerd1;
    int batlowerd2;
    int batloweri1;
    int ldrmaxabil;
    int spy_discov;
    int poison_s;
    int poison_c;
    int bribe;
    int steal_race;
    int steal_neut;
    int riot_min;
    int riot_max;
    int riot_dmg;
    int sell_ratio;
    int t_capture;
    int t_capital;
    int t_city1;
    int t_city2;
    int t_city3;
    int t_city4;
    int t_city5;
    QString rod_cost;
    int rod_range;
    int crystal_p;
    int const_urg;
    int regen_lwar;
    int regen_ruin;
    int d_peace;
    int d_war;
    int d_neutral;
    int d_gold;
    int d_mk_ally;
    int d_attak_sc;
    int d_attak_fo;
    int d_attak_pc;
    int d_rod;
    int d_ref_ally;
    int d_bk_ally;
    int d_noble;
    int d_bka_chnc;
    int d_bka_turn;
    int prot_1;
    int prot_2;
    int prot_3;
    int prot_4;
    int prot_5;
    int prot_cap;
    int bonus_e;
    int bonus_a;
    int bonus_h;
    int bonus_v;
    int income_e;
    int income_a;
    int income_h;
    int income_v;
    int gu_range;
    int pa_range;
    int lo_range;
    int dfendbonus;
    int talis_chrg;
    int splpwr_0;
    int splpwr_1;
    int splpwr_2;
    int splpwr_3;
    int splpwr_4;
    int gain_spell;
    QString tutorial;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(morale_1, "MORALE_1", adapter, model, row);
        readValue(morale_2, "MORALE_2", adapter, model, row);
        readValue(morale_3, "MORALE_3", adapter, model, row);
        readValue(morale_4, "MORALE_4", adapter, model, row);
        readValue(morale_5, "MORALE_5", adapter, model, row);
        readValue(morale_6, "MORALE_6", adapter, model, row);
        readValue(weapn_mstr, "WEAPN_MSTR", adapter, model, row);
        readValue(bat_init, "BAT_INIT", adapter, model, row);
        readValue(bat_damage, "BAT_DAMAGE", adapter, model, row);
        readValue(bat_round, "BAT_ROUND", adapter, model, row);
        readValue(bat_break, "BAT_BREAK", adapter, model, row);
        readValue(bat_bmodif, "BAT_BMODIF", adapter, model, row);
        readValue(batboostd1, "BATBOOSTD1", adapter, model, row);
        readValue(batboostd2, "BATBOOSTD2", adapter, model, row);
        readValue(batboostd3, "BATBOOSTD3", adapter, model, row);
        readValue(batboostd4, "BATBOOSTD4", adapter, model, row);
        readValue(batlowerd1, "BATLOWERD1", adapter, model, row);
        readValue(batlowerd2, "BATLOWERD2", adapter, model, row);
        readValue(batloweri1, "BATLOWERI1", adapter, model, row);
        readValue(ldrmaxabil, "LDRMAXABIL", adapter, model, row);
        readValue(spy_discov, "SPY_DISCOV", adapter, model, row);
        readValue(poison_s, "POISON_S", adapter, model, row);
        readValue(poison_c, "POISON_C", adapter, model, row);
        readValue(bribe, "BRIBE", adapter, model, row);
        readValue(steal_race, "STEAL_RACE", adapter, model, row);
        readValue(steal_neut, "STEAL_NEUT", adapter, model, row);
        readValue(riot_min, "RIOT_MIN", adapter, model, row);
        readValue(riot_max, "RIOT_MAX", adapter, model, row);
        readValue(riot_dmg, "RIOT_DMG", adapter, model, row);
        readValue(sell_ratio, "SELL_RATIO", adapter, model, row);
        readValue(t_capture, "T_CAPTURE", adapter, model, row);
        readValue(t_capital, "T_CAPITAL", adapter, model, row);
        readValue(t_city1, "T_CITY1", adapter, model, row);
        readValue(t_city2, "T_CITY2", adapter, model, row);
        readValue(t_city3, "T_CITY3", adapter, model, row);
        readValue(t_city4, "T_CITY4", adapter, model, row);
        readValue(t_city5, "T_CITY5", adapter, model, row);
        readValue(rod_cost, "ROD_COST", adapter, model, row);
        readValue(rod_range, "ROD_RANGE", adapter, model, row);
        readValue(crystal_p, "CRYSTAL_P", adapter, model, row);
        readValue(const_urg, "CONST_URG", adapter, model, row);
        readValue(regen_lwar, "REGEN_LWAR", adapter, model, row);
        readValue(regen_ruin, "REGEN_RUIN", adapter, model, row);
        readValue(d_peace, "D_PEACE", adapter, model, row);
        readValue(d_war, "D_WAR", adapter, model, row);
        readValue(d_neutral, "D_NEUTRAL", adapter, model, row);
        readValue(d_gold, "D_GOLD", adapter, model, row);
        readValue(d_mk_ally, "D_MK_ALLY", adapter, model, row);
        readValue(d_attak_sc, "D_ATTAK_SC", adapter, model, row);
        readValue(d_attak_fo, "D_ATTAK_FO", adapter, model, row);
        readValue(d_attak_pc, "D_ATTAK_PC", adapter, model, row);
        readValue(d_rod, "D_ROD", adapter, model, row);
        readValue(d_ref_ally, "D_REF_ALLY", adapter, model, row);
        readValue(d_bk_ally, "D_BK_ALLY", adapter, model, row);
        readValue(d_noble, "D_NOBLE", adapter, model, row);
        readValue(d_bka_chnc, "D_BKA_CHNC", adapter, model, row);
        readValue(d_bka_turn, "D_BKA_TURN", adapter, model, row);
        readValue(prot_1, "PROT_1", adapter, model, row);
        readValue(prot_2, "PROT_2", adapter, model, row);
        readValue(prot_3, "PROT_3", adapter, model, row);
        readValue(prot_4, "PROT_4", adapter, model, row);
        readValue(prot_5, "PROT_5", adapter, model, row);
        readValue(prot_cap, "PROT_CAP", adapter, model, row);
        readValue(bonus_e, "BONUS_E", adapter, model, row);
        readValue(bonus_a, "BONUS_A", adapter, model, row);
        readValue(bonus_h, "BONUS_H", adapter, model, row);
        readValue(bonus_v, "BONUS_V", adapter, model, row);
        readValue(income_e, "INCOME_E", adapter, model, row);
        readValue(income_a, "INCOME_A", adapter, model, row);
        readValue(income_h, "INCOME_H", adapter, model, row);
        readValue(income_v, "INCOME_V", adapter, model, row);
        readValue(gu_range, "GU_RANGE", adapter, model, row);
        readValue(pa_range, "PA_RANGE", adapter, model, row);
        readValue(lo_range, "LO_RANGE", adapter, model, row);
        readValue(dfendbonus, "DFENDBONUS", adapter, model, row);
        readValue(talis_chrg, "TALIS_CHRG", adapter, model, row);
        readValue(splpwr_0, "SPLPWR_0", adapter, model, row);
        readValue(splpwr_1, "SPLPWR_1", adapter, model, row);
        readValue(splpwr_2, "SPLPWR_2", adapter, model, row);
        readValue(splpwr_3, "SPLPWR_3", adapter, model, row);
        readValue(splpwr_4, "SPLPWR_4", adapter, model, row);
        readValue(gain_spell, "GAIN_SPELL", adapter, model, row);
        readValue(tutorial, "TUTORIAL", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(morale_1, "MORALE_1", adapter, model, row);
        writeValue(morale_2, "MORALE_2", adapter, model, row);
        writeValue(morale_3, "MORALE_3", adapter, model, row);
        writeValue(morale_4, "MORALE_4", adapter, model, row);
        writeValue(morale_5, "MORALE_5", adapter, model, row);
        writeValue(morale_6, "MORALE_6", adapter, model, row);
        writeValue(weapn_mstr, "WEAPN_MSTR", adapter, model, row);
        writeValue(bat_init, "BAT_INIT", adapter, model, row);
        writeValue(bat_damage, "BAT_DAMAGE", adapter, model, row);
        writeValue(bat_round, "BAT_ROUND", adapter, model, row);
        writeValue(bat_break, "BAT_BREAK", adapter, model, row);
        writeValue(bat_bmodif, "BAT_BMODIF", adapter, model, row);
        writeValue(batboostd1, "BATBOOSTD1", adapter, model, row);
        writeValue(batboostd2, "BATBOOSTD2", adapter, model, row);
        writeValue(batboostd3, "BATBOOSTD3", adapter, model, row);
        writeValue(batboostd4, "BATBOOSTD4", adapter, model, row);
        writeValue(batlowerd1, "BATLOWERD1", adapter, model, row);
        writeValue(batlowerd2, "BATLOWERD2", adapter, model, row);
        writeValue(batloweri1, "BATLOWERI1", adapter, model, row);
        writeValue(ldrmaxabil, "LDRMAXABIL", adapter, model, row);
        writeValue(spy_discov, "SPY_DISCOV", adapter, model, row);
        writeValue(poison_s, "POISON_S", adapter, model, row);
        writeValue(poison_c, "POISON_C", adapter, model, row);
        writeValue(bribe, "BRIBE", adapter, model, row);
        writeValue(steal_race, "STEAL_RACE", adapter, model, row);
        writeValue(steal_neut, "STEAL_NEUT", adapter, model, row);
        writeValue(riot_min, "RIOT_MIN", adapter, model, row);
        writeValue(riot_max, "RIOT_MAX", adapter, model, row);
        writeValue(riot_dmg, "RIOT_DMG", adapter, model, row);
        writeValue(sell_ratio, "SELL_RATIO", adapter, model, row);
        writeValue(t_capture, "T_CAPTURE", adapter, model, row);
        writeValue(t_capital, "T_CAPITAL", adapter, model, row);
        writeValue(t_city1, "T_CITY1", adapter, model, row);
        writeValue(t_city2, "T_CITY2", adapter, model, row);
        writeValue(t_city3, "T_CITY3", adapter, model, row);
        writeValue(t_city4, "T_CITY4", adapter, model, row);
        writeValue(t_city5, "T_CITY5", adapter, model, row);
        writeValue(rod_cost, "ROD_COST", adapter, model, row);
        writeValue(rod_range, "ROD_RANGE", adapter, model, row);
        writeValue(crystal_p, "CRYSTAL_P", adapter, model, row);
        writeValue(const_urg, "CONST_URG", adapter, model, row);
        writeValue(regen_lwar, "REGEN_LWAR", adapter, model, row);
        writeValue(regen_ruin, "REGEN_RUIN", adapter, model, row);
        writeValue(d_peace, "D_PEACE", adapter, model, row);
        writeValue(d_war, "D_WAR", adapter, model, row);
        writeValue(d_neutral, "D_NEUTRAL", adapter, model, row);
        writeValue(d_gold, "D_GOLD", adapter, model, row);
        writeValue(d_mk_ally, "D_MK_ALLY", adapter, model, row);
        writeValue(d_attak_sc, "D_ATTAK_SC", adapter, model, row);
        writeValue(d_attak_fo, "D_ATTAK_FO", adapter, model, row);
        writeValue(d_attak_pc, "D_ATTAK_PC", adapter, model, row);
        writeValue(d_rod, "D_ROD", adapter, model, row);
        writeValue(d_ref_ally, "D_REF_ALLY", adapter, model, row);
        writeValue(d_bk_ally, "D_BK_ALLY", adapter, model, row);
        writeValue(d_noble, "D_NOBLE", adapter, model, row);
        writeValue(d_bka_chnc, "D_BKA_CHNC", adapter, model, row);
        writeValue(d_bka_turn, "D_BKA_TURN", adapter, model, row);
        writeValue(prot_1, "PROT_1", adapter, model, row);
        writeValue(prot_2, "PROT_2", adapter, model, row);
        writeValue(prot_3, "PROT_3", adapter, model, row);
        writeValue(prot_4, "PROT_4", adapter, model, row);
        writeValue(prot_5, "PROT_5", adapter, model, row);
        writeValue(prot_cap, "PROT_CAP", adapter, model, row);
        writeValue(bonus_e, "BONUS_E", adapter, model, row);
        writeValue(bonus_a, "BONUS_A", adapter, model, row);
        writeValue(bonus_h, "BONUS_H", adapter, model, row);
        writeValue(bonus_v, "BONUS_V", adapter, model, row);
        writeValue(income_e, "INCOME_E", adapter, model, row);
        writeValue(income_a, "INCOME_A", adapter, model, row);
        writeValue(income_h, "INCOME_H", adapter, model, row);
        writeValue(income_v, "INCOME_V", adapter, model, row);
        writeValue(gu_range, "GU_RANGE", adapter, model, row);
        writeValue(pa_range, "PA_RANGE", adapter, model, row);
        writeValue(lo_range, "LO_RANGE", adapter, model, row);
        writeValue(dfendbonus, "DFENDBONUS", adapter, model, row);
        writeValue(talis_chrg, "TALIS_CHRG", adapter, model, row);
        writeValue(splpwr_0, "SPLPWR_0", adapter, model, row);
        writeValue(splpwr_1, "SPLPWR_1", adapter, model, row);
        writeValue(splpwr_2, "SPLPWR_2", adapter, model, row);
        writeValue(splpwr_3, "SPLPWR_3", adapter, model, row);
        writeValue(splpwr_4, "SPLPWR_4", adapter, model, row);
        writeValue(gain_spell, "GAIN_SPELL", adapter, model, row);
        writeValue(tutorial, "TUTORIAL", adapter, model, row);
    }
};

class Laction: public GameData
{
public:
    static QString typeName() {return "Laction";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LaiAtt: public GameData
{
public:
    static QString typeName() {return "LaiAtt";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LAiMsg: public GameData
{
public:
    static QString typeName() {return "LAiMsg";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LAiSpell: public GameData
{
public:
    static QString typeName() {return "LAiSpell";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LattC: public GameData
{
public:
    static QString typeName() {return "LattC";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class TApp: public GameData
{
public:
    static QString typeName() {return "TApp";}
    QString key() const {return txt_id;}

    QString txt_id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(txt_id, "TXT_ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(txt_id, "TXT_ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class TAppEdit: public GameData
{
public:
    static QString typeName() {return "TAppEdit";}
    QString key() const {return txt_id;}

    QString txt_id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(txt_id, "TXT_ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(txt_id, "TXT_ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LAttR: public GameData
{
public:
    static QString typeName() {return "LAttR";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;
    StringLink<TApp> reach_txt;
    StringLink<TApp> target_txt;
    QString sel_script;
    QString att_script;
    bool mark_targets;
    int max_targts;
    bool melee;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
        readValue(reach_txt, "REACH_TXT", adapter, model, row);
        readValue(target_txt, "TARGET_TXT", adapter, model, row);
        readValue(sel_script, "TEXT", adapter, model, row);
        readValue(att_script, "SEL_SCRIPT", adapter, model, row);
        readValue(melee, "MELEE", adapter, model, row);
        readValue(mark_targets, "MARK_TARGETS", adapter, model, row);
        readValue(melee, "MELEE", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
        writeValue(reach_txt, "REACH_TXT", adapter, model, row);
        writeValue(target_txt, "TARGET_TXT", adapter, model, row);
        writeValue(max_targts, "MAX_TARGTS", adapter, model, row);
        writeValue(melee, "MELEE", adapter, model, row);
    }
};

class LattS: public GameData
{
public:
    static QString typeName() {return "LattS";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;
    StringLink<TApp> name_txt;
    int immu_ai_r;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
        readValue(name_txt, "NAME_TXT", adapter, model, row);
        readValue(immu_ai_r, "IMMU_AI_R", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
        writeValue(name_txt, "NAME_TXT", adapter, model, row);
        writeValue(immu_ai_r, "IMMU_AI_R", adapter, model, row);
    }
};

class Lbuild: public GameData
{
public:
    static QString typeName() {return "Lbuild";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Ldiff: public GameData
{
public:
    static QString typeName() {return "Ldiff";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LDthAnim: public GameData
{
public:
    static QString typeName() {return "LDthAnim";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LEvCond: public GameData
{
public:
    static QString typeName() {return "LEvCond";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LEvEffct: public GameData
{
public:
    static QString typeName() {return "LEvEffct";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LFort: public GameData
{
public:
    static QString typeName() {return "LFort";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Lground: public GameData
{
public:
    static QString typeName() {return "Lground";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LImmune: public GameData
{
public:
    static QString typeName() {return "LImmune";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LleadA: public GameData
{
public:
    static QString typeName() {return "LleadA";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LleadC: public GameData
{
public:
    static QString typeName() {return "LleadC";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LLMCat: public GameData
{
public:
    static QString typeName() {return "LLMCat";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Llord: public GameData
{
public:
    static QString typeName() {return "Llord";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LmagItm: public GameData
{
public:
    static QString typeName() {return "LmagItm";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LmodifE: public GameData
{
public:
    static QString typeName() {return "LmodifE";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LModifS: public GameData
{
public:
    static QString typeName() {return "LModifS";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LOrder: public GameData
{
public:
    static QString typeName() {return "LOrder";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Lrace: public GameData
{
public:
    static QString typeName() {return "Lrace";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Lres: public GameData
{
public:
    static QString typeName() {return "Lres";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Lsite: public GameData
{
public:
    static QString typeName() {return "Lsite";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Lspell: public GameData
{
public:
    static QString typeName() {return "Lspell";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LSplPhas: public GameData
{
public:
    static QString typeName() {return "LSplPhas";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LSubRace: public GameData
{
public:
    static QString typeName() {return "LSubRace";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Lterrain: public GameData
{
public:
    static QString typeName() {return "Lterrain";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Ltreaty: public GameData
{
public:
    static QString typeName() {return "Ltreaty";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LunitB: public GameData
{
public:
    static QString typeName() {return "LunitB";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class LunitC: public GameData
{
public:
    static QString typeName() {return "LunitC";}
    QString key() const {return QString::number(id);}

    int id;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(id, "ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(id, "ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class TAiMsg: public GameData
{
public:
    static QString typeName() {return "TAiMsg";}
    QString key() const {return race_id;}

    QString race_id;
    int msg_cat;
    QString text;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(msg_cat, "MSG_CAT", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(msg_cat, "MSG_CAT", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
    }
};

class Tglobal: public GameData
{
public:
    static QString typeName() {return "Tglobal";}
    QString key() const {return txt_id;}

    QString txt_id;
    QString text;
    bool verified;
    QString context;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(txt_id, "TXT_ID", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
        readValue(verified, "VERIFIED", adapter, model, row);
        readValue(context, "CONTEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(txt_id, "TXT_ID", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
        writeValue(verified, "VERIFIED", adapter, model, row);
        writeValue(context, "CONTEXT", adapter, model, row);
    }
};

class Tleader: public GameData
{
public:
    static QString typeName() {return "Tleader";}
    QString key() const {return race_id;}

    QString race_id;
    bool sex_m;
    QString text;
    bool verified;
    QString context;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(sex_m, "SEX_M", adapter, model, row);
        readValue(text, "TEXT", adapter, model, row);
        readValue(verified, "VERIFIED", adapter, model, row);
        readValue(context, "CONTEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(sex_m, "SEX_M", adapter, model, row);
        writeValue(text, "TEXT", adapter, model, row);
        writeValue(verified, "VERIFIED", adapter, model, row);
        writeValue(context, "CONTEXT", adapter, model, row);
    }
};

class Tplayer: public GameData
{
public:
    static QString typeName() {return "Tplayer";}
    QString key() const {return race_id;}

    QString race_id;
    QString text_m;
    QString text_f;
    bool verified;
    QString context;

    void read(SimpleDBFAdapter & adapter, DataModel * model, int row)
    {
        readValue(race_id, "RACE_ID", adapter, model, row);
        readValue(text_m, "TEXT_M", adapter, model, row);
        readValue(text_f, "TEXT_F", adapter, model, row);
        readValue(verified, "VERIFIED", adapter, model, row);
        readValue(context, "CONTEXT", adapter, model, row);
    }

    void write(SimpleDBFAdapter & adapter, DataModel * model, int row) const
    {
        writeValue(race_id, "RACE_ID", adapter, model, row);
        writeValue(text_m, "TEXT_M", adapter, model, row);
        writeValue(text_f, "TEXT_F", adapter, model, row);
        writeValue(verified, "VERIFIED", adapter, model, row);
        writeValue(context, "CONTEXT", adapter, model, row);
    }
};

static int calcLeveledValue(int value, int startLevel, int resultLevel, int threshold, int up1, int up2)
{
    int result = value;
    result += (qMin(resultLevel, threshold) - startLevel) * up1;
    result += (qMax(resultLevel, threshold) - threshold) * up2;
    return result;
}

#endif // GAMECLASES_H
