#ifndef SIMPLEDBFADAPTER_H
#define SIMPLEDBFADAPTER_H

#include <QObject>
#include <QFile>
#include <QByteArray>
#include <QList>
#include "ByteHelper.h"

//https://en.wikipedia.org/wiki/.dbf
class SimpleDBFAdapter
{
public:
    explicit SimpleDBFAdapter();
    enum Type
    {
        String,
        Int,
        Double,
        Bool
    };
    struct HeaderData
    {
        QString name;
        QString typeStr;
        Type type;
        int fieldLen;
        QByteArray getData(const QString& data, const QString& encoding)
        {
            QString prepared = data;
            if (type == Type::String)
            {

            }
            else
            {
                while(prepared.length() < fieldLen)
                {
                    prepared.prepend(' ');
                }
            }
            prepared.resize(fieldLen, ' ');
            return stringToByteArrayCp866(prepared);
        }
    };
    struct RecordData
    {
        QString value;
        SimpleDBFAdapter::Type type;
    };

    int rowCount();
    int columnCount();
    HeaderData getHeader(int index);
    QVector<QString> getRow(int index);

    QString getString(QString fieldName, int row) const;
    RecordData getData(QString fieldName, int row);

    QString getString(int row, int column) const;
    RecordData getData(int row, int column) const;

    int getInt(QString fieldName, int row, int valueIfEmpty = 0);
    bool getBool(QString fieldName, int row, bool valueIfEmpty = false);

    bool readDBFFile(QString filename);

    bool readHeader(const QByteArray& array);

    bool readRowsData(const QByteArray& array, int startIndex);

    static Type TypeFromString(QString typeStr);

    static QString stringFromType(Type type);
    //
    bool set(const QString & fieldName, int row, const QString& value);
    bool save(const QString& path);
private:
    int m_rowSize = 0;
    QByteArray m_headerData;
    QVector<HeaderData> m_header;
    QVector<QVector<QString>> m_rowsData;

    QString m_filename;
    bool m_changed;
};

#endif // SIMPLEDBFADAPTER_H
