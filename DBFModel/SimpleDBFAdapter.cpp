#include "SimpleDBFAdapter.h"
#include <QDebug>
#include <QFileInfo>

SimpleDBFAdapter::SimpleDBFAdapter()
{

}

int SimpleDBFAdapter::rowCount()
{
    return m_rowsData.count();
}

int SimpleDBFAdapter::columnCount()
{
    return (m_rowsData.count() > 0)? m_rowsData[0].count(): 0;
}

SimpleDBFAdapter::HeaderData SimpleDBFAdapter::getHeader(int index)
{
    return m_header[index];
}

QVector<QString> SimpleDBFAdapter::getRow(int index)
{
    return m_rowsData[index];
}

QString SimpleDBFAdapter::getString(QString fieldName, int row) const
{
    int index = -1;
    for(int i = 0; i < m_header.count(); i++)
    {
        if (m_header[i].name == fieldName)
        {
            index = i;
            break;
        }
    }
    if (index == -1)
        return QString();

    return m_rowsData[row][index];
}

SimpleDBFAdapter::RecordData SimpleDBFAdapter::getData(QString fieldName, int row)
{
    RecordData result;//TODO: fix
   // result.type = getHeader();
    result.value = getString(fieldName, row);
    return result;
}

QString SimpleDBFAdapter::getString(int row, int column) const
{
    return m_rowsData[row][column];
}

SimpleDBFAdapter::RecordData SimpleDBFAdapter::getData(int row, int column) const
{
    RecordData result;
    result.type = m_header[column].type;
    result.value = getString(row, column);
    return result;
}

int SimpleDBFAdapter::getInt(QString fieldName, int row, int valueIfEmpty)
{
    QString value = getString(fieldName, row);
    if (value.trimmed() == "")
        return valueIfEmpty;
    return value.toInt();
}

bool SimpleDBFAdapter::getBool(QString fieldName, int row, bool valueIfEmpty)
{
    QString value = getString(fieldName, row);
    if (value.trimmed() == "")
        return valueIfEmpty;
    return value == "T";
}

bool SimpleDBFAdapter::readDBFFile(QString filename)
{
    m_header.clear();
    m_rowsData.clear();
    m_changed = false;
    m_rowSize = 0;

    QByteArray array;
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Failed to open DB file" + file.fileName();
        return false;
    }
    array = file.readAll();
    int bytesInHeader = ByteHelper::readInt(array, 8, 2) - 32;
    QByteArray header = array.mid(32, bytesInHeader);
    m_headerData = array.mid(0, bytesInHeader + 32);
    if (!readHeader(header))
        return false;
    if (!readRowsData(array, bytesInHeader + 32))
        return false;
    QFileInfo info(filename);
    m_filename = info.baseName();
    return true;
}

bool SimpleDBFAdapter::readHeader(const QByteArray &array)
{
    int startIndex = 0;
    while (startIndex < array.length() - 31)
    {
        HeaderData data;
        data.name = ByteHelper::trimmed(ByteHelper::readString(array, startIndex, 11));
        data.typeStr = ByteHelper::readString(array, startIndex + 11, 1);
        data.type = TypeFromString(data.typeStr);
        data.fieldLen = ByteHelper::readInt(array, startIndex + 16, 1);

        m_header.append(data);
        startIndex += 32;
    }
    foreach (HeaderData hData, m_header)
        m_rowSize += hData.fieldLen;

    return true;
}

bool SimpleDBFAdapter::readRowsData(const QByteArray &array, int startIndex)
{
    while (startIndex < array.length() - m_rowSize + 1)
    {
        QVector<QString> row;
        QString startSymb = ByteHelper::readString(array, startIndex, 1);
        if (startSymb == "*")
        {
            startIndex += m_rowSize + 1;
            continue;
        }
        else
            startIndex += 1;

        foreach (HeaderData hData, m_header)
        {
            row.append(ByteHelper::readStringUTF8(array, startIndex, hData.fieldLen).trimmed());
            startIndex += hData.fieldLen;
        }
        m_rowsData.append(row);
    }

    return true;
}
//D, F,  M
SimpleDBFAdapter::Type SimpleDBFAdapter::TypeFromString(QString typeStr)
{
    if (typeStr == "L")
        return Type::Bool;
    if (typeStr == "C")
        return Type::String;
    if (typeStr == "N")
        return Type::Int;
    return Type::String;
}

QString SimpleDBFAdapter::stringFromType(SimpleDBFAdapter::Type type)
{
    if (type == Type::Bool)
        return "(bool)";
    if (type == Type::String)
        return "(string)";
    if (type == Type::Int)
        return "(int)";
    return "(bool)";
}

bool SimpleDBFAdapter::set(const QString &fieldName, int row, const QString &value)
{
    int index = -1;
    for (int i = 0; i < m_header.count(); i++)
    {
        if (m_header[i].name == fieldName)
        {
            index = i;
            break;
        }
    }
    if (index == -1)
        return false;
    m_rowsData[row][index] = value;
    m_changed = true;
    return true;
}

bool SimpleDBFAdapter::save(const QString &path)
{
    if (!m_changed)
        return true;
    QString resultName = path + "/" + m_filename + ".dbf";
    int recordsCount = m_rowsData.count();
    m_headerData[4] = static_cast<short>((recordsCount >> 0) & 0xFF);
    m_headerData[5] = static_cast<short>((recordsCount >> 8) & 0xFF);
    m_headerData[6] = static_cast<short>((recordsCount >> 16) & 0xFF);
    m_headerData[7] = static_cast<short>((recordsCount >> 24) & 0xFF);
    QFile file(resultName);
    if (file.open(QIODevice::WriteOnly))
    {
        file.write(m_headerData);
        for(int i = 0; i < recordsCount; ++i)
        {
            file.write(" ", 1);
            for(int q = 0; q < m_header.count(); ++ q)
            {
                file.write(m_header[q].getData(m_rowsData[i][q], "cp866"));
            }
        }
    }
    else
    {
        qDebug()<<"failed to open file";
    }
    qDebug()<<resultName;
    file.close();
    return false;
}
