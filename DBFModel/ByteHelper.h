#ifndef BYTEHELPER_H
#define BYTEHELPER_H
#include <QByteArray>
#include <QDataStream>
#include <QString>
#include "../Common/ByteEncoder.h"

class ByteHelper
{
public:
    ByteHelper() {}
    static int32_t readInt(const QByteArray& array, int startIndex, int size, bool bigEndian = false)
    {
        if (size == 1)
            return (uint8_t)array[startIndex];
        QByteArray resultArray = array.mid(startIndex, size);
        QDataStream ds(resultArray);
        ds.setByteOrder(bigEndian? QDataStream::BigEndian: QDataStream::LittleEndian);
        if (size < 3)
        {
            int16_t result = 0;
            ds >> result;
            return result;
        }
        int32_t result = 0;
        ds >> result;
        return result;
    }
    static QString readString(const QByteArray& array, int startIndex, int size)
    {
        QByteArray resultArray = array.mid(startIndex, size);
        return stringFromByteArray(resultArray);
    }
    static QString readStringUTF8(const QByteArray& array, int startIndex, int size)
    {
        QByteArray resultArray = array.mid(startIndex, size);
        return stringFromByteArrayCp866(resultArray);
    }

    static QString trimmed(const QString& str)
    {
        int index = str.indexOf(QChar('\0'));
        if (index > 0)
            return str.left(index);
        return str;
    }
};
#endif // BYTEHELPER_H
