#ifndef DBFMODEL_H
#define DBFMODEL_H
#include "BaseDBClases.h"
#include "GameClases.h"

class DBFModel : public DataModel
{
public:
    DBFModel()
    {
    }
    DBFModel(const QString& path)
    {
        load(path);
    }

    void load(const QString& path)
    {
        m_tables.clear();
        registerTable<Gaction>(path + "/Globals/Gaction.DBF");
        registerTable<GAI>(path + "/Globals/GAI.dbf");
        registerTable<Gattack>(path + "/Globals/Gattacks.dbf");
        registerTable<Gbuild>(path + "/Globals/Gbuild.DBF");
        registerTable<GbuiList>(path + "/Globals/GbuiList.dbf");
        registerTable<GcityInf>(path + "/Globals/GcityInf.dbf");
        registerTable<GDynUpgr>(path + "/Globals/GDynUpgr.DBF");
        registerTable<Gimmu>(path + "/Globals/Gimmu.dbf");
        registerTable<GimmuC>(path + "/Globals/GimmuC.dbf");
        registerTable<GimmuC>(path + "/Globals/GimmuC.dbf");
        registerTable<GItem>(path + "/Globals/GItem.DBF", 1);
        registerTable<GLabi>(path + "/Globals/GLabi.dbf");
        registerTable<GleaUpg>(path + "/Globals/GleaUpg.dbf");
        registerTable<GLmark>(path + "/Globals/GLmark.dbf");
        registerTable<Glord>(path + "/Globals/Glord.dbf");
        registerTable<GMabi>(path + "/Globals/GMabi.dbf");
        registerTable<Gmodif>(path + "/Globals/Gmodif.dbf");
        registerTable<GmodifL>(path + "/Globals/GmodifL.dbf");
        registerTable<Grace>(path + "/Globals/Grace.dbf");
        registerTable<GspellR>(path + "/Globals/GspellR.dbf", 1);
        registerTable<GSpell>(path + "/Globals/Gspells.dbf");
        registerTable<GSubRace>(path + "/Globals/GSubRace.dbf");
        registerTable<GTileDBI>(path + "/Globals/GTileDBI.dbf");
        registerTable<Gtransf>(path + "/Globals/Gtransf.dbf");
        registerTable<Gunit>(path + "/Globals/Gunits.dbf");
        registerTable<GVars>(path + "/Globals/GVars.DBF");
        registerTable<Laction>(path + "/Globals/Laction.dbf");
        registerTable<LaiAtt>(path + "/Globals/LaiAtt.dbf");
        registerTable<LAiMsg>(path + "/Globals/LAiMsg.dbf");
        registerTable<LAiSpell>(path + "/Globals/LAiSpell.dbf");
        registerTable<LattC>(path + "/Globals/LattC.dbf");
        registerTable<LAttR>(path + "/Globals/LAttR.dbf");
        registerTable<LattS>(path + "/Globals/LattS.dbf");
        registerTable<Lbuild>(path + "/Globals/Lbuild.dbf");
        registerTable<Ldiff>(path + "/Globals/Ldiff.dbf");
        registerTable<LDthAnim>(path + "/Globals/LDthAnim.dbf");
        registerTable<LEvCond>(path + "/Globals/LEvCond.DBF");
        registerTable<LEvEffct>(path + "/Globals/LEvEffct.DBF");
        registerTable<LFort>(path + "/Globals/LFort.dbf");
        registerTable<Lground>(path + "/Globals/Lground.DBF");
        registerTable<LImmune>(path + "/Globals/LImmune.dbf");
        registerTable<LleadA>(path + "/Globals/LleadA.dbf");
        registerTable<LleadC>(path + "/Globals/LleadC.dbf");
        registerTable<LLMCat>(path + "/Globals/LLMCat.dbf");
        registerTable<Llord>(path + "/Globals/Llord.dbf");
        registerTable<LmagItm>(path + "/Globals/LmagItm.dbf");
        registerTable<LmodifE>(path + "/Globals/LmodifE.dbf");
        registerTable<LModifS>(path + "/Globals/LModifS.dbf");
        registerTable<LOrder>(path + "/Globals/LOrder.DBF");
        registerTable<Lrace>(path + "/Globals/Lrace.dbf");
        registerTable<Lres>(path + "/Globals/Lres.dbf");
        registerTable<Lsite>(path + "/Globals/Lsite.dbf");
        registerTable<Lspell>(path + "/Globals/Lspell.DBF");
        registerTable<LSplPhas>(path + "/Globals/LSplPhas.dbf");
        registerTable<LSubRace>(path + "/Globals/LSubRace.dbf");
        registerTable<Lterrain>(path + "/Globals/Lterrain.dbf");
        registerTable<Ltreaty>(path + "/Globals/Ltreaty.DBF");
        registerTable<LunitB>(path + "/Globals/LunitB.dbf");
        registerTable<LunitC>(path + "/Globals/LunitC.dbf");
        registerTable<TAiMsg>(path + "/Globals/TAiMsg.dbf");
        registerTable<Tglobal>(path + "/Globals/Tglobal.dbf");
        registerTable<Tleader>(path + "/Globals/Tleader.dbf");
        registerTable<Tplayer>(path + "/Globals/Tplayer.dbf");
        registerTable<TApp>(path + "/Interf/TApp.dbf");
        registerTable<TAppEdit>(path + "/Interf/TAppEdit.DBF");
        m_path = path;
    }

    void save(const QString& path)
    {
        foreach( QSharedPointer<ITable> table, m_tables)
        {
            if(table.isNull())
                continue;
            table->save(path);
        }
    }

    const QString &path() const;

private:
    QString m_path;
};

inline const QString &DBFModel::path() const
{
    return m_path;
}

#endif // DBFMODEL_H
