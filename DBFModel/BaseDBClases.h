#ifndef BASEDBCLASES_H
#define BASEDBCLASES_H
#include <QString>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QElapsedTimer>
#include <QCache>
#include <unordered_map>
#include "SimpleDBFAdapter.h"

// Optimized version with inline implementation
inline bool isNullId(const QString& id)
{
    const QString& trimmed = id.trimmed();
    return trimmed.isEmpty() || trimmed.toLower() == "g000000000";
}

class DataModel;

class Link
{
public:
    virtual ~Link() = default;
    virtual void read(const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) = 0;
    virtual QString getKey() const = 0;
};


class GameData
{
public:
    virtual ~GameData() = default;
protected:
    void readValue(int& field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row)
    {
        field = adapter.getInt(name, row, 0);
    }
    
    void readValue(bool& field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row, bool defaultVal = false)
    {
        field = adapter.getBool(name, row, defaultVal);
    }
    
    void readValue(QString& field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row)
    {
        field = adapter.getString(name, row);
    }
    
    void readValue(Link& field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row)
    {
        field.read(name, adapter, model, row);
    }

    void writeValue(int field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) const
    {
        adapter.set(name, row, QString::number(field));
    }
    
    void writeValue(bool field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) const
    {
        adapter.set(name, row, field ? "T" : "F");
    }
    
    void writeValue(const QString& field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) const
    {
        adapter.set(name, row, field);
    }
    
    void writeValue(const Link& field, const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) const
    {
        adapter.set(name, row, field.getKey());
    }
};

class ITable
{
public:
    virtual ~ITable() = default;
    virtual void save(const QString& path) = 0;
    virtual void clearCache() = 0;
};

template <class classT>
class BaseTable: public ITable
{
public:
    BaseTable() = default;
    
    BaseTable(const QString& fileName, int keyIndex_ = 0)
    {
        m_keyIndex = keyIndex_;
        load(fileName);
    }
    
    void load(const QString& fileName)
    {
        m_adapter.readDBFFile(fileName);
        QFileInfo fi(fileName);
        m_name = fi.baseName();
        m_path = fi.absoluteDir().path();
        
        // Pre-build index for faster lookups
        buildKeyIndex();
    }

    virtual void save(const QString& path) override
    {
        m_adapter.save(path);
    }
    
    // Build an index for faster key lookups
    void buildKeyIndex()
    {
        m_keyIndexMap.clear();
        const int rowCount = m_adapter.rowCount();
        m_keyIndexMap.reserve(rowCount);
        
        for (int i = 0; i < rowCount; ++i)
        {
            QString rowId = m_adapter.getString(i, m_keyIndex).toUpper().trimmed();
            m_keyIndexMap[rowId] = i;
        }
    }

    int indexByKey(const QString& key)
    {
        // Use the pre-built index for O(1) lookup instead of O(n)
        const QString upperKey = key.toUpper().trimmed();
        auto it = m_keyIndexMap.find(upperKey);
        if (it != m_keyIndexMap.end())
            return it.value();
        return -1;
    }
    
    QSharedPointer<classT> get(const QString& key, DataModel* model)
    {
        if (isNullId(key))
            return QSharedPointer<classT>();
            
        // Check cache first
        if (m_cashedData.contains(key))
        {
            return m_cashedData[key];
        }
        
        int index = indexByKey(key);
        if (index == -1)
            return nullptr;
            
        QSharedPointer<classT> result(new classT);
        m_cashedData.insert(key, result);
        result->read(m_adapter, model, index);
        return result;
    }
    
    QSharedPointer<classT> get(int index, DataModel* model)
    {
        if (index < 0 || index >= m_adapter.rowCount())
            return nullptr;
            
        // Create a unique key for this index-based lookup
        QString rowId = QString::number(index) + ":" +
                m_adapter.getString(index, m_keyIndex).toUpper().trimmed();
                
        // Check cache first
        if (m_cashedData.contains(rowId))
        {
            return m_cashedData[rowId];
        }
        
        QSharedPointer<classT> result(new classT);
        m_cashedData.insert(rowId, result);
        result->read(m_adapter, model, index);
        return result;
    }

    QVector<QSharedPointer<classT>> getList(const QString& key, DataModel* model)
    {
        if (key.isEmpty())
            return getAllList(model);
            
        QVector<QSharedPointer<classT>> result;
        const int rowCount = m_adapter.rowCount();
        
        // Pre-allocate memory to avoid reallocations
        result.reserve(rowCount / 4); // Estimate 25% of rows will match
        
        const QString upperKey = key.toUpper();
        
        for (int i = 0; i < rowCount; ++i)
        {
            QString rowId = m_adapter.getString(i, m_keyIndex).toUpper();
            if (rowId == upperKey)
            {
                result << get(i, model);
            }
        }
        
        return result;
    }

    QVector<QSharedPointer<classT>> getAllList(DataModel* model)
    {
        const int rowCount = m_adapter.rowCount();
        QVector<QSharedPointer<classT>> result;
        result.reserve(rowCount);

        for (int i = 0; i < rowCount; ++i)
        {
            result << get(i, model);
        }

        return result;
    }

    QVector<QString> getKeysList(const QString& key, DataModel* model)
    {
        Q_UNUSED(model);
        const int rowCount = m_adapter.rowCount();
        QVector<QString> result;
        
        // Pre-allocate memory
        if (key.isEmpty())
            result.reserve(rowCount);
        else
            result.reserve(rowCount / 4); // Estimate 25% of rows will match
            
        const QString upperKey = key.toUpper();
        
        for (int i = 0; i < rowCount; ++i)
        {
            QString rowId = m_adapter.getString(i, m_keyIndex).toUpper();
            if (key.isEmpty() || rowId == upperKey)
            {
                result << rowId;
            }
        }
        return result;
    }

    bool update(QSharedPointer<classT> newValue, DataModel* model)
    {
        if (newValue.isNull())
            return true;
            
        const QString& key = newValue->key();
        
        if (m_cashedData.contains(key))
        {
            // Update cache directly without remove/insert
            m_cashedData[key] = newValue;
        }
        
        int index = indexByKey(key);
        if (index != -1)
        {
            newValue->write(m_adapter, model, index);
            return true;
        }
        
        return false;
    }
    
    // Clear cache to free memory
    void clearCache() override
    {
        m_cashedData.clear();
    }
    
protected:
    SimpleDBFAdapter m_adapter;
    QString m_path;
    QString m_name;
    QHash<QString, QSharedPointer<classT>> m_cashedData;
    QHash<QString, int> m_keyIndexMap; // For O(1) key lookups
    int m_keyIndex = 0;
};

class DataModel
{
public:
    DataModel() = default;

    template <class classT>
    void registerTable(const QString& path)
    {
        m_tables.insert(classT::typeName(), QSharedPointer<ITable>(new BaseTable<classT>(path)));
    }
    
    template <class classT>
    void registerTable(const QString& path, int keyIndex)
    {
        m_tables.insert(classT::typeName(), QSharedPointer<ITable>(new BaseTable<classT>(path, keyIndex)));
    }

    template <class classT>
    QSharedPointer<classT> get(const QString& key)
    {
        const QString& table = classT::typeName();
        return getTableT<classT>(table)->get(key, this);
    }

    template <class classT>
    QVector<QSharedPointer<classT>> getList(const QString& key = QString())
    {
        const QString& table = classT::typeName();
        return getTableT<classT>(table)->getList(key, this);
    }

    template <class classT>
    QVector<QString> getKeysList(const QString& key = QString())
    {
        const QString& table = classT::typeName();
        return getTableT<classT>(table)->getKeysList(key, this);
    }

    template <class classT>
    bool update(QSharedPointer<classT> key)
    {
        const QString& table = classT::typeName();
        return getTableT<classT>(table)->update(key, this);
    }
    
    // Clear all caches to free memory
    void clearAllCaches()
    {
        for (auto it = m_tables.begin(); it != m_tables.end(); ++it)
        {
            it.value()->clearCache();
        }
    }
    
protected:
    template <class classT>
    BaseTable<classT>* getTableT(const QString& tableName)
    {
        return static_cast<BaseTable<classT>*>(m_tables[tableName].data());
    }
    
protected:
    QHash<QString, QSharedPointer<ITable>> m_tables;
};

template <class classT>
class StringLink: public Link
{
public:
    QString key;
    QSharedPointer<classT> value;
    
    classT* operator ->() { return value.data(); }
    
    virtual void read(const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) override
    {
        key = adapter.getString(name, row);
        value = model->get<classT>(key);
    }
    
    virtual QString getKey() const override
    {
        return key;
    }
};

template <class classT>
class StringLinkList: public Link
{
public:
    QString m_key;
    QVector<QSharedPointer<classT>> m_value;
    
    virtual void read(const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) override
    {
        m_key = adapter.getString(name, row);
        m_value = model->getList<classT>(m_key);
    }
    
    virtual QString getKey() const override
    {
        return m_key;
    }
};

template <class classT>
class IntLink: public Link
{
public:
    int key;
    QSharedPointer<classT> value;
    
    classT* operator ->() { return value.data(); }
    
    virtual void read(const QString& name, SimpleDBFAdapter& adapter, DataModel* model, int row) override
    {
        key = adapter.getInt(name, row);
        value = model->get<classT>(getKey());
    }
    
    virtual QString getKey() const override
    {
        return QString::number(key);
    }
};

#endif // BASEDBCLASES_H
