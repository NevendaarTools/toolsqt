#ifndef RESOURCEMODEL_H
#define RESOURCEMODEL_H
#include <QHash>
#include "GameResource.h"

class ResourceModel
{
public:
    ResourceModel();
//    bool hasResource(const QString& id)
//    {
//        foreach (const GameResource& source, resources.values())
//            if (source.HasResource(id))
//                return true;
//        return false;
//    }

//            bool hasResource(const QString& resourceName, const QString&  id)
//            {
//                if (resources.ContainsKey(resourceName))
//                {
//                    return (resources[resourceName].HasResource(id));
//                }
//                return false;
//            }
    void init(const QString & path)
    {
        loadResourceDesc(path + "/Imgs", "BatItems");
        loadResourceDesc(path + "/Imgs", "Battle");
        loadResourceDesc(path + "/Imgs", "BatUnits");
        loadResourceDesc(path + "/Imgs", "Capital");
        loadResourceDesc(path + "/Imgs", "City");
        loadResourceDesc(path + "/Imgs", "Events");
        loadResourceDesc(path + "/Imgs", "Faces");
        loadResourceDesc(path + "/Imgs", "GrBorder");
        loadResourceDesc(path + "/Imgs", "Ground");
        loadResourceDesc(path + "/Imgs", "IconBld");
        loadResourceDesc(path + "/Imgs", "IconItem");
        loadResourceDesc(path + "/Imgs", "Icons");
        loadResourceDesc(path + "/Imgs", "IconSpel");
        loadResourceDesc(path + "/Imgs", "IsoAnim");
        loadResourceDesc(path + "/Imgs", "IsoCmon");
        loadResourceDesc(path + "/Imgs", "IsoCursr");
        loadResourceDesc(path + "/Imgs", "IsoSpell");
        loadResourceDesc(path + "/Imgs", "IsoStill");
        loadResourceDesc(path + "/Imgs", "IsoTerrn");
        loadResourceDesc(path + "/Imgs", "IsoUnit");
        loadResourceDesc(path + "/Imgs", "Lords");
        loadResourceDesc(path + "/Imgs", "PalMap");
        loadResourceDesc(path + "/Imgs", "Thief");

        loadResourceDesc(path + "/Interf", "Interf");
        loadResourceDesc(path + "/Interf", "IntfScen");
        loadResourceDesc(path + "/Interf", "MenuAnim");
        loadResourceDesc(path + "/Interf", "MenuImgs");
        //LoadResourceDesc(path + "/Imgs", "Wrapper");
        //LoadResourceDesc(path + "/Imgs", "IsoClouds");
    }
    bool loadResourceDesc(const QString& path, const QString& name)
    {
        QString resultName = path + "/" + name + ".ff";
        GameResource resource;
        if (!resource.read(resultName))
            return false;
        resources.insert(name, resource);
        return true;
    }

    QList<QImage> getFramesById(const QString& resource, const QString& id, PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation)
    {
        if (!resources.contains(resource))
            return QList<QImage>();
        return resources[resource].getFramesById(id, shader);
    }

    QImage getFrameById(const QString& resource, const QString& id, PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation)
    {
        if (!resources.contains(resource))
            return QImage();
        QList<QImage> res = resources[resource].getFramesById(id, shader, true);
        if (res.count() > 0)
            return res[0];
        return QImage();
    }

    QList<QImage> getFramesById(const QStringList& resources, const QString& id, PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation)
    {
        QList<QImage> result;
        foreach(const QString& resource, resources)
        {
            result.append(getFramesById(resource, id, shader));
            if (result.count() > 0)
                return result;
        }
        return result;
    }

    QImage getFrameById(const QStringList& resources, const QString& id, PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation)
    {
        foreach(const QString& resource, resources)
        {
            QImage im = getFrameById(resource, id, shader);
            if (!im.isNull())
                return im;
        }
        return QImage();
    }

    QList<QImage> getFramesById_deprecated(const QString& id, PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation)
    {
        foreach(const QString& key, resources.keys())
        {
            QList<QImage> tmp = resources[key].getFramesById(id, shader);
            if (tmp.count() > 0)
                return tmp;
        }

        return QList<QImage>();
    }

private:
    QHash<QString, GameResource> resources;
};

#endif // RESOURCEMODEL_H
