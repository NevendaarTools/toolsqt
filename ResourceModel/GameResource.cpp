#include "GameResource.h"
#include "../Common/Logger.h"

inline bool isTransparentBlackColor2(int r, int g, int b)
{
    return (r < 5 && g < 5 && b < 5);
}
inline bool isTransparentColor2(int r, int g, int b)
{
    return (r > 247 && b > 247 && g < 8);
}

inline bool isTransparentBorderColor2(int r, int g, int b)
{
    return (r > 250 && b > 250 && g < 5) ||
           (r > 250 && b > 250 && g > 250);
}


void fill(QImage & frame, ImagesContainer::ImageRestoreData & restoreData, QImage & sourceImage)
{
    if (restoreData.parts.count() > 0)
    {
        QPainter painter(&frame);
        foreach (const ImagesContainer::ImageRestoreData::ImagePart & data, restoreData.parts)
        {
            painter.drawImage(QRect(data.sourceX, data.sourceY, data.w, data.h),
                              sourceImage,
                              QRect(data.targetX, data.targetY, data.w, data.h));
        }
    }
}

void fill_indexed(QImage &frame, ImagesContainer::ImageRestoreData &restoreData, QImage & sourceImage)
{
    if (restoreData.parts.count() > 0)
    {
        frame.setColorTable(sourceImage.colorTable());
        uchar *frameBits = frame.bits();
        int frameBytesPerLine = frame.bytesPerLine();
        const uchar *sourceBits = sourceImage.bits();
        int sourceBytesPerLine = sourceImage.bytesPerLine();

        foreach (const ImagesContainer::ImageRestoreData::ImagePart &data, restoreData.parts)
        {
            for (int y = 0; y < data.h; ++y)
            {
                for (int x = 0; x < data.w; ++x)
                {
                    int sourceIndex = (data.targetY + y) * sourceBytesPerLine + (data.targetX + x);
                    int frameIndex = (data.sourceY + y) * frameBytesPerLine + (data.sourceX + x);
                    frameBits[frameIndex] = sourceBits[sourceIndex];
                }
            }
        }
    }
}

void make_transp_indexed(QImage &frame, bool black, bool purple, QColor color)
{
    auto colors = frame.colorTable();
    colors[0] = color.rgba();

    for (int i = 1; i < colors.size(); ++i) {
        QColor color = QColor::fromRgba(colors[i]);
        int r = color.red();
        int g = color.green();
        int b = color.blue();

        if (black && isTransparentBlackColor2(r, g, b) || purple && isTransparentColor2(r, g, b)) {
            colors[i] = color.rgba();
        }
    }

    frame.setColorTable(colors);
}

void GameResource::processParts(ResourceDesc * resDesc, ImagesContainer::ImageRestoreData & restoreData,
                  QList<QImage> & result, PreprocessingShaderType shader)
{
    if (restoreData.parts.count() == 0)
        return;
    QByteArray array = resDesc->getData(this->path);
    QImage sourceImage = QImage::fromData(array,"PNG");
    QImage frame;
    if (sourceImage.format() == QImage::Format_Indexed8/* && false*/) {
        frame = QImage(restoreData.w, restoreData.h, QImage::Format_Indexed8);
        frame.fill(QColor(255,0,255));
        fill_indexed(frame, restoreData, sourceImage);
        make_transp_indexed(frame, (shader & PreprocessingShaderType::TransparentBlack), true, QColor(255,0,255));
        frame.convertTo(QImage::Format_ARGB32);
        // replaceMagentaWithTransparent(frame);
        result.append(shaderProcessedPixmap(frame, shader));
        // result.append(frame);
    }  else {
        sourceImage.convertTo(QImage::Format_ARGB32);
        frame = QImage(restoreData.w, restoreData.h, QImage::Format_ARGB32);
        frame.fill(Qt::transparent);
        fill(frame, restoreData, sourceImage);
        result.append(shaderProcessedPixmap(frame, shader));
    }
}

void GameResource::clear()
{
    names_desc = nullptr;
    images_opt = nullptr;
    indexes_opt = nullptr;
    anim_opt = nullptr;
    header.clear();
    resources.clear();
}

bool GameResource::read(const QString &filename)
{
    clear();
    QFile headerFile(filename);
    if (!headerFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "failed to open file: " + filename;
        return false;
    }
    path = filename;
    long index = 28;
    header = headerFile.read(28);
    while (index < headerFile.size())
    {
        if (!headerFile.seek(index))
        {
            qDebug()<<"Failed to seek file: " + filename;
            return false;
        }
        ResourceDesc * desc = new ResourceDesc();
        index = 28 + desc->read(headerFile);
        if (desc->uid == 2)
            names_desc = desc;
        resources.append(desc);
    }

    restoreNames();
    return true;
}

bool GameResource::read2(const QString &filename)
{
    clear();
    QFile headerFile(filename);
    if (!headerFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "failed to open file: " + filename;
        return false;
    }
    path = filename;
    long index = 0;
    header;// = headerFile.read(28);
    QByteArray data = headerFile.readAll();
    static const QByteArray typeEnd = QString("MQRC").toUtf8();

    while (index < headerFile.size() && index > -1)
    {
        int startIndex = index;
        index = data.indexOf(typeEnd, startIndex + 1);
        if (index < 0)
        {
            ResourceDesc * desc = new ResourceDesc();
            desc->offset = startIndex;
            desc->size = data.size() - startIndex;
            desc->data = new CustomByteData();
            resources.append(desc);
            break;
        }
        ResourceDesc * desc = new ResourceDesc();
        desc->offset = startIndex;
        desc->size = index - startIndex;
        desc->data = new CustomByteData();
        resources.append(desc);
    }

    restoreNames();
    return true;
}

QString GameResource::getPath() const
{
    return path;
}

ResourceDesc *GameResource::ResDescByUid(int uid)
{
    for (int k = 0; k < resources.count(); ++k)
    {
        if (resources[k]->uid == uid)
        {
            return resources[k];
        }
    }
    return nullptr;
}

ResourceDesc *GameResource::ResDescByName(const QString &name)
{
    for (int k = 0; k < resources.count(); ++k)
    {
        if (resources[k]->name.isEmpty())
            continue;
        QString tmp = resources[k]->name;
        tmp.replace(".PNG","");
        if (tmp.toUpper() == name.toUpper())
        {
            return resources[k];
        }
    }
    return nullptr;
}

QList<QString> GameResource::GetAllDescs()
{
    QList<QString> result;
    for (int k = 0; k < resources.count(); ++k)
    {
        if (resources[k]->name.isEmpty())
            continue;
        QString tmp = resources[k]->name;
        result.append(tmp.replace(".PNG", ""));
    }
    return result;
}

QList<QString> GameResource::getIndexNames()
{
    QList<QString> result;
    if (indexes_opt == nullptr)
        return result;
    IndexesData * indexesData = static_cast<IndexesData*>(indexes_opt->data);
    for (int i = 0; i < indexesData->indexesData.count(); ++i)
    {
        result << indexesData->indexesData[i].name;
    }
    return result;
}

IndexesData::IndexesDesc GameResource::GetIndexDescByName(const QString &name)
{
    if (indexes_opt == nullptr)
        return IndexesData::IndexesDesc();
    IndexesData * indexesData = static_cast<IndexesData*>(indexes_opt->data);
    for (int i = 0; i < indexesData->indexesData.count(); ++i)
    {
//        if (indexesData->indexesData[i].uid == -1)
//            qDebug()<<indexesData->indexesData[i].name.toLower();
        if (indexesData->indexesData[i].name.toLower() == name.toLower())
        {
            return indexesData->indexesData[i];
        }
    }
    return IndexesData::IndexesDesc();
}

QImage GameResource::getSimpleImageByName(const QString &id, PreprocessingShaderType shader)
{
    static QByteArray end = QByteArrayLiteral("\0\0\0\0IEND\0B`\202");
    end[8] = -82;
    ResourceDesc * resDesc = ResDescByName(id);//names was restored on load
    if (resDesc == nullptr)
        return QImage();
    QByteArray data = resDesc->getData(this->path);
    if (data.length() < 10)
        return QImage();
    data.append(end);
    QImage pix;
    pix.loadFromData(data);
    return shaderProcessedPixmap(pix, shader);
}

bool GameResource::hasResource(const QString &name)
{
    if (indexes_opt == nullptr)//only mqdb in file
    {
        ResourceDesc * resDesc = ResDescByName(name);//names was restored on load
        return (resDesc != nullptr);
    }
    IndexesData::IndexesDesc indexDesc = GetIndexDescByName(name);
    if (indexDesc.name.isEmpty())
        return false;

    if (indexDesc.uid != -1)//simple image with restore data
    {
        return (images_opt != nullptr);
    }
    else //indexDesc.uid == -1, this is animation
    {
        return (anim_opt != nullptr);
    }
}

void makeImageTransparent(QImage &before, std::function<bool(int r, int g, int b)> isTransparentColorFunction)
{
    if (before.pixelFormat().colorModel() == QPixelFormat::ColorModel::Indexed)
    {
        for(int i = 0; i < before.colorCount(); ++i)
        {
            QColor color = before.color(i);
            if (isTransparentColorFunction(color.red(), color.green(), color.blue()))
            {
                before.setColor(i, Qt::transparent);
            }
        }
    }
    else
    {
        if (before.format() != QImage::Format_ARGB32)
        {
            QColor color;

            for(int y = 0; y < before.height(); y++)
            {
                for(int x = 0; x < before.width(); x++)
                {
                    color = before.pixelColor(x,y);
                    if (isTransparentColorFunction(color.red(), color.green(), color.blue()))
                    {
                        color.setAlpha(0);
                        before.setPixelColor(x,y, color);
                    }
                }
            }
            return;
        }
        uchar* resultBits = before.bits();
        for(int y = 0; y < before.height(); y++)
        {
            for(int x= 0; x < before.width(); x++)
            {
                int index = (y * before.width() + x) * 4;
                if (isTransparentColorFunction(resultBits[index + 0], resultBits[index + 1], resultBits[index + 2]))
                {
                    resultBits[index + 3] = 0;
                }
            }
        }
    }
}
QImage GameResource::shaderProcessedPixmap(QImage &before,
                                           PreprocessingShaderType shader)
{
    if (before.format() == QImage::Format_RGB32  &&
        !(shader & PreprocessingShaderType::NoTransparentActions))
    {
        before.convertTo(QImage::Format_ARGB32);
    }
    if (shader & PreprocessingShaderType::Default)
    {
        makeImageTransparent(before, isTransparentColor2);
    }
    if (shader & PreprocessingShaderType::TransparentBlack)
    {
        makeImageTransparent(before, isTransparentBlackColor2);
    }
    if (shader & PreprocessingShaderType::Shadows)
    {
        if (before.pixelFormat().colorModel() == QPixelFormat::ColorModel::Indexed)
        {
            for(int i = 0; i < before.colorCount(); ++i)
            {
                QColor color = before.color(i);
                if (isTransparentColor2(color.red(), color.green(), color.blue()))
                {
                    before.setColor(i, Qt::transparent);
                }
                else if (isTransparentBlackColor2(color.red(), color.green(), color.blue()) && color.alpha() > 0)
                {
                    color.setAlpha(127);
                    before.setColor(i, color.rgb());
                }
            }
        }
        else
        {
            QColor color;

            for(int y = 0; y < before.height(); y++)
            {
                for(int x = 0; x < before.width(); x++)
                {
                    color = before.pixelColor(x,y);
                    if (isTransparentColor2(color.red(), color.green(), color.blue()))
                    {
                        color.setAlpha(0);
                        before.setPixelColor(x,y, color);
                    }
                    else if (isTransparentBlackColor2(color.red(), color.green(), color.blue()) && color.alpha() > 0)
                    {
                        color.setAlpha(127);
                        before.setPixelColor(x,y, color);
                    }
                }
            }
        }
    }
    if (shader & PreprocessingShaderType::Border)
    {
        makeImageTransparent(before, isTransparentBorderColor2);
    }

    if (shader & PreprocessingShaderType::WithSizeOptimisation)
    {
        if (before.pixelFormat().colorModel() == QPixelFormat::ColorModel::Indexed)
            return before;//currently unsupported
        int minX = 0, maxX = before.width();
        int minY = 0, maxY = before.height();
        QColor color;

        for(int y = 0; y < before.height(); y++)
        {
            for(int x= 0; x < before.width(); x++)
            {
                color = before.pixelColor(x,y);
                if (color.alpha() > 5)
                {
                    if (x > minX) minX = x;
                    if (x < maxX) maxX = x;
                    if (y > minY) minY = y;
                    if (y < maxY) maxY = y;
                }
            }
        }
        int borderX = qMin(before.width() - minX, maxX);
        int borderY = qMin(before.height() - minY, maxY);
        if (borderX == before.width())
            borderX = 0;
        if (borderY == before.height())
            borderY = 0;
        QImage result(before.width() - borderX * 2, before.height() - borderY * 2, QImage::Format_ARGB32);
        result.fill(Qt::transparent);
        QPainter painter(&result);
        painter.drawImage(
            QRect(0, 0, result.width(), result.height()),
            before,
            QRect(borderX, borderY, result.width(), result.height()),
            Qt::AutoColor
        );
        return result;
    }
    return before;
}

QList<QImage> GameResource::getFramesById(const QString &id, PreprocessingShaderType shader, bool onlyFirst)
{
    QList<QImage> result;
    if (indexes_opt == nullptr)//only mqdb in file
    {
        QImage img = getSimpleImageByName(id, shader);
        if (!img.isNull())
            result.append(img);
        return result;
    }
    IndexesData::IndexesDesc indexDesc = GetIndexDescByName(id);
    if (indexDesc.name.isEmpty())
    {
        //TODO: check for removed
//        QImage img = getSimpleImageByName(id);
//        if (!img.isNull())
//            result.append(img);
        return result;
    }
    ImagesContainer * imagesCon = static_cast<ImagesContainer *>(images_opt->data);

    if (indexDesc.uid != -1)//simple image with restore data
    {
        if (images_opt == nullptr)//something wrong
            return result;
        ImagesContainer::ImageData imageInfo = imagesCon->getImageData(
                    images_opt->offset + 28 + indexDesc.relatedOffset,
                    indexDesc.relatedSize, this->path);

        ResourceDesc * resDesc = ResDescByUid(indexDesc.uid);
        if (resDesc == nullptr)//failed to find source image
            return result;
        if (imageInfo.header.isEmpty() || imageInfo.restoreData.count() < 1)
            return result;
        for (int i = 0; i < imageInfo.restoreData.count(); ++i)//might be only one, but...
        {
            if (imageInfo.restoreData[i].name != id)
                continue;
            ImagesContainer::ImageRestoreData restoreData = imageInfo.restoreData[i];
            processParts(resDesc, restoreData, result, shader);
            if (onlyFirst)
                break;
        }
        return result;
    }
    else //indexDesc.uid == -1, this is animation
    {
        AnimationsContainer * animData = static_cast<AnimationsContainer *>(anim_opt->data);
        AnimationsContainer::AnimationsDesc animDesc = animData->GetAnimationsDesc(
                    anim_opt->offset + 28 + indexDesc.relatedOffset,
                    indexDesc.relatedSize, this->path);
        ImagesContainer::ImageData imageData;
        for (int i = 0; i < animDesc.frameNames.count(); ++i)
        {
            IndexesData::IndexesDesc sourceDesc = GetIndexDescByName(animDesc.frameNames[i]);
            if (imageData.header.isEmpty() || imageData.localOffset != sourceDesc.relatedOffset)
            {
                imageData = imagesCon->getImageData(
                            images_opt->offset + 28 + sourceDesc.relatedOffset,
                            sourceDesc.relatedSize, this->path);
            }
            ImagesContainer::ImageRestoreData restoreData;
            for (int k = 0; k < imageData.restoreData.count(); ++k)
            {
                if (imageData.restoreData[k].name == animDesc.frameNames[i])
                {
                    restoreData = imageData.restoreData[k];
                }
            }
            if (restoreData.name.isEmpty())
                continue;

            ResourceDesc * resDesc = ResDescByUid(sourceDesc.uid);
            if (resDesc == nullptr)//failed to find source image
                return result;

            processParts(resDesc, restoreData, result, shader);
            if (onlyFirst)
                break;
        }
    }

    return result;
}




int GameResource::getFramesCountById(const QString &id)
{
    if (indexes_opt == nullptr)//only mqdb in file
    {
        ResourceDesc * resDesc = ResDescByName(id);//names was restored on load
        if (resDesc == nullptr)
            return 0;
        return 1;
    }
    IndexesData::IndexesDesc indexDesc = GetIndexDescByName(id);
    if (indexDesc.name.isEmpty())
    {
        ResourceDesc * resDesc = ResDescByName(id);//names was restored on load
        if (resDesc == nullptr)
            return 0;
        return 1;
    }
    ImagesContainer * imagesCon = static_cast<ImagesContainer *>(images_opt->data);

    if (indexDesc.uid != -1)//simple image with restore data
    {
        if (images_opt == nullptr)//something wrong
            return 0;
        ImagesContainer::ImageData imageInfo = imagesCon->getImageData(
                    images_opt->offset + 28 + indexDesc.relatedOffset,
                    indexDesc.relatedSize, this->path);

        return imageInfo.restoreData.count();
    }
    else //indexDesc.uid == -1, this is animation
    {
        AnimationsContainer * animData = static_cast<AnimationsContainer *>(anim_opt->data);
        AnimationsContainer::AnimationsDesc animDesc = animData->GetAnimationsDesc(
                    anim_opt->offset + 28 + indexDesc.relatedOffset,
                    indexDesc.relatedSize, this->path);

        return animDesc.frameNames.count();
    }
}

void GameResource::restoreNames()
{
    NamesListData * namesData = nullptr;
    for (int i = 0; i < resources.count(); ++i)
    {
        if (resources[i]->uid == 2)
        {
            namesData = new NamesListData();
            QByteArray data = resources[i]->getData(this->path);
            namesData->setData(data);
            resources[i]->data = namesData;
            names_desc = resources[i];
        }
    }
    if (namesData == nullptr)
        return;

    for (int i = 0; i < resources.count(); ++i)
    {
        for(int q = 0; q < namesData->bindings.count(); ++q)
        {
            if (resources[i]->uid == namesData->bindings[q].id)
            {
                resources[i]->name = namesData->bindings[q].name;
                if (images_opt == nullptr && resources[i]->name == "-IMAGES.OPT")
                {
                    resources[i]->data = new ImagesContainer();
                    images_opt = resources[i];
                }
                else if (anim_opt == nullptr && resources[i]->name == "-ANIMS.OPT")
                {
                    resources[i]->data = new AnimationsContainer();
                    anim_opt = resources[i];
                }
                else if (indexes_opt == nullptr && resources[i]->name == "-INDEX.OPT")
                {
                    IndexesData * indexesData = new IndexesData();
                    indexesData->setData(resources[i]->getData(this->path));
                    resources[i]->data = indexesData;
                    indexes_opt = resources[i];
                }
                break;
            }
        }
    }
}

bool GameResource::Write(const QString &name)
{

    return true;
}

const QList<ResourceDesc *> &GameResource::getResources() const
{
    return resources;
}
