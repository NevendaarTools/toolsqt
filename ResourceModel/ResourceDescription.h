#ifndef RESOURCEDESCRIPTION_H
#define RESOURCEDESCRIPTION_H
#include <QByteArray>
#include <QFile>
#include <QDebug>
#include <QSharedPointer>
#include "ByteUtills.h"

class MQRCCustomData
{
public:
    virtual ~MQRCCustomData(){}
    virtual QByteArray getData(const QString & filename, int pos, int size) = 0;
    virtual bool setData(const QByteArray& data) = 0;
};

class CustomByteData :public  MQRCCustomData
{
public:
    QByteArray data;
    virtual QByteArray getData(const QString & filename, int pos, int size) override
    {
        if (!data.isEmpty())
            return data;
        QFile headerFile(filename);
        if (!headerFile.open(QIODevice::ReadOnly))
        {
            qDebug() << "failed to open file: " + filename;
            return data;
        }
        if (!headerFile.seek(pos))
        {
            qDebug()<<"Failed to seek file: " + filename;
            return data;
        }
        QByteArray byteData = headerFile.read(size);
        return byteData;
    }
    virtual bool setData(const QByteArray& data )override
    {
        this->data = data;
        return true;
    }
};

class ResourceDesc
{
public:
    ResourceDesc(){data = nullptr;}
    int32_t uid;
    int offset;
    int size;
    int32_t magic;//0
    int32_t magic2;//size duplicate
    int32_t magic3;//1
    int32_t magic4;//0
    MQRCCustomData* data;

    QString name;//exra data, restored from "2" data block

    QString ToQString()
    {
        return name + "   " + QString::number(uid) + " pos = " + QString::number(offset) + " - " + QString::number(size);
    }

    long read(QFile & file)
    {
        offset = (int)file.pos();
        file.seek(offset + 4);//"MQRC"
        file.read((char*)(&magic), sizeof(magic));
        file.read((char*)(&uid), sizeof(uid));
        file.read((char*)(&size), sizeof(size));
        file.read((char*)(&magic2), sizeof(magic2));
        file.read((char*)(&magic3), sizeof(magic3));
        file.read((char*)(&magic4), sizeof(magic4));//end of 28 bytes header
        data = new CustomByteData();

        return offset + size;
    }

    QByteArray getData(QString filename)
    {
        return data->getData(filename, offset + 28, size);
    }

    bool Write(QFile & file)
    {

        return true;
    }
};


class NamesListData : public MQRCCustomData
{
public:
    struct Binding
    {
        int32_t id;
        QString name;
        QString ToString()
        {
            return QString::number(id) + " - " + name;
        }
    };

    QList<Binding> bindings;

    virtual QByteArray getData(const QString & filename, int pos, int size) override
    {
        return QByteArray();
    }

    virtual bool setData(const QByteArray& newData)override
    {
        for (int k = 4; k < newData.length(); k += 260)
        {
            int hIndex = k;
            QString name = readString(newData, hIndex, 256);
            name = name.remove(QChar('\0'));
            hIndex = k + 256;
            int32_t id = readInt(newData, hIndex, 4);
            Binding binding;
            binding.name = name;
            binding.id = id;
            bindings.append(binding);
        }

        return true;
    }
};

class IndexesData : public MQRCCustomData
{
public:
    class IndexesDesc
    {
    public:
        int32_t uid;
        QString name;
        QString imageName;
        int relatedOffset;//in imgeMap or anim map
        int relatedSize;
        QString ToString()
        {
            return "index: " + QString::number(uid) + "  name: " + name;
        }
    };
    QList<IndexesDesc> indexesData;
    virtual QByteArray getData(const QString & filename, int pos, int size) override
    {
        return QByteArray();
    }

    virtual bool setData(const QByteArray& newData)override
    {
        int index = 0;
        int count = readInt(newData, index, 4);
        for (int i = 0; i < count; ++i)
        {
            IndexesDesc indDesc;
            indDesc.uid = readInt(newData, index, 4);
            indDesc.name = readStringWithTerm(newData, index);
            indDesc.relatedOffset = readInt(newData, index, 4);
            indDesc.relatedSize = readInt(newData, index, 4);
            indexesData.append(indDesc);
        }

        return true;
    }
};

class AnimationsContainer : public MQRCCustomData
{
public:
    class AnimationsDesc
    {
    public:
        QList<QString> frameNames;
        int localOffset;
        int size;
        QString ToString()
        {
            return "Frames: " + frameNames.join(" ");
        }
    };
    QList<AnimationsDesc> animationsData;
    virtual QByteArray getData(const QString & filename, int pos, int size) override
    {
        return QByteArray();
    }

    AnimationsDesc read(int & index, const QByteArray& newData)
    {
        AnimationsDesc resultData;
        resultData.localOffset = index;
        int count = readInt(newData, index, 4);
        for (int i = 0; i < count; ++i)
        {
            resultData.frameNames.append(readStringWithTerm(newData, index));
        }
        resultData.size = index - resultData.localOffset;
        return resultData;
    }

    AnimationsDesc GetAnimationsDesc(int offset, int size, const QString& path)
    {
        QFile headerFile(path);
        if (!headerFile.open(QIODevice::ReadOnly))
        {
            qDebug() << "failed to open file: " + path;
            return AnimationsDesc();
        }
        if (!headerFile.seek(offset))
        {
            qDebug()<<"Failed to seek file: " + path;
            return AnimationsDesc();
        }
        QByteArray byteData = headerFile.read(size);
        int tmpOffset = 0;
        return read(tmpOffset, byteData);
    }

    virtual bool setData(const QByteArray& data)
    {
        int index = 0;
        while (index < data.length())
        {
            animationsData.append(read(index, data));
        }

        return true;
    }
};

class ImagesContainer : public MQRCCustomData
{
public:
    class ImageRestoreData
    {
    public:
        struct ImagePart
        {
            int sourceX;
            int sourceY;
            int targetX;
            int targetY;
            int w;
            int h;
        };
        int w;
        int h;
        QString name;
        QList<ImagePart> parts;
        QString ToString()
        {
            return name + " - [" + QString::number(w) + "x" + QString::number(h) + "]  parts: " + QString::number(parts.count());         }
    };

    class ImageData
    {
    public:
        QList<ImageRestoreData> restoreData;
        QByteArray header;// = new byte[11 + 1024];
        int localOffset;
        int localSize;
        QString ToString()
        {
            return "Frames count: " + QString::number(restoreData.count());
        }
    };

    ImageData getImageData(int offset, int size, const QString & path)
    {
        QFile headerFile(path);
        if (!headerFile.open(QIODevice::ReadOnly))
        {
            qDebug() << "failed to open file: " + path;
            return ImageData();
        }
        if (!headerFile.seek(offset))
        {
            qDebug()<<"Failed to seek file: " + path;
            return ImageData();
        }
        QByteArray byteData = headerFile.read(size);
        int tmpOffset = 0;
        return read(tmpOffset, byteData);
    }
    QList<ImageData> imagesData;
    virtual QByteArray getData(const QString & filename, int pos, int size) override
    {
        return QByteArray();
    }

    ImageData read(int & index, const QByteArray& newData)
    {
        int headerSize = 11 + 1024;
        ImageData resultData;
        resultData.localOffset = index;
        index += headerSize;
        resultData.header = newData.mid(0, headerSize);
        int imagesCount = readInt(newData, index, 4);
        for (int k = 0; k < imagesCount; ++k)
        {
            ImageRestoreData restoreData;
            restoreData.name = readStringWithTerm(newData, index);
            int count = readInt(newData, index, 4);
            restoreData.w = readInt(newData, index, 4);
            restoreData.h = readInt(newData, index, 4);
            for (int i = 0; i < count; ++i)
            {
                ImageRestoreData::ImagePart part;
                part.sourceX = readInt(newData, index, 4);
                part.sourceY = readInt(newData, index, 4);
                part.targetX = readInt(newData, index, 4);
                part.targetY = readInt(newData, index, 4);
                part.w = readInt(newData, index, 4);
                part.h = readInt(newData, index, 4);
                restoreData.parts.append(part);
            }
            resultData.restoreData.append(restoreData);
        }
        resultData.localSize = index - resultData.localOffset;
        return resultData;
    }
    virtual bool setData(const QByteArray& data)
    {
        int index = 0;

        while (index < data.length())
        {
            imagesData.append(read(index, data));
        }

        return true;
    }
};
#endif // RESOURCEDESCRIPTION_H
