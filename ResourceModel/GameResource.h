#ifndef GAMERESOURCE_H
#define GAMERESOURCE_H
#include "ResourceDescription.h"
#include <QImage>
#include <QImage>
#include <QPainter>
enum PreprocessingShaderType
{
    NoTransparentActions = 0,
    Default = 1,
    Shadows = 2,
    TransparentBlack = 4,
    Border = 8,

    WithSizeOptimisation  = (1 << 15),
    DefaultWithSizeOptimisation  = (1 << 15) | Default,
    ShadowsWithSizeOptimisation = Shadows | DefaultWithSizeOptimisation,
    TransparentBlackWithSizeOptimisation = TransparentBlack | DefaultWithSizeOptimisation
};

class GameResource
{
public:
    void clear();

    bool read(const QString& filename);

    ResourceDesc * ResDescByUid(int uid);

    ResourceDesc *ResDescByName(const QString& name);

    QList<QString> GetAllDescs();
    QList<QString> getIndexNames();

    IndexesData::IndexesDesc GetIndexDescByName(const QString& name);

    QImage getSimpleImageByName(const QString& id,
                                PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation);

    bool hasResource(const QString& name);

    static QImage shaderProcessedPixmap(QImage &before, PreprocessingShaderType shader);

    QList<QImage> getFramesById(const QString& id,
                                PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation,
                                bool onlyFirst = false);

    int getFramesCountById(const QString& id);

    void restoreNames();

    bool Write(const QString& name);
    const QList<ResourceDesc *> &getResources() const;

    bool read2(const QString &filename);
    QString getPath() const;
private:
    void processParts(ResourceDesc *resDesc, ImagesContainer::ImageRestoreData &restoreData, QList<QImage> &result, PreprocessingShaderType shader);
private:
    QString path;
    QList<ResourceDesc*> resources;
    QByteArray header;
    ResourceDesc *names_desc = nullptr;
    ResourceDesc *images_opt = nullptr;
    ResourceDesc *indexes_opt = nullptr;
    ResourceDesc *anim_opt = nullptr;
};

#endif // GAMERESOURCE_H
