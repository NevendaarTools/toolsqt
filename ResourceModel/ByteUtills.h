#ifndef BYTEUTILLS_H
#define BYTEUTILLS_H
#include <QByteArray>
#include <QDataStream>
#include "../Common/ByteEncoder.h"
#include <QIODevice>

static QByteArray stringToByteArray(const QString& string, int size)
{
    QByteArray result = stringToByteArray(string);
    if (result.length() < size)
    {
        QByteArray extra(size - result.length(), '\0');
        return result + extra;
    }
    return result;
}

static QByteArray intToByteArray(int value, int size)
{
    QByteArray result;
    QDataStream stream(&result, QIODevice::WriteOnly);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream << value;
    if (result.length() < size)
    {
        QByteArray extra(size - result.length(), '\0');
        return extra + result;
    }
    result.resize(size);
    return result;
}

static int32_t readInt(const QByteArray& newData, int & index, int size)
{
    int32_t val1 = (static_cast<unsigned int>(newData[index++]) & 0xFF);
    val1 <<= 0;
    if (size == 1)
        return val1;
    int32_t val2 = (static_cast<unsigned int>(newData[index++]) & 0xFF);
    val2 <<= 8;
    if (size == 2)
        return val1 + val2;
    int32_t val3 = (static_cast<unsigned int>(newData[index++]) & 0xFF);
    val3 <<= 16;
    if (size == 3)
        return val1 + val2 + val3;
    int32_t val4 = (static_cast<unsigned int>(newData[index++]) & 0xFF);
    val4 <<= 24;
    return val1 + val2 + val3 + val4;
}

static QString readString(const QByteArray& newData, int & index, int size)
{
    QString result = stringFromByteArray(newData.mid(index, size));
    result.remove(QChar('\0'));
    index += size;
    return result;
}

static QString readStringWithTerm(const QByteArray& newData, int & index)
{
    int endIndex = index;
    for (int i = index; i < newData.length(); ++i)
    {
        if (newData[i] == 0)
        {
            endIndex = i;
            break;
        }
    }
    QString res = readString(newData, index, endIndex - index);
    index++;
    return res;
}

static QString readDefaultString(const QByteArray& newData, int & startIndex, const QString & tag)
{
    QString result;
    startIndex = newData.indexOf(tag.toUtf8(), startIndex) + tag.length() + 8;//\v\0\0\0mapv
    result = readString(newData, startIndex, 6);
    return result;
}

static QString readStringWithLenght(const QByteArray& newData, int & startIndex, const QString & tag)
{
    QString result;
    startIndex = newData.indexOf(tag.toUtf8(), startIndex) + tag.length();
    int lenght = readInt(newData, startIndex, 4) - 1;
    result = readString(newData, startIndex, lenght);

    return result;
}

static bool readBool(const QByteArray& array, int & index)
{
    int val = readInt(array, index, 1);
    return val > 0;
}

static bool readBool(const QByteArray& array, int & index, const QString & tag)
{
    index = array.indexOf(tag.toUtf8(), index) + tag.length();
    int val = readInt(array, index, 1);
    return val > 0;
}

static int readDefaultInt(const QByteArray& newData, int & startIndex, const QString & tag)
{
    if (startIndex < 0)
        return 0;
    startIndex = newData.indexOf(tag.toUtf8(), startIndex) + tag.length();
    return readInt(newData, startIndex, 4);
}

static int32_t getEndIndex(const QByteArray& data, int index)
{
    static const QByteArray dataEnd = QString("ENDOBJECT").toUtf8();
    return data.indexOf(dataEnd, index) + 10;
}
//static int ReadInt(ref byte[] array, ref int startIndex, string tag, int size)
//{
//    startIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray(tag), startIndex) + tag.Length;
//    return ReadInt(ref array, ref startIndex, size);
//}

using myarr = char[];

template <size_t N>
QByteArray make_QByteArray(const char(&a)[N]) {
    return {a,N};
}

static QByteArray dataHeader()
{
    static QByteArray res = make_QByteArray(myarr
        {'W', 'H', 'A', 'T', 0, 0, 0, 0,'.','?','A','V','C','@','@', 0});
    return res;
}

static QByteArray dataHeader(int num, const QString& name)
{
    QByteArray res = dataHeader();
    res[4] = num;
    res.insert(13, name.toUtf8());
    return res;
}

static QString dataType(const QByteArray& array)
{
    return QString::fromUtf8(array.mid(13, array.length() - 13 - 3));
}

template <class classT>
static QByteArray blockHeader()
{
    //WHAT\023\000\000\000.?AVCMidLandmark@@\000
    static QByteArray res = dataHeader(classT::code(), classT::type());
    return res;
}

static void setBitInByteArray(QByteArray& byteArray, int index, bool value)
{
    int byteIndex = (index - 1) / 8;
    int bitIndex = (index - 1) % 8;
    char* data = byteArray.data();
    char& targetByte = data[byteIndex];
    if (value) {
        targetByte |= (1 << bitIndex);
    } else {
        targetByte &= ~(1 << bitIndex);
    }
}

#endif // BYTEUTILLS_H
