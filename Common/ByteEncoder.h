#ifndef BYTEENCODER_H
#define BYTEENCODER_H

#include <QtGlobal>
#include <QString>
#include <QByteArray>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#else
#include <QStringEncoder>
#include <QStringDecoder>
#endif

QByteArray stringToByteArray(const QString& string);
QByteArray stringToByteArrayCp866(const QString& string);

QString stringFromByteArray(const QByteArray& data);
QString stringFromByteArrayCp866(const QByteArray& data);

#endif // BYTEENCODER_H
