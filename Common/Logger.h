#ifndef LOGGER_H
#define LOGGER_H
#include <QString>
#include <QList>
#include <QMetaType>
#include "INotifier.h"
#define LOG(text) Logger::log(text)
#define LOG_ERROR(text) Logger::logError(text)
#define LOG_WARNING(text) Logger::logWarning(text)

struct LogUpdatedEvent
{
};
Q_DECLARE_METATYPE(LogUpdatedEvent)

class Logger
{
public:
    static Logger & instance();
    enum LogLevel
    {
        Error = 0,
        Warning,
        Info
    };

    struct Record
    {
        LogLevel level;
        QString message;
    };


    static void logError(const QString & text);
    static void logWarning(const QString & text);
    static void log(const QString & text);

    static void log(LogLevel level, const QString & text);
    static QList<Logger::Record> records(int rowCount);

    static bool writeToFile();
    static void setWriteToFile(bool newWriteToFile);
    static void setNotifier(INotifier *newNotifier);

    static void disable() {instance().m_enabled = false;}
    static void enable() {instance().m_enabled = true;}
private:
    Logger();
private:
    QList<Record> m_records;
    bool m_writeToFile = false;
    INotifier * m_notifier;
    bool m_enabled = true;
};
#endif // LOGGER_H
