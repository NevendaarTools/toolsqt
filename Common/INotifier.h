#ifndef INOTIFIER_H
#define INOTIFIER_H
#include <QVariant>

class INotifier
{
public:
    virtual void notify(const QVariant& event) = 0;
};

#endif // INOTIFIER_H
