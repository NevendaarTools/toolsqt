#include "Logger.h"
#include <QFile>
#include <QTextStream>
#include <iostream>

Logger &Logger::instance()
{
    static Logger instance;
    return instance;
}

Logger::Logger()
{
    m_notifier = nullptr;
}

void Logger::setNotifier(INotifier *newNotifier)
{
    instance().m_notifier = newNotifier;
}

void Logger::logError(const QString &text)
{
    log(LogLevel::Error, text);
}

void Logger::logWarning(const QString &text)
{
    log(LogLevel::Warning, text);
}

void Logger::log(const QString &text)
{
    log(LogLevel::Info, text);
}

void Logger::log(LogLevel level, const QString &text)
{
    if (!instance().m_enabled)
        return;
    Record record;
    record.level = level;
    record.message = text;
    instance().m_records.append(record);
    if (instance().m_writeToFile)
    {
        QFile outFile("log.txt");
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream ts(&outFile);
        switch(level)
        {
        case Logger::Error:
            ts << "[Error]   " ;
            break;
        case Logger::Warning:
            ts << "[Warning] " ;
            break;
        case Logger::Info:
            ts << "[Info]    " ;
            break;

        }

        ts << text << "\n";
        outFile.close();
    }
    std::cout<<text.toStdString()<<"\n";
    if (instance().m_notifier)
        instance().m_notifier->notify(QVariant::fromValue(LogUpdatedEvent()));
}

QList<Logger::Record> Logger::records(int rowCount)
{
    if (instance().m_records.count() <= rowCount)
        return instance().m_records;
    return instance().m_records.mid(instance().m_records.count() - 1 - rowCount);
}

bool Logger::writeToFile()
{
    return instance().m_writeToFile;
}

void Logger::setWriteToFile(bool newWriteToFile)
{
    instance().m_writeToFile = newWriteToFile;
    if (newWriteToFile)
    {
        //clear log file
        QFile outFile("log.txt");
        outFile.open(QIODevice::WriteOnly);
        outFile.close();
    }
}
