#include "D2MapModel.h"
#include "DataBlocks/DataBlocks.h"
#include "../Common/Logger.h"

D2MapModel::D2MapModel()
{
    m_defaultFactory = QSharedPointer<IDataBlockFactory>(new Factory<TagDataBlock>());
    registerFactory(new Factory<D2LandMark>());
    registerFactory(new Factory<D2MapBlock>());
    registerFactory(new Factory<D2Village>());
    registerFactory(new Factory<D2Stack>());
    registerFactory(new Factory<D2Unit>());
    registerFactory(new Factory<D2Capital>());
    registerFactory(new Factory<D2Player>());
    registerFactory(new Factory<D2Mountains>());
    registerFactory(new Factory<D2Crystal>());
    registerFactory(new Factory<D2Ruin>());
    registerFactory(new Factory<D2Item>());
    registerFactory(new Factory<D2Bag>());
    registerFactory(new Factory<D2Merchant>());
    registerFactory(new Factory<D2Mercs>());
    registerFactory(new Factory<D2Mage>());
    registerFactory(new Factory<D2Trainer>());
    registerFactory(new Factory<D2Diplomacy>());
    registerFactory(new Factory<D2Location>());
    registerFactory(new Factory<D2Map>());
    registerFactory(new Factory<D2MapFog>());
    registerFactory(new Factory<D2Plan>());
    registerFactory(new Factory<D2PlayerBuildings>());
    registerFactory(new Factory<D2PlayerSpells>());
    registerFactory(new Factory<D2QuestLog>());
    registerFactory(new Factory<D2ScenarioInfo>());
    registerFactory(new Factory<D2SceneVariables>());
    registerFactory(new Factory<D2SpellCast>());
    registerFactory(new Factory<D2SpellEffects>());
    registerFactory(new Factory<D2StackDestroyed>());
    registerFactory(new Factory<D2StackTemplate>());
    registerFactory(new Factory<D2SubRace>());
    registerFactory(new Factory<D2TalismanCharges>());
    registerFactory(new Factory<D2TurnSummary>());
    registerFactory(new Factory<D2Road>());
    registerFactory(new Factory<D2Event>());
    registerFactory(new Factory<D2Rod>());
    registerFactory(new Factory<D2Tomb>());
}

QSharedPointer<IDataBlockFactory> D2MapModel::factoryByHeader(const QString &header)
{
    if (m_factoryBinding.contains(header))
        return m_factoryBinding[header];
    return m_defaultFactory;
}

QSharedPointer<IDataBlockFactory> D2MapModel::factoryByHeader(const QByteArray &header)
{
    return factoryByHeader(dataType(header));
}

void D2MapModel::readBlock(const QByteArray &data, int &index)
{
    static const QByteArray typeStart = QString(".?").toUtf8();
    static const QByteArray typeEnd = QString("@@\0").toUtf8();
    static const QByteArray dataEnd = QString("ENDOBJECT\0").toUtf8();

    int endTypeIndex = data.indexOf(typeEnd, index) + 3;
    if (endTypeIndex < 3)
    {
        index = -1;
        return;
    }
    int initialIndex = index;
    int size = endTypeIndex - index;
    QByteArray header = data.mid(index, size);
    IDataBlock * res = factoryByHeader(header)->read(data, index, this->header);
    if (index < initialIndex)
    {
        Logger::logError("data block read failed! " + res->blockType() + " " + res->stringId());
        qDebug()<<"data block read failed!";
        return;
    }
    bool test = false;
    if (test)
    {
        TagDataBlock * tmp = new TagDataBlock();
        tmp->read(data, initialIndex, this->header);
        QByteArray result = tmp->data(this->header);
        QByteArray localData = res->data(this->header);
        bool equal = localData == result;
        if (!equal)
        {
            QFile file("C:/before.bin");
            file.open(QIODevice::WriteOnly);
            file.write(result);
            file.close();
            {
                QFile file("C:/after.bin");
                file.open(QIODevice::WriteOnly);
                file.write(localData);
                file.close();
            }
            Logger::logError("data block read failed! 2 " + res->blockType() + " " + res->stringId());
            qDebug()<<"data block read failed! 2" << res->blockType()<<"_"<<res->blockId();

        }
    }
    addDataBlock(res);
}

void D2MapModel::registerFactory(IDataBlockFactory *factory)
{
    m_factoryBinding.insert(factory->type(), QSharedPointer<IDataBlockFactory>(factory));
}

void D2MapModel::addDataBlock(IDataBlock *block)
{
    if (hasBlock(block->blockId()))
    {
        qWarning()<<"addDataBlock. already has key - " << block->blockId();
        qWarning()<<("data block read failed! " + block->blockType() + " " + block->stringId());

    }
    m_blocks.append(QSharedPointer<IDataBlock>(block));
}

bool D2MapModel::open(const QString &path)
{
    QByteArray array;
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Failed to open map file" + file.fileName();
        return false;
    }
    array = file.readAll();
    file.close();
    return read(array);
}

bool D2MapModel::read(const QByteArray & array)
{
    int index = 0;
    header.read(array, index);
    m_blocks.clear();

    while (true)
    {
        readBlock(array, index);
        if (index < 0)
            break;
    }
    return true;
}


bool D2MapModel::save(const QString &path)
{
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Failed to save map file" + file.fileName();
        return false;
    }

    file.write(header.data(m_blocks.count()));
    foreach (QSharedPointer<IDataBlock> block, m_blocks)
    {
        file.write(block->data(header));
    }
    file.close();

    return true;
}
