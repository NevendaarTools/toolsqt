#ifndef MAPGRID_H
#define MAPGRID_H
#include <QVector>
#include <QPointF>

static constexpr inline std::uint8_t tileTerrain(std::uint32_t tile)
{
    return static_cast<std::uint8_t>(tile & 7);
}

static constexpr inline std::uint8_t tileGround(std::uint32_t tile)
{
    return static_cast<std::uint8_t>((tile >> 3) & 7);
}

static constexpr inline std::uint8_t tileForestImage(std::uint32_t tile)
{
    return tile >> 26;
}

static QPointF cartesianToIsometric(int x, int y)
{
    float newX = x-y;
    float newY = (x+y) / 2.0f;
    return QPointF(newX, newY);
}

static QPointF isometricToCartesian(int x, int y)
{
    float newX = (2 * y + x) / 2.0f;
    float newY = (2 * y - x) / 2.0f;
    return QPointF(newX, newY);
}

static inline uint getXOffset(int x, int y, int totalW, int tileW)
{
    return (totalW/2 + x - y - tileW);
}

static inline uint getYOffset(int x, int y)
{
    return (x + y) / 2;
}

struct MapCell
{
    MapCell(){}
    MapCell(int value_) : value(value_){}
    int32_t value = 1;
    int roadType = -1;
    int roadVar = -1;

    bool operator ==(const MapCell& other) const
    {
        return value == other.value && roadType == other.roadType;
    }

    bool operator !=(const MapCell& other) const
    {
        return !(value == other.value);
    }

    bool isWater() const
    {
        return tileGround(value)  == 3;
    }
};

struct MapObjBinding
{
    struct UIDList
    {
        QList<QPair<int, int>> items;
    };

    QVector<QVector<UIDList>> binding;
    void clear(){binding.clear();}
    void init(int size)
    {
        binding.resize(size);
        for (int i = 0; i < size; ++i) {
            binding[i].resize(size);
        }
    }
};

struct MapGrid
{
    QList<QList<MapCell>> cells;
    void init(int size, int val = 1)
    {
        clear();
        objBinging.init(size);
        locationsBinging.init(size);
        for(int i = 0; i < size; ++i)
        {
            cells.append(QList<MapCell>());
            for(int k = 0; k < size; ++k)
                cells[i].append(MapCell(val));
        }
    }
    void clear(){cells.clear(); objBinging.clear();}
    MapObjBinding objBinging;
    MapObjBinding locationsBinging;
};

#endif // MAPGRID_H
