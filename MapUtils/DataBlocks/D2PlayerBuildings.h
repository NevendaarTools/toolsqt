#pragma once
#include "../DataBlock.h"

class D2PlayerBuildings : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "PlayerBuildings";
        return res;
    }
    static int code()
    {
        return 0x17;
    }
    static QString shortType()
    {
        return "PB";
    }
    static int intType()
    {
        return IDataBlock::PlayerBuildings;
    }

    D2PlayerBuildings()
    {
        uid.first = intType();
    }

    QList<QString> buildings;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            buildings.append(readDefaultString(data, index, "BUILD_ID"));
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, buildings.count());
        for (int i = 0; i < buildings.count(); ++i)
        {
            buffer.writeDefaultString("BUILD_ID", header.version, buildings[i]);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2PlayerBuildings)
