#pragma once
#include "../DataBlock.h"
#include "D2Stack.h"

class D2Capital : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "Capital";
        return res;
    }
    static int code()
    {
        return 0x0f;//15
    }
    static QString shortType()
    {
        return "FT";
    }
    static int intType()
    {
        return IDataBlock::Fort_capital;
    }

    D2Capital()
    {
        uid.first = intType();
    }

    bool Hasstack()
    {
        for (int i = 0; i < 6; ++i)
        {
            if (stack.unit[i] != -1)
                return true;
        }
        return false;
    }

    QString name = "";
    QString desc = "";
    int owner;
    QPair<int, int> subRace;
    int stackId;
    int posX = 0;
    int posY = 0;
    StackData stack;

    QList<int> items;
    int AIpriority = 3;


    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        name  = readStringWithLenght(data, index, "NAME_TXT");
        desc  = readStringWithLenght(data, index, "DESC_TXT");
        QString ownerStr = readDefaultString(data, index, "OWNER");
        owner = getIntId(ownerStr);
        subRace = readUid(data, index, "SUBRACE");
        QString stackStr = readDefaultString(data, index, "STACK");
        stackId = getIntId(stackStr);
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");

        for (int i = 0; i < 6; ++i)
        {
            QString unitid = readDefaultString(data, index, "UNIT_" + QString::number(i));
            stack.unit[i] = getIntId(unitid);
        }

        for (int i = 0; i < 6; ++i)
        {
            stack.pos[i] = readDefaultInt(data, index, "POS_" + QString::number(i));
        }

        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            QString itemId = readDefaultString(data, index, "ITEM_ID");
            items.append(getIntId(itemId));
        }

        AIpriority = readDefaultInt(data, index, "AIPRIORITY");
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("CITY_ID", header.version, objId);
        buffer.writeString("NAME_TXT", name);
        buffer.writeString("DESC_TXT", desc);
        buffer.writeDefaultString("OWNER", header.version, IDataBlock::Player, owner);
        buffer.writeDefaultString("SUBRACE", header.version, IDataBlock::SubRace, subRace.second);
        buffer.writeDefaultString("STACK", header.version, IDataBlock::Stack, stackId);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y",  posY);
        buffer.writeDefaultString("GROUP_ID", header.version, objId);

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultString("UNIT_" + QString::number(i), header.version, IDataBlock::Unit, stack.unit[i]);
            if (stack.unit[i] != -1)
                qDebug()<<"#"<<TypeHolder::ShortByType(IDataBlock::Unit) +
                           QString::number(stack.unit[i], 16).rightJustified(4, '0').toLower();
        }

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultInt("POS_" + QString::number(i), stack.pos[i]);
        }

        int itemsCount = items.count();
        buffer.writeDefaultInt(header.version + objId, itemsCount);
        for (int i = 0; i < itemsCount; ++i)
        {
            buffer.writeDefaultString("ITEM_ID", header.version, IDataBlock::Item, items[i]);
        }

        buffer.writeDefaultInt("AIPRIORITY", AIpriority);

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Capital)
