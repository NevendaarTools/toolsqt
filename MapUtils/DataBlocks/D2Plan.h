#pragma once
#include "../DataBlock.h"

class D2Plan : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidgardPlan";
        return res;
    }
    static int code()
    {
        return 0x13;
    }
    static QString shortType()
    {
        return "PN";
    }
    static int intType()
    {
        return IDataBlock::Plan;
    }

    D2Plan()
    {
        uid.first = intType();
        uid.second = 0;
    }

    int size = 48;
    struct PlanElement
    {
        int posX;
        int posY;
        QPair<int, int> id;
    };
    QList<PlanElement> elements;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        size = readDefaultInt(data, index, objId);
        int count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
        {
            PlanElement elem;
            elem.posX = readDefaultInt(data, index, "POS_X");
            elem.posY = readDefaultInt(data, index, "POS_Y");
            QString elemId = readDefaultString(data, index, "ELEMENT");
            elem.id = TypeHolder::idFromString(elemId);

            elements.append(elem);
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, size);
        buffer.writeDefaultInt(header.version + objId, elements.count());
        for (int i = 0; i < elements.count(); ++i)
        {
            buffer.writeDefaultInt("POS_X", elements[i].posX);
            buffer.writeDefaultInt("POS_Y", elements[i].posY);
            buffer.writeDefaultString("ELEMENT", header.version,
                                      (IDataBlock::Type)elements[i].id.first,
                                       elements[i].id.second);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Plan)
