#pragma once
#include "../DataBlock.h"

class D2Location : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidLocation";
        return res;
    }
    static int code()
    {
        return 0x13;
    }
    static QString shortType()
    {
        return "LO";
    }
    static int intType()
    {
        return IDataBlock::Location;
    }

    D2Location()
    {
        uid.first = intType();
    }

    int posX = 0;
    int posY = 0;
    int radius = 1;// 0 - 1x1, 1 - 3x3, 2 - 5x5
    QString name;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        name = readStringWithLenght(data, index, "NAME_TXT");
        radius = readDefaultInt(data, index, "RADIUS");

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("LOC_ID", header.version, objId);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeString("NAME_TXT", name);
        buffer.writeDefaultInt("RADIUS", radius);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Location)
