#pragma once
#include "../DataBlock.h"

class D2SpellCast : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidSpellCast";
        return res;
    }
    static int code()
    {
        return 0x14;
    }
    static QString shortType()
    {
        return "ST";
    }
    static int intType()
    {
        return IDataBlock::SpellCast;
    }

    D2SpellCast()
    {
        uid.first = intType();
    }

    int unknown1 = 0;
    int unknown2 = 0;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        unknown1 = readDefaultInt(data, index, objId);
        unknown2 = readDefaultInt(data, index, objId);

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultInt(header.version + objId,  unknown1);
        buffer.writeDefaultInt(header.version + objId,  unknown2);

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2SpellCast)
