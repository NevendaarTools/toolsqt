#pragma once
#include "../DataBlock.h"

class D2ScenarioInfo : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "ScenarioInfo";
        return res;
    }
    static int code()
    {
        return 0x14;
    }
    static QString shortType()
    {
        return "IF";
    }
    static int intType()
    {
        return IDataBlock::ScenarioInfo;
    }

    D2ScenarioInfo()
    {
        uid.first = intType();
        players[0] = 4;
        for (int i = 1; i < 13; ++i) {
            players[i] = 99;
        }
    }

    QString campaign = "C000CC0001";
    bool sourceM = false;
    int qtyCities = 0;
    QString name = "";
    QString desc = "";
    QString briefing = "";
    QString debunkw = "";
    QString debunkw2 = "";
    QString debunkw3 = "";
    QString debunkw4 = "";
    QString debunkw5 = "";
    QString debunkl = "";
    QString brieflong1 = "";
    QString brieflong2 = "";
    QString brieflong3 = "";
    QString brieflong4 = "";
    QString brieflong5 = "";
    bool o = false;
    int curTurn = 0;
    int maxUnit = 2;
    int maxSpell = 2;
    int maxLeader = 3;
    int maxCity = 5;
    int mapSize = 48;
    int diffscen = 3;
    int diffgame = 1;
    QString creator="";
    int players[13];
    int suggLvl = 1;
    int mapSeed = 390690065;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        campaign = readStringWithLenght(data, index, "CAMPAIGN");
        sourceM = readBool(data, index, "SOURCE_M");
        qtyCities = readDefaultInt(data, index, "QTY_CITIES");
        name = readStringWithLenght(data, index, "NAME");
        desc = readStringWithLenght(data, index, "DESC");
        briefing = readStringWithLenght(data, index, "BRIEFING");
        debunkw = readStringWithLenght(data, index, "DEBUNKW");
        if (header.mapType == "D2EESFISIG" || header.offset != 34)
        {
            debunkw2 = readStringWithLenght(data, index, "DEBUNKW2");
            debunkw3 = readStringWithLenght(data, index, "DEBUNKW3");
            debunkw4 = readStringWithLenght(data, index, "DEBUNKW4");
            debunkw5 = readStringWithLenght(data, index, "DEBUNKW5");
        }
        debunkl = readStringWithLenght(data, index, "DEBUNKL");
        brieflong1 = readStringWithLenght(data, index, "BRIEFLONG1");
        brieflong2 = readStringWithLenght(data, index, "BRIEFLONG2");
        brieflong3 = readStringWithLenght(data, index, "BRIEFLONG3");
        brieflong4 = readStringWithLenght(data, index, "BRIEFLONG4");
        brieflong5 = readStringWithLenght(data, index, "BRIEFLONG5");
        o = readBool(data, index, "O");
        curTurn = readDefaultInt(data, index, "CUR_TURN");
        maxUnit = readDefaultInt(data, index, "MAX_UNIT");
        maxSpell = readDefaultInt(data, index, "MAX_SPELL");
        maxLeader = readDefaultInt(data, index, "MAX_LEADER");
        maxCity = readDefaultInt(data, index, "MAX_CITY");
        mapSize = readDefaultInt(data, index, "MAP_SIZE");
        diffscen = readDefaultInt(data, index, "DIFFSCEN");
        diffgame = readDefaultInt(data, index, "DIFFGAME");
        creator = readStringWithLenght(data, index, "CREATOR");
        for(int i = 0; i < 13; ++i)
            players[i] = readDefaultInt(data, index, "PLAYER_" + QString::number(i+1));

        suggLvl = readDefaultInt(data, index, "SUGG_LVL");
        mapSeed = readDefaultInt(data, index, "MAP_SEED");
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString( "INFO_ID", header.version, objId);
        buffer.writeString( "CAMPAIGN", campaign);
        buffer.writeBool( "SOURCE_M", sourceM);
        buffer.writeDefaultInt( "QTY_CITIES", qtyCities);
        buffer.writeString( "NAME", name);
        buffer.writeString( "DESC", desc);
        buffer.writeString( "BRIEFING", briefing);
        buffer.writeString( "DEBUNKW", debunkw);
        if (header.mapType == "D2EESFISIG" || header.offset != 34)
        {
            buffer.writeString("DEBUNKW2", debunkw2);
            buffer.writeString("DEBUNKW3", debunkw3);
            buffer.writeString("DEBUNKW4", debunkw4);
            buffer.writeString("DEBUNKW5", debunkw5);
        }
        buffer.writeString( "DEBUNKL", debunkl);
        buffer.writeString( "BRIEFLONG1", brieflong1);
        buffer.writeString( "BRIEFLONG2", brieflong2);
        buffer.writeString( "BRIEFLONG3", brieflong3);
        buffer.writeString( "BRIEFLONG4", brieflong4);
        buffer.writeString( "BRIEFLONG5", brieflong5);
        buffer.writeBool( "O", o);
        buffer.writeDefaultInt( "CUR_TURN", curTurn);
        buffer.writeDefaultInt( "MAX_UNIT", maxUnit);
        buffer.writeDefaultInt( "MAX_SPELL", maxSpell);
        buffer.writeDefaultInt( "MAX_LEADER", maxLeader);
        buffer.writeDefaultInt( "MAX_CITY", maxCity);
        buffer.writeDefaultInt( "MAP_SIZE", mapSize);
        buffer.writeDefaultInt( "DIFFSCEN", diffscen);
        buffer.writeDefaultInt( "DIFFGAME", diffgame);
        buffer.writeString( "CREATOR", creator);

        for (int i = 0; i < 13; ++i)
            buffer.writeDefaultInt("PLAYER_" + QString::number(i + 1), players[i]);

        buffer.writeDefaultInt( "SUGG_LVL", suggLvl);
        buffer.writeDefaultInt( "MAP_SEED", mapSeed);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2ScenarioInfo)
