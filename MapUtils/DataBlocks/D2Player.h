#pragma once
#include "../DataBlock.h"

class D2Player : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidPlayer";
        return res;
    }
    static int code()
    {
        return 0x11;
    }
    static QString shortType()
    {
        return "PL";
    }
    static int intType()
    {
        return IDataBlock::Player;
    }

    D2Player()
    {
        uid.first = intType();
    }

    QString name= "";
    QString desc = "";
    QString lordId = "G000LR0013";
    QString raceId = "G000RR0004";
    int fogId = 0;
    int knownId = 0;
    int buildingsId = 0;
    int face = 1;
    int qtyBreaks = 0;
    QString bank = "G0000:R0000:Y0000:E0000:W0000:B0000";
    bool isHuman = false;
    QString spellBank = "G0000:R0000:Y0000:E0000:W0000:B0000";
    int attitude = 1;
    int researchOnThisTurn = 0;
    int constructOnTHisTurn = 0;
    QString spyRace1 = "000000";
    QString spyRace2 = "000000";
    QString spyRace3 = "000000";
    QString capturedBy = "000000";
    bool alwaysai = false;
    QString exmapid1 = "000000";
    int exmapturn1 = 0;
    QString exmapid2 = "000000";
    int exmapturn2 = 0;
    QString exmapid3 = "000000";
    int exmapturn3 = 0;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        name = readStringWithLenght(data, index, "NAME_TXT");
        desc = readStringWithLenght(data, index, "DESC_TXT");
        lordId = readStringWithLenght(data, index, "LORD_ID");
        raceId = readStringWithLenght(data, index, "RACE_ID");
        fogId = getIntId(readDefaultString(data, index, "FOG_ID"));
        knownId = getIntId(readDefaultString(data, index, "KNOWN_ID"));
        buildingsId = getIntId(readDefaultString(data, index, "BUILDS_ID"));
        face = readDefaultInt(data, index, "FACE");
        qtyBreaks = readDefaultInt(data, index, "QTY_BREAKS");
        bank = readStringWithLenght(data, index, "BANK");
        isHuman = readBool(data, index, "IS_HUMAN");
        spellBank = readStringWithLenght(data, index, "SPELL_BANK");
        attitude = readDefaultInt(data, index, "ATTITUDE");
        researchOnThisTurn = readDefaultInt(data, index, "RESEAR_T");
        constructOnTHisTurn = readDefaultInt(data, index, "CONSTR_T");
        spyRace1 = readDefaultString(data, index, "SPY_1");
        spyRace2 = readDefaultString(data, index, "SPY_2");
        spyRace3 = readDefaultString(data, index, "SPY_3");
        capturedBy = readDefaultString(data, index, "CAPT_BY");
        if (header.mapType == "D2EESFISIG" || header.offset == 0)
            alwaysai = readBool(data, index, "ALWAYSAI");
        if (header.mapType == "D2EESFISIG")
        {
            exmapid1 = readDefaultString(data, index, "EXMAPID1");
            exmapturn1 = readDefaultInt(data, index, "EXMAPTURN1");
            exmapid2 = readDefaultString(data, index, "EXMAPID2");
            exmapturn2 = readDefaultInt(data, index, "EXMAPTURN2");
            exmapid3 = readDefaultString(data, index, "EXMAPID3");
            exmapturn3 = readDefaultInt(data, index, "EXMAPTURN3");
        }

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("PLAYER_ID", header.version, objId);
        buffer.writeString("NAME_TXT", name);
        buffer.writeString("DESC_TXT", desc);
        buffer.writeString("LORD_ID", lordId);
        buffer.writeString("RACE_ID", raceId);
        buffer.writeDefaultString("FOG_ID", header.version, IDataBlock::Fog, fogId);
        buffer.writeDefaultString("KNOWN_ID", header.version, IDataBlock::PlayerSpells, knownId);
        buffer.writeDefaultString("BUILDS_ID", header.version, IDataBlock::PlayerBuildings, buildingsId);
        buffer.writeDefaultInt("FACE", face);
        buffer.writeDefaultInt("QTY_BREAKS", qtyBreaks);
        buffer.writeString("BANK", bank);
        buffer.writeBool("IS_HUMAN", isHuman);
        buffer.writeString("SPELL_BANK", spellBank);
        buffer.writeDefaultInt("ATTITUDE", attitude);
        buffer.writeDefaultInt("RESEAR_T", researchOnThisTurn);
        buffer.writeDefaultInt("CONSTR_T", constructOnTHisTurn);
        buffer.writeDefaultString("SPY_1", header.version, spyRace1);
        buffer.writeDefaultString("SPY_2", header.version, spyRace2);
        buffer.writeDefaultString("SPY_3", header.version, spyRace3);
        buffer.writeDefaultString("CAPT_BY", header.version, capturedBy);

        if (header.mapType == "D2EESFISIG" || header.offset == 0)
            buffer.writeBool("ALWAYSAI", alwaysai);
        if (header.mapType == "D2EESFISIG")
        {
            buffer.writeDefaultString("EXMAPID1", header.version, exmapid1);
            buffer.writeDefaultInt("EXMAPTURN1", exmapturn1);
            buffer.writeDefaultString("EXMAPID2", header.version, exmapid2);
            buffer.writeDefaultInt("EXMAPTURN2", exmapturn2);
            buffer.writeDefaultString("EXMAPID3", header.version, exmapid3);
            buffer.writeDefaultInt("EXMAPTURN3", exmapturn3);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Player)
