#pragma once
#include "../DataBlock.h"

class D2Map : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidgardMap";
        return res;
    }
    static int code()
    {
        return 0x12;
    }
    static QString shortType()
    {
        return "MP";
    }
    static int intType()
    {
        return IDataBlock::Map;
    }

    D2Map()
    {
        uid.first = intType();
    }

    int size = 48;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        size = readDefaultInt(data, index, objId);

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, size);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Map)
