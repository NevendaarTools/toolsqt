#pragma once
#include "../DataBlock.h"

class D2Crystal : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidCrystal";
        return res;
    }
    static int code()
    {
        return 0x12;//18
    }
    static QString shortType()
    {
        return "CR";
    }
    static int intType()
    {
        return IDataBlock::Crystal;
    }

    D2Crystal()
    {
        uid.first = intType();
    }

    int resource = 0;
    int posX = 0;
    int posY = 0;
    int aipriority = 3;


    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        resource = readDefaultInt(data, index, "RESOURCE");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        aipriority = readDefaultInt(data, index, "AIPRIORITY");

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("CRYSTAL_ID", header.version, objId);
        buffer.writeDefaultInt("RESOURCE",  resource);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeDefaultInt( "AIPRIORITY",  aipriority);

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Crystal)
