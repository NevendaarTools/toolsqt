#pragma once
#include "../DataBlock.h"


class D2Tomb : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidTomb";
        return res;
    }
    static int code()
    {
        return 0x0f;
    }
    static QString shortType()
    {
        return "TB";
    }
    static int intType()
    {
        return IDataBlock::Tomb;
    }

    D2Tomb()
    {
        uid.first = intType();
    }

    int posX = 0;
    int posY = 0;

    struct TombEntry
    {
        QPair<int, int> owner;//"PL0000";
        QPair<int, int> killer;//"PL0000";
        int turn = 0;
        QString name;
    };
    QVector<TombEntry> entries;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        int qtyEp = readDefaultInt(data, index, "QTY_EP");
        for (int i = 0; i < qtyEp; ++i)
        {
            TombEntry entry;
            entry.owner = readUid(data, index, "STACK_OWNR");
            entry.killer = readUid(data, index, "KILLER");
            entry.turn = readDefaultInt(data, index, "TURN");
            entry.name = readStringWithLenght(data, index, "STACK_NAME");
            entries << entry;
        }

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("TOMB_ID", header.version, objId);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeInt("QTY_EP", entries.count(), 4);
        foreach(const TombEntry & entry, entries)
        {
            buffer.writeDefaultString("STACK_OWNR", header.version, entry.owner);
            buffer.writeDefaultString("KILLER", header.version, entry.killer);
            buffer.writeDefaultInt("TURN", entry.turn);
            buffer.writeString("STACK_NAME", entry.name);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Tomb)
