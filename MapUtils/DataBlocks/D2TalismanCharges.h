#pragma once
#include "../DataBlock.h"
#include "D2Item.h"

class D2TalismanCharges : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidTalismanCharges";
        return res;
    }
    static int code()
    {
        return 0x1a;
    }
    static QString shortType()
    {
        return "TC";
    }
    static int intType()
    {
        return IDataBlock::TalismanCharges;
    }

    D2TalismanCharges()
    {
        uid.first = intType();
    }

    struct TalismanChargeInfo
    {
        int talismanId;
        int count;
    };

    QList<TalismanChargeInfo> charges;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            TalismanChargeInfo info;
            QString talismanId = readDefaultString(data, index, "ID_TALIS");
            info.talismanId = getIntId(talismanId);
            info.count = readDefaultInt(data, index, "CHARGES");
            charges.append(info);
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, charges.count());
        for (int i = 0; i < charges.count(); ++i)
        {
            buffer.writeDefaultString("ID_TALIS", header.version,
                                      D2Item::shortType() +
                                      QString::number(charges[i].talismanId, 16).rightJustified(4, '0').toLower());
            buffer.writeDefaultInt("CHARGES", charges[i].count);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2TalismanCharges)
