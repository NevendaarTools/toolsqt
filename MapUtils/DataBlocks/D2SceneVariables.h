#pragma once
#include "../DataBlock.h"

class D2SceneVariables : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidScenVariables";
        return res;
    }
    static int code()
    {
        return 0x18;
    }
    static QString shortType()
    {
        return "SV";
    }
    static int intType()
    {
        return IDataBlock::Variables;
    }

    D2SceneVariables()
    {
        uid.first = intType();
        uid.second = 0;
    }

    struct SceneVariable
    {
        int id;
        QString name;
        int value;

    };

    QList<SceneVariable> variables;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
        {
            SceneVariable elem ;
            elem.id = readDefaultInt(data, index, "ID");
            elem.name = readStringWithLenght(data, index, "NAME");
            elem.value = readDefaultInt(data, index, "VALUE");
            variables.append(elem);
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, variables.count());
        for (int i = 0; i < variables.count(); ++i)
        {
            SceneVariable elem = variables[i];
            buffer.writeDefaultInt("ID", elem.id);
            buffer.writeString("NAME", elem.name);
            buffer.writeDefaultInt("VALUE", elem.value);

        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2SceneVariables)
