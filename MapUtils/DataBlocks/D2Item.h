#pragma once
#include "../DataBlock.h"
#include "D2Stack.h"

class D2Item : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidItem";
        return res;
    }
    static int code()
    {
        return 0x0f;
    }
    static QString shortType()
    {
        return "IM";
    }
    static int intType()
    {
        return IDataBlock::Item;
    }

    D2Item()
    {
        uid.first = intType();
    }

    QString itemType = "G000IG2001";

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        itemType = readStringWithLenght(data, index, "ITEM_TYPE");

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("ITEM_ID", header.version, objId);
        buffer.writeString("ITEM_TYPE", itemType);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Item)
