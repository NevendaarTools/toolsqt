#pragma once
#include "../DataBlock.h"
#include <variant>
#include <QDebug>

class D2Event : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidEvent";
        return res;
    }
    static int code()
    {
        return 0x10;
    }
    static QString shortType()
    {
        return "EV";
    }
    static int intType()
    {
        return IDataBlock::Event;
    }

    D2Event()
    {
        uid.first = intType();
    }
    QString name_txt = "";

    bool human = false;
    bool dwarf = false;
    bool undead = false;
    bool heretic = false;
    bool neutral = false;
    bool elf = false;

    bool verhuman = false;
    bool verdwarf = false;
    bool verundead = false;
    bool verheretic = false;
    bool verneutral = false;
    bool verelf = false;

    bool enabled = true;
    bool occurOnce = true;

    int chance = 100;
    int order = 0;

    struct EventCondition
    {
        enum ConditionType
        {
            FREQUENCY = 0,
            ENTERING_A_PREDEFINED_ZONE = 2,
            ENTERING_A_CITY = 3,
            OWNING_A_CITY = 4,
            DESTROY_STACK = 5,
            OWNING_AN_ITEM = 6,
            SPECIFIC_LEADER_OWNING_AN_ITEM = 7,
            DIPLOMACY_RELATIONS = 8,
            ALLIANCE = 9,
            LOOTING_A_RUIN = 10,
            TRANSFORMING_LAND = 11,
            VISITING_A_SITE = 12,
            STACK_IN_LOCATION = 14,
            STACK_IN_CITY = 15,
            ITEM_TO_LOCATION = 16,
            STACK_EXISTANCE = 17,
            VARIABLE_IS_IN_RANGE = 18,
            RESOURCE_AMOUNT = 19,
            CHECK_GAMEMODE = 20,
            CHECK_FOR_HUMAN = 21,
            COMPARE_VAR = 22,
            CUSTOM_SCRIPT = 23
        };

        void read(ConditionType type,const QByteArray &data, int &index)
        {
            category = type;
            switch (type) {
            case D2Event::EventCondition::RESOURCE_AMOUNT:
            {
                ConditionResourceAmount cond;
                cond.bank = readStringWithLenght(data, index, "BANK");
                cond.greaterOrEqual = readBool(data, index, "GRE");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::FREQUENCY:
            {
                ConditionFrequency cond;
                cond.frequency = readDefaultInt(data, index, "FREQUENCY");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE:
            {
                ConditionEnterZone cond;
                cond.locId = readDefaultString(data, index, "ID_LOC");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::ENTERING_A_CITY:
            {
                ConditionEnterCity cond;
                cond.villageId = readDefaultString(data, index, "ID_CITY");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::OWNING_A_CITY:
            {
                ConditionOwningCity cond;
                cond.villageId = readDefaultString(data, index, "ID_CITY");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::DESTROY_STACK:
            {
                ConditionDestroyStack cond;
                cond.stackId = readDefaultString(data, index, "ID_STACK");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::OWNING_AN_ITEM:
            {
                ConditionOwningItem cond;
                cond.itemType = readStringWithLenght(data, index, "TYPE_ITEM");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM:
            {
                ConditionLeaderOwningItem cond;
                cond.itemType = readStringWithLenght(data, index, "TYPE_ITEM");
                cond.stackId = readDefaultString(data, index, "ID_STACK");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::DIPLOMACY_RELATIONS:
            {
                ConditionDiplomacyRelation cond;
                cond.player1 = readDefaultString(data, index, "ID_PLAYER1");
                cond.player2 = readDefaultString(data, index, "ID_PLAYER2");
                cond.diplomacy = readDefaultInt(data, index, "DIPLOMACY");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::ALLIANCE:
            {
                ConditionAlliance cond;
                cond.player1 = readDefaultString(data, index, "ID_PLAYER1");
                cond.player2 = readDefaultString(data, index, "ID_PLAYER2");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::LOOTING_A_RUIN:
            {
                ConditionLootingRuin cond;
                cond.ruinId = readDefaultString(data, index, "ID_RUIN");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::TRANSFORMING_LAND:
            {
                ConditionTransformLand cond;
                cond.value = readDefaultInt(data, index, "PCT_LAND");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::VISITING_A_SITE:
            {
                ConditionVisitingSite cond;
                cond.siteId = readDefaultString(data, index, "ID_SITE");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::STACK_IN_LOCATION:
            {
                ConditionStackInLocation cond;
                cond.stackId = readDefaultString(data, index, "ID_STACK");
                cond.locId = readDefaultString(data, index, "ID_LOC");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::STACK_IN_CITY:
            {
                ConditionStackInCity cond;
                cond.stackId = readDefaultString(data, index, "ID_STACK");
                cond.villageId = readDefaultString(data, index, "ID_CITY");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::ITEM_TO_LOCATION:
            {
                ConditionItemToLocation cond;
                cond.itemType = readStringWithLenght(data, index, "TYPE_ITEM");
                cond.locId = readDefaultString(data, index, "ID_LOC");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::STACK_EXISTANCE:
            {
                ConditionStackExist cond;
                cond.stackId = readDefaultString(data, index, "ID_STACK");
                int exist = readDefaultInt(data, index, "MISC_INT");
                cond.exist = exist == 0;
                condition = cond;
                break;
            }
            case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
            {
                ConditionVarInRange cond;
                cond.var1 = readDefaultInt(data, index, "MISC_INT");
                cond.min_1 = readDefaultInt(data, index, "MISC_INT2");
                cond.max_1 = readDefaultInt(data, index, "MISC_INT3");
                cond.var2 = readDefaultInt(data, index, "MISC_INT4");
                cond.min_2 = readDefaultInt(data, index, "MISC_INT5");
                cond.max_2 = readDefaultInt(data, index, "MISC_INT6");
                cond.relation = readDefaultInt(data, index, "MISC_INT7");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::COMPARE_VAR:
            {
                ConditionCompareVar cond;
                cond.var1 = readDefaultInt(data, index, "VAR1");
                cond.var2 = readDefaultInt(data, index, "VAR2");
                qDebug()<<"#######"<<cond.var1;
                qDebug()<<"#######"<<(cond.var1>>28);
                cond.relation = (ConditionCompareVar::CompareType)readDefaultInt(data, index, "CMP");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::CUSTOM_SCRIPT:
            {
                ConditionCustomScript cond;
                int codeLen = readDefaultInt(data, index, "CODE_LEN");

                index += 8;//CODE + 4
                cond.code = readString(data, index, codeLen);
                codeLen = readDefaultInt(data, index, "DESCR_LEN");
                index += 9;//DESCR + 4
                cond.desc = readString(data, index, codeLen);
                condition = cond;
                break;
            }
            case D2Event::EventCondition::CHECK_GAMEMODE:
            {
                ConditionGameMode cond;
                cond.mode = (ConditionGameMode::GameMode)readDefaultInt(data, index, "MODE");
                condition = cond;
                break;
            }
            case D2Event::EventCondition::CHECK_FOR_HUMAN:
            {
                ConditionPlayerType cond;
                cond.isAI = readBool(data, index, "AI");
                condition = cond;
                break;
            }
            default:
                int customType = (int)type;
                qDebug()<<"customType = "<<customType;
                break;
            }
        }

        void write(ByteBuffer& buffer, const MapHeaderBlock& header) const
        {
            switch (category)
            {
            case D2Event::EventCondition::RESOURCE_AMOUNT:
            {
                auto& cond = std::get<ConditionResourceAmount>(condition);
                buffer.writeString("BANK", cond.bank);
                buffer.writeBool("GRE", cond.greaterOrEqual);
                break;
            }
            case ConditionType::FREQUENCY:
            {
                auto& cond = std::get<ConditionFrequency>(condition);
                buffer.writeDefaultInt("FREQUENCY", cond.frequency);
                break;
            }
            case ConditionType::ENTERING_A_PREDEFINED_ZONE:
            {
                auto& cond = std::get<ConditionEnterZone>(condition);
                buffer.writeDefaultString("ID_LOC", header.version, cond.locId);
                break;
            }
            case ConditionType::ENTERING_A_CITY:
            {
                auto& cond = std::get<ConditionEnterCity>(condition);
                buffer.writeDefaultString("ID_CITY", header.version, cond.villageId);
                break;
            }
            case ConditionType::OWNING_A_CITY:
            {
                auto& cond = std::get<ConditionOwningCity>(condition);
                buffer.writeDefaultString("ID_CITY", header.version, cond.villageId);
                break;
            }
            case ConditionType::DESTROY_STACK:
            {
                auto& cond = std::get<ConditionDestroyStack>(condition);
                buffer.writeDefaultString("ID_STACK", header.version, cond.stackId);
                break;
            }
            case ConditionType::OWNING_AN_ITEM:
            {
                auto& cond = std::get<ConditionOwningItem>(condition);
                buffer.writeDefaultString("TYPE_ITEM", cond.itemType);
                break;
            }
            case ConditionType::SPECIFIC_LEADER_OWNING_AN_ITEM:
            {
                auto& cond = std::get<ConditionLeaderOwningItem>(condition);
                buffer.writeDefaultString("TYPE_ITEM", cond.itemType);
                buffer.writeDefaultString("ID_STACK", header.version, cond.stackId);
                break;
            }
            case ConditionType::DIPLOMACY_RELATIONS:
            {
                auto& cond = std::get<ConditionDiplomacyRelation>(condition);
                buffer.writeDefaultString("ID_PLAYER1", header.version, cond.player1);
                buffer.writeDefaultString("ID_PLAYER2", header.version, cond.player2);
                buffer.writeDefaultInt("DIPLOMACY", cond.diplomacy);
                break;
            }
            case ConditionType::ALLIANCE:
            {
                auto& cond = std::get<ConditionAlliance>(condition);
                buffer.writeDefaultString("ID_PLAYER1", header.version, cond.player1);
                buffer.writeDefaultString("ID_PLAYER2", header.version, cond.player2);
                break;
            }
            case ConditionType::LOOTING_A_RUIN:
            {
                auto& cond = std::get<ConditionLootingRuin>(condition);
                buffer.writeDefaultString("ID_RUIN", header.version, cond.ruinId);
                break;
            }
            case ConditionType::TRANSFORMING_LAND:
            {
                auto& cond = std::get<ConditionTransformLand>(condition);
                buffer.writeDefaultInt("PCT_LAND", cond.value);
                break;
            }
            case ConditionType::VISITING_A_SITE:
            {
                auto& cond = std::get<ConditionVisitingSite>(condition);
                buffer.writeDefaultString("ID_SITE", header.version, cond.siteId);
                break;
            }
            case ConditionType::STACK_IN_LOCATION:
            {
                auto& cond = std::get<ConditionStackInLocation>(condition);
                buffer.writeDefaultString("ID_STACK", header.version, cond.stackId);
                buffer.writeDefaultString("ID_LOC", header.version, cond.locId);
                break;
            }
            case D2Event::EventCondition::STACK_IN_CITY:
            {
                const auto& cond = std::get<ConditionStackInCity>(condition);
                buffer.writeDefaultString("ID_STACK", header.version, cond.stackId);
                buffer.writeDefaultString("ID_CITY", header.version, cond.villageId);
                break;
            }
            case D2Event::EventCondition::ITEM_TO_LOCATION:
            {
                const auto& cond = std::get<ConditionItemToLocation>(condition);
                buffer.writeDefaultString("TYPE_ITEM", cond.itemType);
                buffer.writeDefaultString("ID_LOC", header.version, cond.locId);
                break;
            }
            case D2Event::EventCondition::STACK_EXISTANCE:
            {
                const auto& cond = std::get<ConditionStackExist>(condition);
                buffer.writeDefaultString("ID_STACK", header.version, cond.stackId);
                buffer.writeDefaultInt("MISC_INT", cond.exist ? 0 : 1);
                break;
            }
            case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
            {
                const auto& cond = std::get<ConditionVarInRange>(condition);
                buffer.writeDefaultInt("MISC_INT", cond.var1);
                buffer.writeDefaultInt("MISC_INT2", cond.min_1);
                buffer.writeDefaultInt("MISC_INT3", cond.max_1);
                buffer.writeDefaultInt("MISC_INT4", cond.var2);
                buffer.writeDefaultInt("MISC_INT5", cond.min_2);
                buffer.writeDefaultInt("MISC_INT6", cond.max_2);
                buffer.writeDefaultInt("MISC_INT7", cond.relation);
                break;
            }
            case D2Event::EventCondition::COMPARE_VAR:
            {
                const auto& cond = std::get<ConditionCompareVar>(condition);
                buffer.writeDefaultInt("VAR1", cond.var1);
                buffer.writeDefaultInt("VAR2", cond.var2);
                buffer.writeDefaultInt("CMP", (int)cond.relation);
                break;
            }
            case D2Event::EventCondition::CUSTOM_SCRIPT:
            {
                const auto& cond = std::get<ConditionCustomScript>(condition);
                buffer.writeDefaultInt("CODE_LEN", cond.code.size());
                buffer.writeString("CODE", cond.code);
                buffer.writeDefaultInt("DESCR_LEN", cond.desc.size());
                buffer.writeString("DESCR", cond.desc);
                break;
            }
            case D2Event::EventCondition::CHECK_GAMEMODE:
            {
                const auto& cond = std::get<ConditionGameMode>(condition);
                buffer.writeDefaultInt("MODE", (int)cond.mode);
                break;
            }
            case D2Event::EventCondition::CHECK_FOR_HUMAN:
            {
                const auto& cond = std::get<ConditionPlayerType>(condition);
                buffer.writeBool("AI", cond.isAI);
                break;
            }
            default:
                qWarning() << "Unsupported condition category: " << category;
                break;
            }
        }

        QString toString() const
        {
            QString res =  D2Event::EventCondition::toString(category);
            return res;
        }

        static QString toString(D2Event::EventCondition::ConditionType category)
        {
            switch (category) {
            case D2Event::EventCondition::FREQUENCY:
                return "FREQUENCY";
                break;
            case D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE:
                return "ENTERING_A_PREDEFINED_ZONE";
                break;
            case D2Event::EventCondition::ENTERING_A_CITY:
                return "ENTERING_A_CITY";
                break;
            case D2Event::EventCondition::OWNING_A_CITY:
                return "OWNING_A_CITY";
                break;
            case D2Event::EventCondition::DESTROY_STACK:
                return "DESTROY_STACK";
                break;
            case D2Event::EventCondition::OWNING_AN_ITEM:
                return "OWNING_AN_ITEM";
                break;
            case D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM:
                return "SPECIFIC_LEADER_OWNING_AN_ITEM";
                break;
            case D2Event::EventCondition::DIPLOMACY_RELATIONS:
                return "DIPLOMACY_RELATIONS";
                break;
            case D2Event::EventCondition::ALLIANCE:
                return "ALLIANCE";
                break;
            case D2Event::EventCondition::LOOTING_A_RUIN:
                return "LOOTING_A_RUIN";
                break;
            case D2Event::EventCondition::TRANSFORMING_LAND:
                return "TRANSFORMING_LAND";
                break;
            case D2Event::EventCondition::VISITING_A_SITE:
                return "VISITING_A_SITE";
                break;
            case D2Event::EventCondition::STACK_IN_LOCATION:
                return "STACK_IN_LOCATION";
                break;
            case D2Event::EventCondition::STACK_IN_CITY:
                return "STACK_IN_CITY";
                break;
            case D2Event::EventCondition::ITEM_TO_LOCATION:
                return "ITEM_TO_LOCATION";
                break;
            case D2Event::EventCondition::STACK_EXISTANCE:
                return "STACK_EXISTANCE";
                break;
            case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
                return "VARIABLE_IS_IN_RANGE";
                break;
            case RESOURCE_AMOUNT:
                return "RESOURCE_AMOUNT";
                break;
            case CHECK_GAMEMODE:
                return "CHECK_GAMEMODE";
                break;
            case CHECK_FOR_HUMAN:
                return "CHECK_FOR_HUMAN";
                break;
            case COMPARE_VAR:
                return "COMPARE_VAR";
                break;
            case CUSTOM_SCRIPT:
                return "CUSTOM_SCRIPT";
                break;
            }
            return "Unknown condition!";
        }

        ConditionType category;

        struct ConditionResourceAmount
        {
            QString bank = "G0000:R0000:Y0000:E0000:W0000:B0000";
            /* If true, player must have more than specified amount to met the condition. */
            bool greaterOrEqual;
        };
        struct ConditionGameMode
        {
            enum class GameMode : int
            {
                Single,
                Hotseat,
                Online,
            };
            GameMode mode;
        };
        struct ConditionAlliance
        {
            QString player1;
            QString player2;
        };
        struct ConditionCompareVar
        {
            enum class CompareType : int
            {
                Equal,
                NotEqual,
                Greater,
                GreaterEqual,
                Less,
                LessEqual,
            };

            int var1;
            int var2;
            CompareType relation;
        };
        struct ConditionCustomScript
        {
            QString code;
            QString desc;
        };

        struct ConditionDestroyStack
        {
            QString stackId;
        };
        struct ConditionEnterZone
        {
            QString locId;
        };
        struct ConditionDiplomacyRelation
        {
            QString player1;
            QString player2;
            int diplomacy;
        };
        struct ConditionFrequency
        {
            int frequency;
        };
        struct ConditionEnterCity
        {
            QString villageId;
        };
        struct ConditionItemToLocation
        {
            QString itemType;
            QString locId;
        };
        struct ConditionLootingRuin
        {
            QString ruinId;
        };
        struct ConditionOwningCity
        {
            QString villageId;
        };
        struct ConditionOwningItem
        {
            QString itemType;
        };
        struct ConditionLeaderOwningItem
        {
            QString itemType;
            QString stackId;
        };
        struct ConditionStackExist
        {
            QString stackId;
            bool exist;
        };
        struct ConditionStackInCity
        {
            QString stackId;
            QString villageId;
        };
        struct ConditionStackInLocation
        {
            QString stackId;
            QString locId;
        };
        struct ConditionTransformLand
        {
            int value;
        };
        struct ConditionVarInRange
        {
            int var1;
            int min_1;
            int max_1;
            int var2;
            int min_2;
            int max_2;
            int relation;
        };
        struct ConditionVisitingSite
        {
            QString siteId;
        };
        struct ConditionPlayerType
        {
            bool isAI;
        };
        std::variant<
                        ConditionDestroyStack,
                        ConditionAlliance,
                        ConditionEnterZone,
                        ConditionDiplomacyRelation,
                        ConditionEnterCity,
                        ConditionFrequency,
                        ConditionItemToLocation,
                        ConditionLootingRuin,
                        ConditionOwningCity,
                        ConditionOwningItem,
                        ConditionLeaderOwningItem,
                        ConditionStackExist,
                        ConditionStackInCity,
                        ConditionStackInLocation,
                        ConditionTransformLand,
                        ConditionVarInRange,
                        ConditionVisitingSite,
                        ConditionCompareVar,
                        ConditionCustomScript,
                        ConditionGameMode,
                        ConditionPlayerType,
                        ConditionResourceAmount
        >
        condition;

    };

    struct EventEffect
    {
        enum EffectType
        {
            WIN_OR_LOSE_SCENARIO = 0,
            CREATE_NEW_STACK = 1,
            CAST_SPELL_ON_TRIGGERER = 2,
            CAST_SPELL_AT_SPECIFIC_LOCATION = 3,
            CHANGE_STACK_OWNER = 4,
            MOVE_STACK_NEXT_TO_TRIGGERER = 5,
            GO_INTO_BATTLE = 6,
            ENABLE_DISABLE_ANOTHER_EVENT = 7,
            GIVE_SPELL = 8,
            GIVE_ITEM = 9,
            MOVE_STACK_TO_SPECIFIC_LOCATION = 10,
            ALLY_TWO_AI_PLAYERS = 11,
            CHANGE_PLAYER_DIPLOMACY_METER = 12,
            UNFOG_OR_FOG_AN_AREA_ON_THE_MAP = 13,
            REMOVE_MOUNTAINS_AROUND_A_LOCATION = 14,
            REMOVE_LANDMARK = 15,
            CHANGE_SCENARIO_OBJECTIVE_TEXT = 16,
            DISPLAY_POPUP_MESSAGE = 17,
            CHANGE_STACK_ORDER = 18,
            DESTROY_ITEM = 19,
            REMOVE_STACK = 20,
            CHANGE_LANDMARK = 21,
            CHANGE_TERRAIN = 22,
            MODIFY_VARIABLE = 23
        };

        void read(EffectType type,const QByteArray &data, int &index, const MapHeaderBlock& header, const QString& id)
        {
            category = type;
            switch (type) {
            case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
            {
                WinLooseEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.win = readBool(data, index, "WIN_SCEN");
                effect.playerId = readDefaultString(data, index, "ID_PLAYER1");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CREATE_NEW_STACK:
            {
                CreateStackEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.templateId = readDefaultString(data, index, "ID_STKTEMP");
                effect.locId = readDefaultString(data, index, "ID_LOC");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
            {
                CastSpellOnTriggerEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.spellType = readStringWithLenght(data, index, "TYPE_SPELL");
                effect.playerId1 = readDefaultString(data, index, "ID_PLAYER1");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
            {
                CastSpellOnLocationEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.spellType = readStringWithLenght(data, index, "TYPE_SPELL");
                effect.locId = readDefaultString(data, index, "ID_LOC");
                effect.playerId1 = readDefaultString(data, index, "ID_PLAYER1");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CHANGE_STACK_OWNER:
            {
                ChangeStackOwnerEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.stackId = readDefaultString(data, index, "ID_STACK");
                effect.playerId1 = readDefaultString(data, index, "ID_PLAYER1");
                effect.firstOnly = readBool(data, index, "FIRST_ONLY");
                effect.anim = readBool(data, index, "PLAY_ANIM");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
            {
                MoveStackToTriggererEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.stackId = readDefaultString(data, index, "ID_STACK");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::GO_INTO_BATTLE:
            {
                GoBattleEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.stackId = readDefaultString(data, index, "ID_STACK");
                effect.firstOnly = readBool(data, index, "FIRST_ONLY");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
            {
                DisableEventEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.eventId = readDefaultString(data, index, "ID_EVENT");
                effect.enable = readBool(data, index, "ENABLE");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::GIVE_SPELL:
            {
                GiveSpellEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.spellType = readStringWithLenght(data, index, "TYPE_SPELL");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::GIVE_ITEM:
            {
                GiveItemEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.giveTo = readDefaultInt(data, index, "GIVETO");
                effect.itemType = readStringWithLenght(data, index, "TYPE_ITEM");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
            {
                MoveStackToLocationEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.stackTmpId = readDefaultString(data, index, "ID_STKTEMP");
                effect.locId = readDefaultString(data, index, "ID_LOC");
                if (header.mapType == "D2EESFISIG")
                {
                    effect.boolVal = readBool(data, index, "BOOLVALUE");
                }
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
            {
                AllyAIPlayersEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.playerId1 = readDefaultString(data, index, "ID_PLAYER1");
                effect.playerId2 = readDefaultString(data, index, "ID_PLAYER2");
                effect.permally = readBool(data, index, "PERMALLI");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
            {
                ChangeDiplomacyEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.playerId1 = readDefaultString(data, index, "ID_PLAYER1");
                effect.playerId2 = readDefaultString(data, index, "ID_PLAYER2");
                effect.diplomacy = readDefaultInt(data, index, "DIPLOMACY");
                effect.enabled = readBool(data, index, "ENABLE");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
            {
                ChangeFogEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.locId = readDefaultString(data, index, "ID_LOC");
                int count = readDefaultInt(data, index, id);
                for (int i = 0; i < count; ++i)
                {
                    ChangeFogEffect::FogEntry entry;
                    entry.eventId = readDefaultString(data, index, "EVENT_ID");
                    entry.player = readDefaultString(data, index, "PLAYER");
                    effect.events << entry;
                }
                if (header.mapType == "D2EESFISIG")
                {
                    effect.enable = readBool(data, index, "ENABLE");
                    effect.value = readDefaultInt(data, index, "NUMVALUE");
                }
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
            {
                RemoveMountainsEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.locId = readDefaultString(data, index, "ID_LOC");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::REMOVE_LANDMARK:
            {
                RemoveLandMarkEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.lmarkId = readDefaultString(data, index, "ID_LMARK");
                if (header.mapType == "D2EESFISIG")
                {
                    effect.boolVal = readBool(data, index, "BOOLVALUE");
                }
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
            {
                ChangeScenarioTextEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.text = readStringWithLenght(data, index, "OBJECT_TXT");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
            {
                DisplayPopupEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.text = readStringWithLenght(data, index, "POPUP_TXT");
                effect.music = readStringWithLenght(data, index, "MUSIC");
                effect.sound = readStringWithLenght(data, index, "SOUND");
                effect.image = readStringWithLenght(data, index, "IMAGE");
                effect.image2 = readStringWithLenght(data, index, "IMAGE2");
                effect.leftSide = readBool(data, index, "LEFT_SIDE");
                effect.popupShow = readStringWithLenght(data, index, "POPUP_SHOW");
                if (header.mapType == "D2EESFISIG")
                {
                    effect.boolValue = readBool(data, index, "BOOLVALUE");
                }
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CHANGE_STACK_ORDER:
            {
                ChangeStackOrderEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.stackId = readDefaultString(data, index, "ID_STACK");
                effect.orderTarget = readDefaultString(data, index, "ORDER_TARG");
                effect.firstOnly = readBool(data, index, "FIRST_ONLY");
                effect.order = readDefaultInt(data, index, "ORDER");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::DESTROY_ITEM:
            {
                DestroyItemEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.itemType = readStringWithLenght(data, index, "TYPE_ITEM");
                effect.triggerOnly = readBool(data, index, "TRIG_ONLY");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::REMOVE_STACK:
            {
                RemoveStackEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.stackId = readDefaultString(data, index, "ID_STACK");
                effect.firstOnly = readBool(data, index, "FIRST_ONLY");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CHANGE_LANDMARK:
            {
                ChangeLandmarkEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.lmarkId = readDefaultString(data, index, "ID_LMARK");
                effect.lmarkType = readStringWithLenght(data, index, "TYPE_LMARK");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::CHANGE_TERRAIN:
            {
                ChangeTerrainEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.locId = readDefaultString(data, index, "ID_LOC");
                effect.lookup = readDefaultInt(data, index, "LOOKUP");
                effect.value = readDefaultInt(data, index, "NUMVALUE");
                this->effect = effect;
                break;
            }
            case D2Event::EventEffect::MODIFY_VARIABLE:
            {
                ModifyVarEffect effect;
                effect.num = readDefaultInt(data, index, "NUM");
                effect.lookup = readDefaultInt(data, index, "LOOKUP");
                effect.val1 = readDefaultInt(data, index, "NUMVALUE");
                effect.val2 = readDefaultInt(data, index, "NUMVALUE2");
                this->effect = effect;
                break;
            }

            }
        }


        void write(ByteBuffer & buffer, const MapHeaderBlock& header, const QString& objId) const
        {
            switch (category) {
            case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
            {
                const WinLooseEffect& effect = std::get<WinLooseEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeBool("WIN_SCEN", effect.win);
                buffer.writeDefaultString("ID_PLAYER1", header.version, effect.playerId);
                break;
            }
            case D2Event::EventEffect::CREATE_NEW_STACK:
            {
                const CreateStackEffect& effect = std::get<CreateStackEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_STKTEMP", header.version, effect.templateId);
                buffer.writeDefaultString("ID_LOC", header.version, effect.locId);
                break;
            }
            case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
            {
                const CastSpellOnTriggerEffect& effect = std::get<CastSpellOnTriggerEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("TYPE_SPELL", effect.spellType);
                buffer.writeDefaultString("ID_PLAYER1", header.version, effect.playerId1);
                break;
            }
            case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
            {
                const CastSpellOnLocationEffect& effect = std::get<CastSpellOnLocationEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("TYPE_SPELL", effect.spellType);
                buffer.writeDefaultString("ID_LOC", header.version, effect.locId);
                buffer.writeDefaultString("ID_PLAYER1", header.version, effect.playerId1);
                break;
            }
            case D2Event::EventEffect::CHANGE_STACK_OWNER:
            {
                const ChangeStackOwnerEffect& effect = std::get<ChangeStackOwnerEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_STACK", header.version, effect.stackId);
                buffer.writeDefaultString("ID_PLAYER1", header.version, effect.playerId1);
                buffer.writeBool("FIRST_ONLY", effect.firstOnly);
                buffer.writeBool("PLAY_ANIM", effect.anim);
                break;
            }
            case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
            {
                const MoveStackToTriggererEffect& effect = std::get<MoveStackToTriggererEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_STACK", header.version, effect.stackId);
                break;
            }
            case D2Event::EventEffect::GO_INTO_BATTLE:
            {
                const GoBattleEffect& effect = std::get<GoBattleEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_STACK", header.version, effect.stackId);
                buffer.writeBool("FIRST_ONLY", effect.firstOnly);
                break;
            }
            case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
            {
                const DisableEventEffect& effect = std::get<DisableEventEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_EVENT", header.version, effect.eventId);
                buffer.writeBool("ENABLE", effect.enable);
                break;
            }
            case D2Event::EventEffect::GIVE_SPELL:
            {
                const GiveSpellEffect& effect = std::get<GiveSpellEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("TYPE_SPELL", effect.spellType);
                break;
            }
            case D2Event::EventEffect::GIVE_ITEM:
            {
                const GiveItemEffect& effect = std::get<GiveItemEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultInt("GIVETO", effect.giveTo);
                buffer.writeDefaultString("TYPE_ITEM", effect.itemType);
                break;
            }
            case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
            {
                const MoveStackToLocationEffect& effect = std::get<MoveStackToLocationEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_STKTEMP", header.version, effect.stackTmpId);
                buffer.writeDefaultString("ID_LOC", header.version, effect.locId);
                if (header.mapType == "D2EESFISIG")
                {
                    buffer.writeBool("BOOLVALUE", effect.boolVal);
                }
                break;
            }
            case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
            {
                const AllyAIPlayersEffect& effect = std::get<AllyAIPlayersEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_PLAYER1", header.version, effect.playerId1);
                buffer.writeDefaultString("ID_PLAYER2", header.version, effect.playerId2);
                buffer.writeBool("PERMALLI", effect.permally);
                break;
            }
            case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
            {
                const ChangeDiplomacyEffect& effect = std::get<ChangeDiplomacyEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_PLAYER1", header.version, effect.playerId1);
                buffer.writeDefaultString("ID_PLAYER2", header.version, effect.playerId2);
                buffer.writeDefaultInt("DIPLOMACY", effect.diplomacy);
                buffer.writeBool("ENABLE", effect.enabled);
                break;
            }
            case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
            {
                const auto& effect = std::get<ChangeFogEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_LOC", header.version, effect.locId);
                buffer.writeDefaultInt(header.version + objId, effect.events.count());
                for (const auto& entry : effect.events)
                {
                    buffer.writeDefaultString("EVENT_ID", header.version, entry.eventId);
                    buffer.writeDefaultString("PLAYER", header.version, entry.player);
                }
                if (header.mapType == "D2EESFISIG")
                {
                    buffer.writeBool("ENABLE", effect.enable);
                    buffer.writeDefaultInt("NUMVALUE", effect.value);
                }
                break;
            }
            case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
            {
                const auto& effect = std::get<RemoveMountainsEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_LOC", header.version, effect.locId);
                break;
            }
            case D2Event::EventEffect::REMOVE_LANDMARK:
            {
                const auto& effect = std::get<RemoveLandMarkEffect>(this->effect);
                buffer.writeDefaultInt("NUM", effect.num);
                buffer.writeDefaultString("ID_LMARK", header.version, effect.lmarkId);
                if (header.mapType == "D2EESFISIG")
                {
                    buffer.writeBool("BOOLVALUE", effect.boolVal);
                }
                break;
            }
            case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
            {
                const ChangeScenarioTextEffect& effectData = std::get<ChangeScenarioTextEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeString("OBJECT_TXT", effectData.text);
                break;
            }
            case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
            {
                const DisplayPopupEffect& effectData = std::get<DisplayPopupEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeString("POPUP_TXT", effectData.text);
                buffer.writeString("MUSIC", effectData.music);
                buffer.writeString("SOUND", effectData.sound);
                buffer.writeString("IMAGE", effectData.image);
                buffer.writeString("IMAGE2", effectData.image2);
                buffer.writeBool("LEFT_SIDE", effectData.leftSide);
                buffer.writeString("POPUP_SHOW", effectData.popupShow);
                if (header.mapType == "D2EESFISIG") {
                    buffer.writeBool("BOOLVALUE", effectData.boolValue);
                }
                break;
            }
            case D2Event::EventEffect::CHANGE_STACK_ORDER:
            {
                const ChangeStackOrderEffect& effectData = std::get<ChangeStackOrderEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeDefaultString("ID_STACK", header.version, effectData.stackId);
                buffer.writeDefaultString("ORDER_TARG", header.version, effectData.orderTarget);
                buffer.writeBool("FIRST_ONLY", effectData.firstOnly);
                buffer.writeDefaultInt("ORDER", effectData.order);
                break;
            }
            case D2Event::EventEffect::DESTROY_ITEM:
            {
                const DestroyItemEffect& effectData = std::get<DestroyItemEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeString("TYPE_ITEM", effectData.itemType);
                buffer.writeBool("TRIG_ONLY", effectData.triggerOnly);
                break;
            }
            case D2Event::EventEffect::REMOVE_STACK:
            {
                const RemoveStackEffect& effectData = std::get<RemoveStackEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeDefaultString("ID_STACK", header.version, effectData.stackId);
                buffer.writeBool("FIRST_ONLY", effectData.firstOnly);
                break;
            }
            case D2Event::EventEffect::CHANGE_LANDMARK:
            {
                const ChangeLandmarkEffect& effectData = std::get<ChangeLandmarkEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeDefaultString("ID_LMARK", header.version, effectData.lmarkId);
                buffer.writeString("TYPE_LMARK", effectData.lmarkType);
                break;
            }
            case D2Event::EventEffect::CHANGE_TERRAIN:
            {
                const ChangeTerrainEffect& effectData = std::get<ChangeTerrainEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeDefaultString("ID_LOC", header.version, effectData.locId);
                buffer.writeDefaultInt("LOOKUP", effectData.lookup);
                buffer.writeDefaultInt("NUMVALUE", effectData.value);
                break;
            }
            case D2Event::EventEffect::MODIFY_VARIABLE:
            {
                const ModifyVarEffect& effectData = std::get<ModifyVarEffect>(effect);
                buffer.writeDefaultInt("NUM", effectData.num);
                buffer.writeDefaultInt("LOOKUP", effectData.lookup);
                buffer.writeDefaultInt("NUMVALUE", effectData.val1);
                buffer.writeDefaultInt("NUMVALUE2", effectData.val2);
                break;
            }
            default:
            {
                break;
            }
            }
        }

        struct ChangeTerrainEffect
        {
            int  num;
            QString locId;
            int lookup;
            int value;
        };

        struct AllyAIPlayersEffect
        {
            int  num;
            QString playerId1;
            QString playerId2;
            bool permally;
        };
        struct CastSpellOnLocationEffect
        {
            int  num;
            QString spellType;
            QString locId;
            QString playerId1;
        };
        struct CastSpellOnTriggerEffect
        {
            int  num;
            QString spellType;
            QString playerId1;
        };
        struct ChangeLandmarkEffect
        {
            int  num;
            QString lmarkId;
            QString lmarkType;
        };

        struct ChangeDiplomacyEffect
        {
            int  num;
            QString playerId1;
            QString playerId2;
            int diplomacy;
            bool enabled;
        };
        struct ChangeScenarioTextEffect
        {
            int  num;
            QString text;
        };
        struct ChangeStackOrderEffect
        {
            int  num;
            QString stackId;
            QString orderTarget;
            bool firstOnly;
            int order;
        };
        struct ChangeStackOwnerEffect
        {
            int  num;
            QString stackId;
            QString playerId1;
            bool firstOnly;
            bool anim;
        };
        struct CreateStackEffect
        {
            int  num;
            QString templateId;
            QString locId;
        };
        struct DestroyItemEffect
        {
            int  num;
            QString itemType;
            bool triggerOnly;
        };

        struct DisplayPopupEffect
        {
            int  num;
            QString text;
            QString music;
            QString sound;
            QString image;
            QString image2;
            bool leftSide;
            QString popupShow;
            bool boolValue;
        };

        struct DisableEventEffect
        {
            int  num;
            QString eventId;
            bool enable;
        };

        struct GiveItemEffect
        {
            int  num;
            int giveTo;
            //Triggerer = 0,
            //Capital = 1
            QString itemType;
        };

        struct GiveSpellEffect
        {
            int  num;
            QString spellType;
        };

        struct GoBattleEffect
        {
            int  num;
            QString stackId;
            bool firstOnly;
        };

        struct ModifyVarEffect
        {
            int  num;
            int lookup;
            int val1;
            int val2;
        };

        struct MoveStackToTriggererEffect
        {
            int  num;
            QString stackId;
        };

        struct MoveStackToLocationEffect
        {
            int  num;
            QString stackTmpId;
            QString locId;
            bool boolVal;
        };

        struct RemoveLandMarkEffect
        {
            int  num;
            QString lmarkId;
            bool boolVal;
        };

        struct RemoveMountainsEffect
        {
            int  num;
            QString locId;
        };

        struct RemoveStackEffect
        {
            int  num;
            QString stackId;
            bool firstOnly;
        };

        struct ChangeFogEffect
        {
            struct FogEntry
            {
                QString eventId;
                QString player;
            };

            int  num;
            QString locId;
            QVector<FogEntry> events;
            bool enable;
            int value;
        };

        struct WinLooseEffect
        {
            int  num;
            bool win;
            QString playerId;
        };

        EffectType category;
        std::variant <
            ChangeTerrainEffect,
            AllyAIPlayersEffect,
            CastSpellOnLocationEffect,
            CastSpellOnTriggerEffect,
            ChangeLandmarkEffect,
            ChangeDiplomacyEffect,
            ChangeScenarioTextEffect,
            ChangeStackOrderEffect,
            ChangeStackOwnerEffect,
            CreateStackEffect,
            DestroyItemEffect,
            DisplayPopupEffect,
            DisableEventEffect,
            GiveItemEffect,
            GiveSpellEffect,
            GoBattleEffect,
            ModifyVarEffect,
            MoveStackToTriggererEffect,
            MoveStackToLocationEffect,
            RemoveLandMarkEffect,
            RemoveMountainsEffect,
            RemoveStackEffect,
            ChangeFogEffect,
            WinLooseEffect
            > effect;

        static QString toString(EventEffect::EffectType category)
        {
            switch (category) {
            case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
                return "WIN_OR_LOSE_SCENARIO";
                break;
            case D2Event::EventEffect::CREATE_NEW_STACK:
                return "CREATE_NEW_STACK";
                break;
            case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
                return "CAST_SPELL_ON_TRIGGERER";
                break;
            case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
                return "CAST_SPELL_AT_SPECIFIC_LOCATION";
                break;
            case D2Event::EventEffect::CHANGE_STACK_OWNER:
                return "CHANGE_STACK_OWNER";
                break;
            case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
                return "MOVE_STACK_NEXT_TO_TRIGGERER";
                break;
            case D2Event::EventEffect::GO_INTO_BATTLE:
                return "GO_INTO_BATTLE";
                break;
            case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
                return "ENABLE_DISABLE_ANOTHER_EVENT";
                break;
            case D2Event::EventEffect::GIVE_SPELL:
                return "GIVE_SPELL";
                break;
            case D2Event::EventEffect::GIVE_ITEM:
                return "GIVE_ITEM";
                break;
            case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
                return "MOVE_STACK_TO_SPECIFIC_LOCATION";
                break;
            case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
                return "ALLY_TWO_AI_PLAYERS";
                break;
            case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
                return "CHANGE_PLAYER_DIPLOMACY_METER";
                break;
            case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
                return "UNFOG_OR_FOG_AN_AREA_ON_THE_MAP";
                break;
            case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
                return "REMOVE_MOUNTAINS_AROUND_A_LOCATION";
                break;
            case D2Event::EventEffect::REMOVE_LANDMARK:
                return "REMOVE_LANDMARK";
                break;
            case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
                return "CHANGE_SCENARIO_OBJECTIVE_TEXT";
                break;
            case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
                return "DISPLAY_POPUP_MESSAGE";
                break;
            case D2Event::EventEffect::CHANGE_STACK_ORDER:
                return "CHANGE_STACK_ORDER";
                break;
            case D2Event::EventEffect::DESTROY_ITEM:
                return "DESTROY_ITEM";
                break;
            case D2Event::EventEffect::REMOVE_STACK:
                return "REMOVE_STACK";
                break;
            case D2Event::EventEffect::CHANGE_LANDMARK:
                return "CHANGE_LANDMARK";
                break;
            case D2Event::EventEffect::CHANGE_TERRAIN:
                return "CHANGE_TERRAIN";
                break;
            case D2Event::EventEffect::MODIFY_VARIABLE:
                return "MODIFY_VARIABLE";
                break;

            }
            return "Unknown effect!";
        }

        QString toString() const
        {
            QString res =  D2Event::EventEffect::toString(category);
            return res;
        }
    };

    QList<EventCondition> conditions;
    QList<EventEffect> effects;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        name_txt = readStringWithLenght(data, index, "NAME_TXT");
        human = readBool(data, index, "HUMAN");
        dwarf = readBool(data, index, "DWARF");
        undead = readBool(data, index, "UNDEAD");
        heretic = readBool(data, index, "HERETIC");
        neutral = readBool(data, index, "NEUTRAL");
        if (header.mapType == "D2EESFISIG")
            elf = readBool(data, index, "ELF");

        verhuman = readBool(data, index, "VERHUMAN");
        verdwarf = readBool(data, index, "VERDWARF");
        verundead = readBool(data, index, "VERUNDEAD");
        verheretic = readBool(data, index, "VERHERETIC");
        verneutral = readBool(data, index, "VERNEUTRAL");
        if (header.mapType == "D2EESFISIG")
            verelf = readBool(data, index, "VERELF");

        enabled = readBool(data, index, "ENABLED");
        occurOnce = readBool(data, index, "OCCUR_ONCE");

        chance = readDefaultInt(data, index, "CHANCE");
        order = readDefaultInt(data, index, "ORDER");

        int count = readDefaultInt(data, index, "COND_QTY");
        for (int i = 0; i < count; ++i)
        {
            int type = readDefaultInt(data, index, "CATEGORY");
            EventCondition condition;
            condition.read((EventCondition::ConditionType)type, data, index);
            conditions<<condition;
        }
        count = readDefaultInt(data, index, "EFFECT_QTY");
        for (int i = 0; i < count; ++i)
        {
            int type = readDefaultInt(data, index, "CATEGORY");
            EventEffect effect;
            effect.category = (EventEffect::EffectType)type;
            effect.read((EventEffect::EffectType)type, data, index, header, objId);
            effects << effect;
        }

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("ID", header.version, objId);
        buffer.writeString("NAME_TXT", name_txt);
        buffer.writeBool("HUMAN", human);
        buffer.writeBool("DWARF", dwarf);
        buffer.writeBool("UNDEAD", undead);
        buffer.writeBool("HERETIC", heretic);
        buffer.writeBool("NEUTRAL", neutral);
        if (header.mapType == "D2EESFISIG")
            buffer.writeBool("ELF", elf);

        buffer.writeBool("VERHUMAN", verhuman);
        buffer.writeBool("VERDWARF", verdwarf);
        buffer.writeBool("VERUNDEAD", verundead);
        buffer.writeBool("VERHERETIC", verheretic);
        buffer.writeBool("VERNEUTRAL", verneutral);
        if (header.mapType == "D2EESFISIG")
            buffer.writeBool("VERELF", verelf);

        buffer.writeBool("ENABLED", enabled);
        buffer.writeBool("OCCUR_ONCE", occurOnce);

        buffer.writeDefaultInt("CHANCE", chance);
        buffer.writeDefaultInt("ORDER", order);

        buffer.writeDefaultInt("COND_QTY", conditions.count());
        for (const EventCondition &condition : conditions)
        {
            buffer.writeDefaultInt("CATEGORY", (int)condition.category);
            condition.write(buffer, header);
        }

        buffer.writeDefaultInt("EFFECT_QTY", effects.count());
        for (const EventEffect &effect : effects)
        {
            buffer.writeDefaultInt("CATEGORY", effect.category);
            effect.write(buffer, header, objId);
        }

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Event)
