#pragma once
#include "../DataBlock.h"

class D2Road : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidRoad";
        return res;
    }
    static int code()
    {
        return 0x0f;
    }
    static QString shortType()
    {
        return "RA";
    }
    static int intType()
    {
        return IDataBlock::Road;
    }

    D2Road()
    {
        uid.first = intType();
    }

    int roadIndex = 0;
    int var = 0;
    int posX = 0;
    int posY = 0;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "ROAD_ID");
        uid.second = getIntId(objId);
        roadIndex = readDefaultInt(data, index, "INDEX");
        var = readDefaultInt(data, index, "VAR");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("ROAD_ID", header.version, objId);
        buffer.writeDefaultInt("INDEX", roadIndex);
        buffer.writeDefaultInt("VAR", var);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Road)
