#pragma once
#include "../DataBlock.h"

class D2Mountains : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidMountains";
        return res;
    }
    static int code()
    {
        return 0x14;
    }
    static QString shortType()
    {
        return "ML";
    }
    static int intType()
    {
        return IDataBlock::Mountain;
    }

    D2Mountains()
    {
        uid.first = intType();
        uid.second = 0;
    }

    struct Mountain
    {
        int id;
        int sizeX;
        int sizeY;
        int posX;
        int posY;
        int image = 0;
        int race = 0;
    };
    QList<Mountain> mountains;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int dataSize = readDefaultInt(data, index, objId);
        for (int i = 0; i < dataSize; ++i)
        {
            Mountain mountain;
            mountain.id = readDefaultInt(data, index, "ID_MOUNT");
            mountain.sizeX = readDefaultInt(data, index, "SIZE_X");
            mountain.sizeY = readDefaultInt(data, index, "SIZE_Y");
            mountain.posX = readDefaultInt(data, index, "POS_X");
            mountain.posY = readDefaultInt(data, index, "POS_Y");
            mountain.image = readDefaultInt(data, index, "IMAGE");
            mountain.race = readDefaultInt(data, index, "RACE");

            mountains.append(mountain);
        }
        index = endIndex;
    }
    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeInt(header.version + objId, mountains.count(), 4);
        foreach(const Mountain & mountain, mountains)
        {
            buffer.writeDefaultInt("ID_MOUNT", mountain.id);
            buffer.writeDefaultInt("SIZE_X", mountain.sizeX);
            buffer.writeDefaultInt("SIZE_Y", mountain.sizeY);
            buffer.writeDefaultInt("POS_X", mountain.posX);
            buffer.writeDefaultInt("POS_Y", mountain.posY);
            buffer.writeDefaultInt("IMAGE", mountain.image);
            buffer.writeDefaultInt("RACE", mountain.race);
        }

        buffer.writeEnd();

        return buffer.GetData();
    }
    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Mountains)
