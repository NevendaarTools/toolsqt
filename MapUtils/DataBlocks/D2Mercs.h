#pragma once
#include "../DataBlock.h"
#include "D2Merchant.h"

class D2Mercs : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidSiteMercs";
        return res;
    }
    static int code()
    {
        return 0x14;
    }

    static QString shortType()
    {
        return "SI";
    }
    static int intType()
    {
        return IDataBlock::Merchant_mercs;
    }

    D2Mercs()
    {
        uid.first = intType();
    }

    struct MercsUnitEntry
    {
        QString id;
        int level;
        bool isUnique = false;
    };

    QString name = "";
    QString desc = "";
    int posX = 0;
    int posY = 0;
    int AIpriority = 3;
    int image = 0;
    QString imageIntf = "";
    QString currentVisiter = "000000";
    QList<MercsUnitEntry> units;
    QList<SiteVisitor> visiters;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        image = readDefaultInt(data, index, "IMG_ISO");
        imageIntf = readStringWithLenght(data, index, "IMG_INTF");
        name = readStringWithLenght(data, index, "TXT_TITLE");
        desc = readStringWithLenght(data, index, "TXT_DESC");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        currentVisiter = readStringWithLenght(data, index, "VISITER");
        AIpriority = readDefaultInt(data, index, "AIPRIORITY");
        int count = readDefaultInt(data, index, "QTY_UNIT");
        for (int i = 0; i < count; ++i)
        {
            MercsUnitEntry elem;
            elem.id = readStringWithLenght(data, index, "UNIT_ID");
            elem.level = readDefaultInt(data, index, "UNIT_LEVEL");
            elem.isUnique = readBool(data, index, "UNIT_UNIQ");
            units.append(elem);
        }
        count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
        {
            SiteVisitor elem;
            elem.siteId = readDefaultString(data, index, "SITE_ID");
            elem.visiter = readDefaultString(data, index, "VISITER");
            visiters.append(elem);
        }

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("SITE_ID", header.version, objId);
        buffer.writeDefaultInt("IMG_ISO", image);
        buffer.writeString("IMG_INTF", imageIntf);
        buffer.writeString("TXT_TITLE", name);
        buffer.writeString("TXT_DESC", desc);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeDefaultString("VISITER", currentVisiter);
        buffer.writeDefaultInt("AIPRIORITY", AIpriority);
        buffer.writeDefaultInt("QTY_UNIT",  units.count());
        for (int i = 0; i < units.count() ; ++i)
        {
            MercsUnitEntry elem = units[i];
            buffer.writeString("UNIT_ID", elem.id);
            buffer.writeDefaultInt( "UNIT_LEVEL", elem.level);
            buffer.writeBool("UNIT_UNIQ", elem.isUnique);

        }
        buffer.writeDefaultInt(header.version + objId, visiters.count());
        for (int i = 0; i < visiters.count(); ++i)
        {
            SiteVisitor elem = visiters[i];
            buffer.writeDefaultString("SITE_ID", header.version, elem.siteId);
            buffer.writeDefaultString("VISITER", header.version, elem.visiter);

        }

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};

REGISTER_TYPE(D2Mercs)
