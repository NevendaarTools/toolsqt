#pragma once
#include "../DataBlock.h"

class D2LandMark : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidLandmark";
        return res;
    }
    static int code()
    {
        return 0x13;//19
    }
    static QString shortType()
    {
        return "MM";
    }
    static int intType()
    {
        return IDataBlock::LandMark;
    }

    D2LandMark()
    {
        uid.first = intType();
    }

    QString baseType;
    int posX;
    int posY;
    QString desc = "";

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        baseType = readStringWithLenght(data, index, "TYPE");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");

        if ((header.mapType == "D2EESFISIG" || header.offset == 0) && index < endIndex - 10)
            desc = readStringWithLenght(data, index, "DESC_TXT");

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("LMARK_ID", header.version, objId);
        buffer.writeString("TYPE", baseType);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        if (desc.size() > 0)
            buffer.writeString("DESC_TXT", desc);

        buffer.writeEnd();
        return buffer.GetData();
    }
    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2LandMark)
