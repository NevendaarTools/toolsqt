#pragma once
#include "../DataBlock.h"

class D2QuestLog : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidQuestLog";
        return res;
    }
    static int code()
    {
        return 0x13;
    }
    static QString shortType()
    {
        return "QL";
    }
    static int intType()
    {
        return IDataBlock::QuestLog;
    }

    D2QuestLog()
    {
        uid.first = intType();
        uid.second = 0;
    }

    struct MidQuestLogSequence
    {
        QString playerId;
        int seqNumber;
        int currentTurn;
        int type;
        QString eventId;
        int effectNumber;
    };
    QList <MidQuestLogSequence> sequences;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
        {
            MidQuestLogSequence elem;
            elem.playerId = readDefaultString(data, index, "ID_PLAYER");
            elem.seqNumber = readDefaultInt(data, index, "SEQ_NUM");
            elem.currentTurn = readDefaultInt(data, index, "CUR_TURN");
            elem.type = readDefaultInt(data, index, "TYPE");
            elem.eventId = readDefaultString(data, index, "ID_EVENT");
            elem.effectNumber = readDefaultInt(data, index, "EFF_NUM");
            sequences.append(elem);
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultInt(header.version + objId, sequences.count());
        for (int i = 0; i < sequences.count(); ++i)
        {
            MidQuestLogSequence elem = sequences[i];
            buffer.writeDefaultString("ID_PLAYER", header.version, elem.playerId);
            buffer.writeDefaultInt("SEQ_NUM", elem.seqNumber);
            buffer.writeDefaultInt("CUR_TURN", elem.currentTurn);
            buffer.writeDefaultInt("TYPE", elem.type);
            buffer.writeDefaultString("ID_EVENT", header.version, elem.eventId);
            buffer.writeDefaultInt("EFF_NUM", elem.effectNumber);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2QuestLog)
