﻿#pragma once
#include "../DataBlock.h"

struct StackData
{
    StackData()
    {
        clear();
    }
    int unit[6];
    int pos [6];
    void clear()
    {
        for(int i = 0; i < 6; ++i)
        {
            unit[i] = -1;
            pos[i] = -1;
        }
    }
};

struct StackTemplateData
{
    StackTemplateData()
    {
        clear();
    }
    QString unit[6];
    int pos [6];
    void clear()
    {
        for(int i = 0; i < 6; ++i)
        {
            unit[i] = "000000";
            pos[i] = -1;
        }
    }
};

class D2Stack : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidStack";
        return res;
    }
    static int code()
    {
        return 0x10;//16
    }
    enum Order
    {
        Normal = 1,
        Stand = 2,
        Guard = 3,
        Attack = 4,
        Roam = 7,
        Move = 8,
        Defend = 9,
        Berserk = 10
    };
    static QString shortType()
    {
        return "KC";
    }
    static int intType()
    {
        return IDataBlock::Stack;
    }

    D2Stack()
    {
        uid.first = intType();
    }

    QList<int> items;
    StackData stack;

    QString sourceTmplateId = "000000";
    int leaderId;
    bool leaderAliv = true;
    int posX = 0;
    int posY = 0;
    int morale = 0;
    int move = 20;
    int facing = 0;
    QString banner = "000000";
    QString tome = "000000";
    QString battle1 = "000000";
    QString battle2 = "000000";
    QString artifact1 = "000000";
    QString artifact2 = "000000";
    QString boots = "000000";
    QPair<int, int> owner;//"PL0000";
    QPair<int, int> inside;//FT00000
    QPair<int, int> subRace;// = "SR0000";
    bool invisible = false;
    bool aiIgnore = false;
    int upgcount = 0;
    int order = (int)Order::Normal;
    QPair<int, int> orderTarget{-1,-1};// = "000000";
    int aiorder = (int)Order::Stand;
    QPair<int, int> aiorderTarget{-1,-1};// = "000000";
    int aipriority = 3;
    int creatLvl = 1;
    int nbbattle = 0;


    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        for (int i = 0; i < 6; ++i)
        {
            QString unitid = readDefaultString(data, index, "UNIT_" + QString::number(i));
            stack.unit[i] = getIntId(unitid);
        }

        for (int i = 0; i < 6; ++i)
        {
            stack.pos[i] = readDefaultInt(data, index, "POS_" + QString::number(i));
        }

        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            QString itemId = readDefaultString(data, index, "ITEM_ID");
            items.append(getIntId(itemId));
        }

        sourceTmplateId = readDefaultString(data, index, "SRCTMPL_ID");
        QString leader = readDefaultString(data, index, "LEADER_ID");
        leaderId = getIntId(leader);
        leaderAliv = readBool(data, index, "LEADR_ALIV");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        morale = readDefaultInt(data, index, "MORALE");
        move = readDefaultInt(data, index, "MOVE");
        facing = readDefaultInt(data, index, "FACING");
        banner = readDefaultString(data, index, "BANNER");
        tome = readDefaultString(data, index, "TOME");
        battle1 = readDefaultString(data, index, "BATTLE1");
        battle2 = readDefaultString(data, index, "BATTLE2");
        artifact1 = readDefaultString(data, index, "ARTIFACT1");
        artifact2 = readDefaultString(data, index, "ARTIFACT2");
        boots = readDefaultString(data, index, "BOOTS");
        owner = readUid(data, index, "OWNER");
        inside = readUid(data, index, "INSIDE");
        subRace = readUid(data, index, "SUBRACE");
        invisible = readBool(data, index, "INVISIBLE");
        aiIgnore = readBool(data, index, "AI_IGNORE");
        upgcount = readDefaultInt(data, index, "UPGCOUNT");
        order = readDefaultInt(data, index, "ORDER");
        orderTarget = readUid(data, index, "ORDER_TARG");
        aiorder = readDefaultInt(data, index, "AIORDER");
        aiorderTarget = readUid(data, index, "AIORDERTAR");
        aipriority = readDefaultInt(data, index, "AIPRIORITY");
        creatLvl = readDefaultInt(data, index, "CREAT_LVL");
        nbbattle = readDefaultInt(data, index, "NBBATTLE");


        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("GROUP_ID", header.version, objId);

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultString("UNIT_" + QString::number(i), header.version,
                                    IDataBlock::Unit, stack.unit[i]);
        }

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultInt("POS_" + QString::number(i), stack.pos[i]);
        }

        int itemsCount = items.count();
        buffer.writeDefaultInt(header.version + objId, itemsCount);
        for (int i = 0; i < itemsCount; ++i)
        {
            buffer.writeDefaultString("ITEM_ID", header.version,
                                      IDataBlock::Item, items[i]);
        }

        buffer.writeDefaultString("STACK_ID", header.version, objId);
        buffer.writeDefaultString("SRCTMPL_ID", header.version, sourceTmplateId);
        buffer.writeDefaultString("LEADER_ID", header.version,
                                  IDataBlock::Unit, leaderId);
        buffer.writeBool("LEADR_ALIV", leaderAliv);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeDefaultInt("MORALE", morale);
        buffer.writeDefaultInt("MOVE", move);
        buffer.writeDefaultInt("FACING", facing);
        buffer.writeDefaultString("BANNER", header.version, banner);
        buffer.writeDefaultString("TOME", header.version, tome);
        buffer.writeDefaultString("BATTLE1", header.version, battle1);
        buffer.writeDefaultString("BATTLE2", header.version, battle2);
        buffer.writeDefaultString("ARTIFACT1", header.version, artifact1);
        buffer.writeDefaultString("ARTIFACT2", header.version, artifact2);
        buffer.writeDefaultString("BOOTS", header.version, boots);
        buffer.writeDefaultString("OWNER", header.version, owner);
        buffer.writeDefaultString("INSIDE", header.version, inside);
        buffer.writeDefaultString("SUBRACE", header.version, subRace);
        buffer.writeBool("INVISIBLE", invisible);
        buffer.writeBool("AI_IGNORE", aiIgnore);
        buffer.writeDefaultInt("UPGCOUNT", upgcount);
        buffer.writeDefaultInt("ORDER", order);
        buffer.writeDefaultString("ORDER_TARG", header.version, orderTarget);
        buffer.writeDefaultInt("AIORDER", aiorder);
        buffer.writeDefaultString("AIORDERTAR", header.version, aiorderTarget);
        buffer.writeDefaultInt("AIPRIORITY", aipriority);
        buffer.writeDefaultInt("CREAT_LVL", creatLvl);
        buffer.writeDefaultInt("NBBATTLE", nbbattle);

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Stack)
