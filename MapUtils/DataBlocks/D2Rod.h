#pragma once
#include "../DataBlock.h"

class D2Rod : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidRod";
        return res;
    }
    static int code()
    {
        return 0x0e;//14
    }
    static QString shortType()
    {
        return "RD";
    }
    static int intType()
    {
        return IDataBlock::Rod;
    }

    D2Rod()
    {
        uid.first = intType();
    }

    int posX = 0;
    int posY = 0;
    QPair<int, int> owner;//"PL0000";

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        owner = readUid(data, index, "OWNER");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("ROD_ID", header.version, objId);
        buffer.writeDefaultString("OWNER", header.version, owner);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Rod)
