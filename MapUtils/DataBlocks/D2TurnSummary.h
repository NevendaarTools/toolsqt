#pragma once
#include "../DataBlock.h"

class D2TurnSummary : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "TurnSummary";
        return res;
    }
    static int code()
    {
        return 0x13;
    }
    static QString shortType()
    {
        return "TS";
    }
    static int intType()
    {
        return IDataBlock::TurnSummary;
    }

    D2TurnSummary()
    {
        uid.first = intType();
    }

    struct TurnSummaryEntry
    {
        struct TurnSummaryEntryType0
        {
            QString player2Id;
            QString _IdSpell;
            QString _IdStkD;
            QString _StrStkD;
        };
        struct TurnSummaryEntryType1
        {
            QString player2Id;
            QString _IdStkA;
            QString _StrStkA;
            QString _IdStkD;
            QString _StrStkD;
        };
        struct TurnSummaryEntryType2
        {
            QString _IdStkD;
            QString _StrStkD;
        };
        struct TurnSummaryEntryType3
        {
            QString _IdCity;
        };
        struct TurnSummaryEntryType4
        {


        };
        struct TurnSummaryEntryType5
        {
            QString _IdStkD;
            QString _StrStkD;
        };
        struct TurnSummaryEntryType6
        {
            QString _IdStkD;
            QString _StrStkD;
            bool _Asquire;
        };

        QString playerId;
        int entryType;
        int posX;
        int posY;

        TurnSummaryEntryType0 entry0;
        TurnSummaryEntryType1 entry1;
        TurnSummaryEntryType2 entry2;
        TurnSummaryEntryType3 entry3;
        TurnSummaryEntryType4 entry4;
        TurnSummaryEntryType5 entry5;
        TurnSummaryEntryType6 entry6;
    };

    QList<TurnSummaryEntry> summary;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
        {
            TurnSummaryEntry elem;
            elem.playerId = readDefaultString(data, index, "ID_PLAYER");
            elem.entryType = readDefaultInt(data, index, "TYPE");
            elem.posX = readDefaultInt(data, index, "POS_X");
            elem.posY = readDefaultInt(data, index, "POS_Y");
            if (elem.entryType == 0)
            {
                elem.entry0.player2Id = readDefaultString(data, index, "ID_PLAYER2");
                elem.entry0._IdSpell= readStringWithLenght(data, index, "ID_SPELL");
                elem.entry0._IdStkD = readDefaultString(data, index, "ID_STK_D");
                elem.entry0._StrStkD = readStringWithLenght(data, index, "STR_STK_D");
            }
            else if (elem.entryType == 1)
            {
                elem.entry1.player2Id = readDefaultString(data, index, "ID_PLAYER2");
                elem.entry1._IdStkA = readDefaultString(data, index, "ID_STK_A");
                elem.entry1._StrStkA = readStringWithLenght(data, index, "STR_STK_A");
                elem.entry1._IdStkD = readDefaultString(data, index, "ID_STK_D");
                elem.entry1._StrStkD = readStringWithLenght(data, index, "STR_STK_D");
            }
            else if (elem.entryType == 2)
            {
                elem.entry2._IdStkD = readDefaultString(data, index, "ID_STK_D");
                elem.entry2._StrStkD = readStringWithLenght(data, index, "STR_STK_D");
            }
            else if (elem.entryType == 3)
            {
                elem.entry3._IdCity = readDefaultString(data, index, "ID_CITY");
            }
            else if (elem.entryType == 4)
            {

            }
            else if (elem.entryType == 5)
            {
                elem.entry5._IdStkD = readDefaultString(data, index, "ID_STK_D");
                elem.entry5._StrStkD = readStringWithLenght(data, index, "STR_STK_D");
            }
            else if (elem.entryType == 6)
            {
                elem.entry6._IdStkD = readDefaultString(data, index, "ID_STK_D");
                elem.entry6._StrStkD = readStringWithLenght(data, index, "STR_STK_D");
                elem.entry6._Asquire = readBool(data, index, "ACQUIRE");
            }
            summary << elem;
        }

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, summary.count());
        for (int i = 0; i < summary.count(); ++i)
        {
            TurnSummaryEntry elem = summary[i];
            buffer.writeDefaultString( "ID_PLAYER", header.version, elem.playerId);
            buffer.writeDefaultInt( "TYPE", elem.entryType);
            buffer.writeDefaultInt("POS_X", elem.posX);
            buffer.writeDefaultInt("POS_Y", elem.posY);
            if (elem.entryType == 0)
            {
                buffer.writeDefaultString("ID_PLAYER2", header.version, elem.entry0.player2Id);
                buffer.writeDefaultString("ID_SPELL", header.version, elem.entry0._IdSpell);
                buffer.writeDefaultString("ID_STK_D", header.version, elem.entry0._IdStkD);
                buffer.writeString("STR_STK_D", elem.entry0._StrStkD);
            }
            else if (elem.entryType == 1)
            {
                buffer.writeDefaultString("ID_PLAYER2", header.version, elem.entry1.player2Id);
                buffer.writeDefaultString("ID_STK_A", header.version, elem.entry1._IdStkA);
                buffer.writeString("STR_STK_A", elem.entry1._StrStkA);
                buffer.writeDefaultString("ID_STK_D", header.version, elem.entry1._IdStkD);
                buffer.writeString("STR_STK_D", elem.entry1._StrStkD);
            }
            else if (elem.entryType == 2)
            {
                buffer.writeDefaultString("ID_STK_D", header.version, elem.entry2._IdStkD);
                buffer.writeString("STR_STK_D", elem.entry2._StrStkD);
            }
            else if (elem.entryType == 4)
            {

            }
            else if (elem.entryType == 5)
            {
                buffer.writeDefaultString("ID_STK_D", header.version, elem.entry5._IdStkD);
                buffer.writeString("STR_STK_D", elem.entry5._StrStkD);
            }
            else if (elem.entryType == 6)
            {
                buffer.writeDefaultString("ID_STK_D", header.version, elem.entry6._IdStkD);
                buffer.writeString("STR_STK_D", elem.entry6._StrStkD);
                buffer.writeBool("ACQUIRE", elem.entry6._Asquire);
            }
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2TurnSummary)
