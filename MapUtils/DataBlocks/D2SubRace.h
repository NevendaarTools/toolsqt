#pragma once
#include "../DataBlock.h"

class D2SubRace : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidSubRace";
        return res;
    }
    static int code()
    {
        return 0x12;
    }
    static QString shortType()
    {
        return "SR";
    }
    static int intType()
    {
        return IDataBlock::SubRace;
    }

    D2SubRace()
    {
        uid.first = intType();
    }

    int subrace;
    int playerId;
    int number = 0;
    QString name = "";
    int banner;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "SUBRACE_ID");
        uid.second = getIntId(objId);
        subrace = readDefaultInt(data, index, "SUBRACE");
        auto playerIdStr = readDefaultString(data, index, "PLAYER_ID");
        playerId = getIntId(playerIdStr);
        number = readDefaultInt(data, index, "NUMBER");
        name = readStringWithLenght(data, index, "NAME_TXT");
        banner = readDefaultInt(data, index, "BANNER");

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("SUBRACE_ID", header.version,  objId);
        buffer.writeDefaultInt("SUBRACE",  subrace);
        buffer.writeDefaultString("PLAYER_ID", header.version,  IDataBlock::Player, playerId);
        buffer.writeDefaultInt("NUMBER",  number);
        buffer.writeString("NAME_TXT", name);
        buffer.writeDefaultInt( "BANNER",  banner);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2SubRace)
