#pragma once
#include "../DataBlock.h"

class D2Diplomacy : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidDiplomacy";
        return res;
    }
    static int code()
    {
        return 0x14;
    }
    static QString shortType()
    {
        return "DP";
    }
    static int intType()
    {
        return IDataBlock::Diplomacy;
    }

    D2Diplomacy()
    {
        uid.first = intType();
    }

    struct DiplomacyEntry
    {
        int race1;
        int race2;
        int relation;

    };

    QList<DiplomacyEntry> entries;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            DiplomacyEntry elem;
            elem.race1 = readDefaultInt(data, index, "RACE_1");
            elem.race2 = readDefaultInt(data, index, "RACE_2");
            elem.relation = readDefaultInt(data, index, "RELATION");
            entries.append(elem);
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultInt(header.version + objId, entries.count());
        for (int i = 0; i < entries.count(); ++i)
        {
            DiplomacyEntry elem = entries[i];
            buffer.writeDefaultInt("RACE_1", elem.race1);
            buffer.writeDefaultInt("RACE_2", elem.race2);
            buffer.writeDefaultInt("RELATION", elem.relation);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Diplomacy)
