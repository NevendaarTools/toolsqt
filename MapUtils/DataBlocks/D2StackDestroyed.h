#pragma once
#include "../DataBlock.h"

class D2StackDestroyed : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidStackDestroyed";
        return res;
    }
    static int code()
    {
        return 0x19;
    }
    static QString shortType()
    {
        return "SD";
    }
    static int intType()
    {
        return IDataBlock::StackDestroed;
    }

    D2StackDestroyed()
    {
        uid.first = intType();
        uid.second = 0;
    }

    struct MidStackDestroyedEntry
    {
        QString stackId;
        QString killerId;
        QString templateId;

    };
    QList<MidStackDestroyedEntry> items;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
        {
            MidStackDestroyedEntry elem;
            elem.stackId = readDefaultString(data, index, "ID_STACK");
            elem.killerId = readDefaultString(data, index, "ID_KILLER");
            elem.templateId = readDefaultString(data, index, "SRCTMPL_ID");
            items.append(elem);

        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, items.count());
        for (int i = 0; i < items.count(); ++i)
        {
            MidStackDestroyedEntry elem = items[i];
            buffer.writeDefaultString("ID_STACK", header.version, elem.stackId);
            buffer.writeDefaultString("ID_KILLER", header.version, elem.killerId);
            buffer.writeDefaultString("SRCTMPL_ID", header.version, elem.templateId);
        }

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2StackDestroyed)
