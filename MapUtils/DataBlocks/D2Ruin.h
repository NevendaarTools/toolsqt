#pragma once
#include "../DataBlock.h"
#include "D2Stack.h"

class D2Ruin : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidRuin";
        return res;
    }
    static int code()
    {
        return 0x0f;
    }
    static QString shortType()
    {
        return "RU";
    }
    static int intType()
    {
        return IDataBlock::Ruin;
    }

    D2Ruin()
    {
        uid.first = intType();
    }

    QString name = "";
    QString desc = "";
    int image = 0;
    int posX = 0;
    int posY = 0;
    QString cash = "G0100:R0000:Y0000:E0000:W0000:B0000";
    QString item = "000000";
    QString looter = "000000";
    int AIPriority = 3;
    StackData stack;
    QList<QString> visiters;


    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        name = readStringWithLenght(data, index, "TITLE");
        desc = readStringWithLenght(data, index, "DESC");
        image = readDefaultInt(data, index, "IMAGE");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        cash = readStringWithLenght(data, index, "CASH");
        item = readStringWithLenght(data, index, "ITEM");
        looter = readDefaultString(data, index, "LOOTER");
        AIPriority = readDefaultInt(data, index, "AIPRIORITY");
        int count = readDefaultInt(data, index, objId);

        for (int i = 0; i < count; ++i)
            visiters.append(readDefaultString(data, index, "VISITER"));

        for (int i = 0; i < 6; ++i)
        {
            QString unitid = readDefaultString(data, index, "UNIT_" + QString::number(i));
            stack.unit[i] = getIntId(unitid);
        }

        for (int i = 0; i < 6; ++i)
        {
            stack.pos[i] = readDefaultInt(data, index, "POS_" + QString::number(i));
        }

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("RUIN_ID", header.version, objId);
        buffer.writeString("TITLE", name);
        buffer.writeString("DESC", desc);
        buffer.writeDefaultInt("IMAGE", image);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeString("CASH", cash);
        buffer.writeString("ITEM", item);
        buffer.writeDefaultString("LOOTER", header.version, looter);
        buffer.writeDefaultInt("AIPRIORITY", AIPriority);
        buffer.writeDefaultInt(header.version + objId, visiters.count());

        for (int i = 0; i < visiters.count(); ++i)
        {
            buffer.writeDefaultString("RUIN_ID", header.version, objId);
            buffer.writeDefaultString("VISITER", header.version, visiters[i]);
        }
        buffer.writeDefaultString("GROUP_ID", header.version, objId);


        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultString("UNIT_" + QString::number(i), header.version,
                                      IDataBlock::Unit, stack.unit[i]);
        }

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultInt("POS_" + QString::number(i), stack.pos[i]);
        }

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Ruin)
