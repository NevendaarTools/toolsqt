﻿#pragma once
#include "../DataBlock.h"

class D2Unit : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidUnit";
        return res;
    }
    static int code()
    {
        return 0x0f;
    }
    static QString shortType()
    {
        return "UN";
    }
    static int intType()
    {
        return IDataBlock::Unit;
    }

    D2Unit()
    {
        uid.first = intType();
    }

    class TransInfo
    {
    public:
        bool set = false;
        QString originalUnit;
        bool keepHP;
        int exp;
        int hpBefore;
        int hpBeforeMax;
    };

    QString baseUnit = "G000UU5130";
    int level = 1;
    QList<QString> mods;
    int creation = 0;
    QString name = "";
    TransInfo transf;

    bool dynLevel = false;
    int hp = 65;
    int exp = 0;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        mods.clear();
        baseUnit = readStringWithLenght(data, index, "TYPE");
        level = readDefaultInt(data, index, "LEVEL");

        int count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
            mods.append(readStringWithLenght(data, index, "MODIF_ID"));

        creation = readDefaultInt(data, index, "CREATION");
        name = readStringWithLenght(data, index, "NAME_TXT");
        bool transfSet = readBool(data, index, "TRANSF");
        if (transfSet)
        {
            transf.set = true;
            transf.originalUnit = readStringWithLenght(data, index, "ORIGTYPEID");
            transf.keepHP = readBool(data, index, "KEEP_HP");
            transf.exp = readDefaultInt(data, index, "ORIG_XP");
            transf.hpBefore = readDefaultInt(data, index, "HP_BEFORE");
            transf.hpBeforeMax = readDefaultInt(data, index, "HP_BEF_MAX");
        }

        if (header.mapType == "D2EESFISIG" || header.offset != 34)
            dynLevel = readBool(data, index, "DYNLEVEL");

        hp = readDefaultInt(data, index, "HP");
        exp = readDefaultInt(data, index, "XP");

       index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("UNIT_ID", header.version, objId);
        buffer.writeString("TYPE", baseUnit);
        buffer.writeDefaultInt("LEVEL", level);
        buffer.writeDefaultInt(header.version + objId, mods.count());

        for (int i = 0; i < mods.count(); ++i)
        {
            buffer.writeString("MODIF_ID", mods[i]);
        }
        buffer.writeDefaultInt("CREATION", creation);
        buffer.writeString("NAME_TXT", name);
        if (!transf.set)
        {
            buffer.writeBool("TRANSF", false);
        }
        else
        {
            buffer.writeBool("TRANSF", true);
            buffer.writeString("ORIGTYPEID", transf.originalUnit);
            buffer.writeBool("KEEP_HP", transf.keepHP);
            buffer.writeDefaultInt("ORIG_XP", transf.exp);
            buffer.writeDefaultInt("HP_BEFORE", transf.hpBefore);
            buffer.writeDefaultInt("HP_BEF_MAX", transf.hpBeforeMax);
        }
        if (header.mapType == "D2EESFISIG" || header.offset != 34)
            buffer.writeBool("DYNLEVEL", dynLevel);

        buffer.writeDefaultInt("HP", hp);
        buffer.writeDefaultInt("XP", exp);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Unit)
