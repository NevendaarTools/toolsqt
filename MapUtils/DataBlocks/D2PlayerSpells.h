#pragma once
#include "../DataBlock.h"

class D2PlayerSpells : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "PlayerKnownSpells";
        return res;
    }
    static int code()
    {
        return 0x19;
    }
    static QString shortType()
    {
        return "KS";
    }
    static int intType()
    {
        return IDataBlock::PlayerSpells;
    }

    D2PlayerSpells()
    {
        uid.first = intType();
    }

    QList<QString> spells;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            spells.append(readStringWithLenght(data, index, "SPELL_ID"));
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, spells.count());
        for (int i = 0; i < spells.count(); ++i)
        {
            buffer.writeString("SPELL_ID", spells[i]);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2PlayerSpells)
