#pragma once
#include "../DataBlock.h"

class D2StackTemplate : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidStackTemplate";
        return res;
    }
    static int code()
    {
        return 0x18;
    }
    static QString shortType()
    {
        return "TM";
    }
    static int intType()
    {
        return IDataBlock::StackTemplate;
    }

    D2StackTemplate()
    {
        uid.first = intType();
    }

    struct StackTemplateData
    {
        StackTemplateData()
        {
            clear();
        }
        QString unit[6];
        int pos [6];
        int level [6];
        void clear()
        {
            for(int i = 0; i < 6; ++i)
            {
                unit[i] = "000000";
                pos[i] = -1;
                level[i] = 0;
            }
        }
    };

    struct UnitTemplateMod
    {
        int index = 0;
        QString modifier = "G000UM9001";
    };

    QPair<int, int> owner;//"PL0000";
    QString _Leader = "G000UU7505";
    int _LeaderLvl = 1;
    QString _Name = "Name";
    QPair<int, int> orderTarget{-1, -1};// = "G000000000";
    QPair<int, int> subRace;// = "SR0000";
    int _Order = 2;
    StackTemplateData stack;
    bool useFacing = false;
    int _Facing = 0;
    QList<UnitTemplateMod> mods;
    int _AIPriority = 3;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        owner = readUid(data, index, "OWNER");
        _Leader = readStringWithLenght(data, index, "LEADER");
        _LeaderLvl = readDefaultInt(data, index, "LEADER_LVL");
        _Name = readStringWithLenght(data, index, "NAME_TXT");
        orderTarget = readUid(data, index, "ORDER_TARG");
        subRace = readUid(data, index, "SUBRACE");
        _Order = readDefaultInt(data, index, "ORDER");

        for (int i = 0; i < 6; ++i)
        {
            stack.unit[i] = readStringWithLenght(data, index, "UNIT_" + QString::number(i));
            stack.level[i] = readDefaultInt(data, index, "UNIT_" + QString::number(i) + "_LVL");
        }

        for (int i = 0; i < 6; ++i)
        {
            stack.pos[i] = readDefaultInt(data, index, "POS_" + QString::number(i));
        }

        useFacing = readBool(data, index, "USE_FACING");
        _Facing = readDefaultInt(data, index, "FACING");

        int modCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < modCount; ++i)
        {
            UnitTemplateMod mod;
            mod.index = readDefaultInt(data, index, "UNIT_POS"),
            mod.modifier = readStringWithLenght(data, index, "MODIF_ID");
            mods.append(mod);
        }

        if (header.mapType == "D2EESFISIG" || header.offset == 0)
           _AIPriority = readDefaultInt(data, index, "AIPRIORITY");
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("ID" , header.version, objId);
        buffer.writeDefaultString("OWNER", header.version, owner);
        buffer.writeString("LEADER", _Leader);
        buffer.writeDefaultInt("LEADER_LVL", _LeaderLvl);
        buffer.writeString("NAME_TXT", _Name);
        buffer.writeDefaultString("ORDER_TARG", header.version, orderTarget);
        buffer.writeDefaultString("SUBRACE", header.version, subRace);
        buffer.writeDefaultInt("ORDER", _Order);

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultString("UNIT_" + QString::number(i), stack.unit[i]);
            buffer.writeDefaultInt("UNIT_" + QString::number(i) + "_LVL", stack.level[i]);
        }

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultInt("POS_" + QString::number(i), stack.pos[i]);
        }

        buffer.writeBool("USE_FACING", useFacing);
        buffer.writeDefaultInt("FACING", _Facing);
        buffer.writeSimpleString(header.version + objId);
        buffer.writeDefaultInt("", mods.count());

        for (int i = 0; i < mods.count(); ++i)
        {
            buffer.writeDefaultInt("UNIT_POS", mods[i].index);
            buffer.writeString("MODIF_ID", mods[i].modifier);
        }

        if (header.mapType == "D2EESFISIG" || header.offset == 0)
            buffer.writeDefaultInt("AIPRIORITY", _AIPriority);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2StackTemplate)
