﻿#pragma once
#include "../DataBlock.h"

class D2MapBlock : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidgardMapBlock";
        return res;
    }
    static int code()
    {
        return 0x17;
    }
    static QString shortType()
    {
        return "MB";
    }
    static int intType()
    {
        return IDataBlock::MapBlock;
    }

    D2MapBlock()
    {
        uid.first = intType();
    }

    int32_t blockData [32];
    int32_t dataSize = 128;

    int x() const
    {
        return uid.second & 0xFF;
    }
    int y() const
    {
        return (uid.second >> 8) & 0xFF;
    }

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        dataSize = readDefaultInt(data, index, "BLOCKDATA");
        for(int i = 0; i < 32; ++i)
        {
            blockData[i] = readInt(data, index, 4);
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("BLOCKID", header.version, objId);
        buffer.writeInt("BLOCKDATA", dataSize, 4);
        for (int i = 0; i < 32; ++i)
        {
            buffer.writeInt(blockData[i], 4, false);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2MapBlock)
