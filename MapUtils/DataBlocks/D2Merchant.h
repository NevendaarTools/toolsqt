#pragma once
#include "../DataBlock.h"

struct SiteVisitor
{
    QString siteId;
    QString visiter;
};

class D2Merchant : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidSiteMerchant";
        return res;
    }
    static int code()
    {
        return 0x17;
    }
    static QString shortType()
    {
        return "SI";
    }
    static int intType()
    {
        return IDataBlock::Merchant_items;
    }

    D2Merchant()
    {
        uid.first = intType();
    }

    struct MerchantEntry
    {
        QString itemId;
        int count;
    };

    QString name = "";
    QString desc = "";
    int posX = 0;
    int posY = 0;
    int AIpriority = 3;
    int image = 0;
    QString imageIntf = "";
    QString currentVisiter = "000000";
    QList<MerchantEntry> items;

    bool buyArmor = true;
    bool buyJewel = true;
    bool buyWeapon = true;
    bool buyBanner = true;
    bool buyPotion = true;
    bool buyScroll = true;
    bool buyWand = true;
    bool buyValue = true;

    bool mission = false;
    QList<SiteVisitor> visiters;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        image = readDefaultInt(data, index, "IMG_ISO");
        imageIntf = readStringWithLenght(data, index, "IMG_INTF");
        name = readStringWithLenght(data, index, "TXT_TITLE");
        desc = readStringWithLenght(data, index, "TXT_DESC");
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        currentVisiter = readStringWithLenght(data, index, "VISITER");
        AIpriority = readDefaultInt(data, index, "AIPRIORITY");

        buyArmor = readBool(data, index, "BUY_ARMOR");
        buyJewel = readBool(data, index, "BUY_JEWEL");
        buyWeapon = readBool(data, index, "BUY_WEAPON");
        buyBanner = readBool(data, index, "BUY_BANNER");
        buyPotion = readBool(data, index, "BUY_POTION");
        buyScroll = readBool(data, index, "BUY_SCROLL");
        buyWand = readBool(data, index, "BUY_WAND");
        buyValue = readBool(data, index, "BUY_VALUE");


        int count = readDefaultInt(data, index, "QTY_ITEM");
        for (int i = 0; i < count; ++i)
        {
            MerchantEntry elem;
            elem.itemId = readStringWithLenght(data, index, "ITEM_ID");
            elem.count = readDefaultInt(data, index, "ITEM_COUNT");
            items.append(elem);
        }

        mission = readBool(data, index, "MISSION");
        count = readDefaultInt(data, index, objId);
        for (int i = 0; i < count; ++i)
        {
            SiteVisitor elem;
            elem.siteId = readDefaultString(data, index, "SITE_ID");
            elem.visiter = readDefaultString(data, index, "VISITER");
            visiters.append(elem);
        }

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("SITE_ID", header.version, objId);
        buffer.writeDefaultInt("IMG_ISO", image);
        buffer.writeString("IMG_INTF", imageIntf);
        buffer.writeString("TXT_TITLE", name);
        buffer.writeString("TXT_DESC", desc);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeDefaultString("VISITER", currentVisiter);
        buffer.writeDefaultInt("AIPRIORITY", AIpriority);

        buffer.writeBool("BUY_ARMOR",  buyArmor);
        buffer.writeBool("BUY_JEWEL",  buyJewel);
        buffer.writeBool("BUY_WEAPON",  buyWeapon);
        buffer.writeBool("BUY_BANNER",  buyBanner);
        buffer.writeBool("BUY_POTION",  buyPotion);
        buffer.writeBool("BUY_SCROLL",  buyScroll);
        buffer.writeBool("BUY_WAND",  buyWand);
        buffer.writeBool("BUY_VALUE",  buyValue);
        buffer.writeDefaultInt( "QTY_ITEM", items.count());
        for (int i = 0; i < items.count(); ++i)
        {
            MerchantEntry elem = items[i];
            buffer.writeString("ITEM_ID", elem.itemId);
            buffer.writeDefaultInt( "ITEM_COUNT", elem.count);

        }
        buffer.writeBool("MISSION",  mission);
        buffer.writeDefaultInt(header.version + objId, visiters.count());
        for (int i = 0; i < visiters.count(); ++i)
        {
            SiteVisitor elem = visiters[i];
            buffer.writeDefaultString("SITE_ID", elem.siteId);
            buffer.writeDefaultString("VISITER", header.version, elem.visiter);

        }

        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Merchant)
