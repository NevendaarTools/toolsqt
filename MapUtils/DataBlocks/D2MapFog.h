#pragma once
#include "../DataBlock.h"

class D2MapFog : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidgardMapFog";
        return res;
    }
    static int code()
    {
        return 0x15;
    }
    static QString shortType()
    {
        return "FG";
    }
    static int intType()
    {
        return IDataBlock::Fog;
    }

    D2MapFog()
    {
        uid.first = intType();
    }

    struct FogEntry
    {
        int posY;
        QByteArray fog;
    };

    QList<FogEntry> entries;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            FogEntry elem;
            elem.posY = readDefaultInt(data, index, "POS_Y");
            int byteCount = readDefaultInt(data, index, "FOG");
            elem.fog = data.mid(index, byteCount);
            index += byteCount;
            entries.append(elem);
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, entries.count());
        for (int i = 0; i < entries.count(); ++i)
        {
            FogEntry elem = entries[i];
            buffer.writeDefaultInt("POS_Y", elem.posY);
            buffer.writeDefaultInt("FOG", elem.fog.size());
            buffer.write(elem.fog);
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2MapFog)
