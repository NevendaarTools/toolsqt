#pragma once
#include "../DataBlock.h"

class D2SpellEffects : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidSpellEffects";
        return res;
    }
    static int code()
    {
        return 0x17;
    }
    static QString shortType()
    {
        return "ET";
    }
    static int intType()
    {
        return IDataBlock::SpellEffects;
    }

    D2SpellEffects()
    {
        uid.first = intType();
    }

    int unknown = 0;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        unknown = readDefaultInt(data, index, objId);

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultInt(header.version + objId, unknown);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2SpellEffects)
