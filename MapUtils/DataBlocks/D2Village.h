﻿#pragma once
#include "../DataBlock.h"
#include "D2Stack.h"

class D2Village : public IDataBlock
{
public :
    static QString type()
    {
        static QString res = "MidVillage";
        return res;
    }
    static int code()
    {
        return 0x12;
    }
    static QString shortType()
    {
        return "FT";
    }
    static int intType()
    {
        return IDataBlock::Fort_village;
    }

    D2Village()
    {
        uid.first = intType();
    }

    QString name = "";
    QString desc = "";
    QPair<int, int> owner;// = "PL0000";
    QPair<int, int> subRace;// = "SR0000";
    int stackId = -1;
    int posX = 0;
    int posY = 0;
    StackData stack;
    QList<int> items;
    int AIpriority = 3;
    QString protectB = "000000";
    int regenB = 0;
    int morale = 0;
    int growthT = 0;
    int size = 1;
    bool _POUN = false;
    bool _POHE = false;
    bool _POHU = false;
    bool _PODW = false;
    bool _POEL = false;

    int _RiotT = 0;

    bool HasUnits()
    {
        for (int i = 0; i < 6; ++i)
        {
            if (stack.unit[i] != -1)
                return true;
        }
        return false;
    }

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        name  = readStringWithLenght(data, index, "NAME_TXT");
        desc  = readStringWithLenght(data, index, "DESC_TXT");
        owner = readUid(data, index, "OWNER");
        subRace = readUid(data, index, "SUBRACE");
        QString stackStr = readDefaultString(data, index, "STACK");
        stackId = getIntId(stackStr);
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");

        for (int i = 0; i < 6; ++i)
        {
            QString unitid = readDefaultString(data, index, "UNIT_" + QString::number(i));
            stack.unit[i] = getIntId(unitid);
        }

        for (int i = 0; i < 6; ++i)
        {
            stack.pos[i] = readDefaultInt(data, index, "POS_" + QString::number(i));
        }

        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            QString itemId = readDefaultString(data, index, "ITEM_ID");
            items.append(getIntId(itemId));
        }

        AIpriority = readDefaultInt(data, index, "AIPRIORITY");
        protectB = readDefaultString(data, index, "PROTECT_B");
        regenB = readDefaultInt(data, index, "REGEN_B");
        morale = readDefaultInt(data, index, "MORALE");
        growthT = readDefaultInt(data, index, "GROWTH_T");
        size = readDefaultInt(data, index, "SIZE");

        _POUN = readBool(data, index, "P_O_UN");
        _POHE = readBool(data, index, "P_O_HE");
        _POHU = readBool(data, index, "P_O_HU");
        _PODW = readBool(data, index, "P_O_DW");

        if (header.mapType == "D2EESFISIG")
            _POEL = readBool(data, index, "P_O_EL");

        _RiotT = readDefaultInt(data, index, "RIOT_T");

        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;
        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();

        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();

        buffer.writeDefaultString("CITY_ID", header.version, objId);
        buffer.writeString("NAME_TXT", name);
        buffer.writeString("DESC_TXT", desc);
        buffer.writeDefaultString("OWNER", header.version, owner);
        buffer.writeDefaultString("SUBRACE", header.version, subRace);
        buffer.writeDefaultString("STACK", header.version, IDataBlock::Stack, stackId);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeDefaultString("GROUP_ID", header.version, objId);

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultString("UNIT_" + QString::number(i), header.version, IDataBlock::Unit, stack.unit[i]);
        }

        for (int i = 0; i < 6; ++i)
        {
            buffer.writeDefaultInt("POS_" + QString::number(i), stack.pos[i]);
        }

        int itemsCount = items.count();
        buffer.writeDefaultInt(header.version + objId, itemsCount);
        for (int i = 0; i < itemsCount; ++i)
        {
            buffer.writeDefaultString("ITEM_ID", header.version, IDataBlock::Item, items[i]);
        }

        buffer.writeDefaultInt("AIPRIORITY", AIpriority);
        buffer.writeDefaultString("PROTECT_B", header.version, protectB);

        buffer.writeDefaultInt("REGEN_B", regenB);
        buffer.writeDefaultInt("MORALE", morale);
        buffer.writeDefaultInt("GROWTH_T", growthT);
        buffer.writeDefaultInt("SIZE", size);

        buffer.writeBool("P_O_UN", _POUN);
        buffer.writeBool("P_O_HE", _POHE);
        buffer.writeBool("P_O_HU", _POHU);
        buffer.writeBool("P_O_DW", _PODW);
        if (header.mapType == "D2EESFISIG")
            buffer.writeBool("P_O_EL", _POEL);


        buffer.writeDefaultInt("RIOT_T", _RiotT);
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};

REGISTER_TYPE(D2Village)
