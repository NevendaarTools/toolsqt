#pragma once
#include "../DataBlock.h"

class D2Bag : public IDataBlock
{
public:
    static QString type()
    {
        static QString res = "MidBag";
        return res;
    }
    static int code()
    {
        return 0x0e;//14
    }
    static QString shortType()
    {
        return "BG";
    }
    static int intType()
    {
        return IDataBlock::Treasure;
    }

    D2Bag()
    {
        uid.first = intType();
    }

    int posX = 0;
    int posY = 0;
    int AIpriority = 3;
    int image;
    QList<int> items;

    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) override
    {
        Q_UNUSED(header);
        int endIndex = getEndIndex(data, index);

        QString objId = readDefaultString(data, index, "OBJ_ID");
        uid.second = getIntId(objId);
        posX = readDefaultInt(data, index, "POS_X");
        posY = readDefaultInt(data, index, "POS_Y");
        image = readDefaultInt(data, index, "IMAGE");
        AIpriority = readDefaultInt(data, index, "AIPRIORITY");
        int itemsCount = readDefaultInt(data, index, objId);
        for (int i = 0; i < itemsCount; ++i)
        {
            QString itemId = readDefaultString(data, index, "ITEM_ID");
            items.append(getIntId(itemId));
        }
        index = endIndex;
    }

    virtual QByteArray data(const MapHeaderBlock& header) const override
    {
        ByteBuffer buffer;

        QString objId = shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
        buffer.writeBlockHeader(type(), code());
        buffer.writeDefaultString("OBJ_ID", header.version, objId);
        buffer.writeBegin();
        buffer.writeDefaultString("BAG_ID", header.version, objId);
        buffer.writeDefaultInt("POS_X", posX);
        buffer.writeDefaultInt("POS_Y", posY);
        buffer.writeDefaultInt("IMAGE", image);
        buffer.writeDefaultInt("AIPRIORITY", AIpriority);
        buffer.writeDefaultInt(header.version + objId, items.count());
        for (int i = 0; i < items.count(); ++i)
        {
            buffer.writeDefaultString("ITEM_ID", header.version,
                                      TypeHolder::ShortByType(IDataBlock::Item) +
                                      QString::number(items[i], 16).rightJustified(4, '0').toLower());
        }
        buffer.writeEnd();

        return buffer.GetData();
    }

    virtual QString blockType() const override {return type();}
};
REGISTER_TYPE(D2Bag)
