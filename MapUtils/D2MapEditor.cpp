#include "D2MapEditor.h"
#include "DataBlocks/D2MapFog.h"
#include "DataBlocks/D2PlayerBuildings.h"
#include "DataBlocks/D2PlayerSpells.h"

D2MapEditor::D2MapEditor(QSharedPointer<DBFModel> data) : m_data(data)
{

}

bool D2MapEditor::open(const QString &path)
{
    if (!m_map.open(path))
        return false;
    setMap(m_map);
    return true;
}

bool D2MapEditor::save(const QString &path)
{
    return m_map.save(path);
}

void D2MapEditor::createMap(int size)
{
    m_map = D2MapModel();
    m_map.header.mapSize = size;
    m_map.header.emptyInit();
    int mapSize = m_map.header.mapSize;
    m_grid.init(mapSize, 5);

    D2StackDestroyed destroyed;
    m_map.replace(destroyed);

    D2ScenarioInfo info;
    info.name = "my test";
    info.desc = "";
    info.briefing = "Цели сценария не определены";
    info.debunkw = "Поздравляем! Вы победили в этом сценарии._";
    info.debunkl = "Вы потерпели поражение, враги достигли своей цели.";
    info.brieflong1 = "Цели сценария_";
    m_map.replace(info);

    D2Mountains mountains;
    m_map.replace(mountains);

    D2QuestLog log;
    m_map.replace(log);

    D2Plan plan;
    m_map.replace(plan);

    D2Map map;
    map.size = size;
    m_map.replace(map);


    D2SceneVariables variables;
    m_map.replace(variables);

    D2TurnSummary summary;
    m_map.replace(summary);


    D2TalismanCharges charges;
    m_map.replace(charges);

    D2SpellEffects spellEffects;
    m_map.replace(spellEffects);

    D2SpellCast spellCast;
    m_map.replace(spellCast);

    D2Diplomacy diplomacy;
    m_map.replace(diplomacy);

    D2Player neutral;
    neutral.name = "Нейтралы";
    neutral.desc = "Описание расы отсутствует";
    m_map.replace(neutral);

//    D2MapFog fog;
//    fog.uid.second = 0;
//    for (int y = 0; y < mapSize; ++y)
//    {
//        D2MapFog::FogEntry entry;
//        entry.posY = y;
//        entry.fog.resize(mapSize / 8);
//        entry.fog.fill('\0');
//        fog.entries << entry;
//    }
//    m_map.replace(fog);

    D2PlayerBuildings buildings;
    buildings.uid.second = 0;
    m_map.replace(buildings);

    D2PlayerSpells spells;
    spells.uid.second = 0;
    m_map.replace(spells);
}

D2Capital D2MapEditor::addRace(const QString &raceId,
                          int capitalX, int capitalY,
                          Glord::Category lordCategory,
                          const QString & name, const QString &desc,
                          const QString &fortName, const QString &heroName)
{
    //auto players = m_map.getAll<D2Player>();//TODO: check for raceId already exist - return false
    auto vars = m_data->get<GVars>("");

    D2Player player;
    player.name = name;
    player.desc = desc;
    player.bank = "G0100:R0000:Y0000:E0000:W0000:B0000";
    player.face = 0;
    auto race = m_data->get<Grace>(raceId);
    qDebug()<<"Add race = "<<race->name_txt->text;
    QVector<QSharedPointer<Glord>> lords = m_data->getList<Glord>();
    for (int i = 0; i < lords.count(); ++i)
    {
        if (lords[i]->race_id.key.toLower() == raceId.toLower() &&
            lords[i]->category.key == lordCategory)
        {
            player.lordId = lords[i]->lord_id.toUpper();
            break;
        }
    }
    player.raceId = raceId;
    player.uid.second = m_idsHelper.nextId<D2Player>(&m_map);

    int mapSize = m_map.header.mapSize;

    D2MapFog fog;
    fog.uid.second = m_idsHelper.nextId<D2MapFog>(&m_map);
    for (int y = 0; y < mapSize; ++y)
    {
        D2MapFog::FogEntry entry;
        entry.posY = y;
        entry.fog.resize(mapSize / 8);
        entry.fog.fill('\0');
        fog.entries << entry;
    }
    int minFogX = qMax(0, capitalX - 5);
    int minFogY = qMax(0, capitalY - 5);
    int maxFogX = qMin(mapSize - 1, capitalX + 10);
    int maxFogY = qMin(mapSize - 1, capitalY + 10);

    for (int x = minFogX; x < maxFogX; ++x)
    {
        for (int y = minFogY = 0; y < maxFogY; ++y)
        {
            //setBitInByteArray(fog.entries[y].fog, x, true);
        }
    }
    m_map.replace(fog);
    player.fogId = fog.uid.second;

    int terrainValue = 5;
    auto terrains = m_data->getList<Lterrain>();
    for (int i = 0; i < terrains.count(); ++i)
    {
        if (terrains[i]->text == race->race_type->text.left(4))
        {
            terrainValue = terrains[i]->id;
            break;
        }
    }

    for (int i = 0; i < 5; ++i)
    {
        for (int k = 0; k < 5; k++)
        {
            m_grid.cells[capitalX + i][capitalY + k].value = terrainValue;
        }
    }

    D2PlayerBuildings buildings;
    buildings.uid.second = m_idsHelper.nextId<D2PlayerBuildings>(&m_map);
    m_map.replace(buildings);
    player.buildingsId = buildings.uid.second;

    D2PlayerSpells spells;
    spells.uid.second = m_idsHelper.nextId<D2PlayerSpells>(&m_map);
    m_map.replace(spells);
    player.knownId = spells.uid.second;
    m_map.replace(player);

    D2Capital capital;
    capital.posX = capitalX;
    capital.posY = capitalY;
    capital.uid.second = m_idsHelper.nextId<D2Capital>(&m_map);
    capital.owner = player.uid.second;
    capital.subRace.first = IDataBlock::SubRace;
    capital.subRace.second = player.uid.second;

    for (int i = 0; i < 3; ++i)
    {
        capital.items.append(addItem("G000IG0006").uid.second);
    }
    capital.name = fortName;

    auto guardian = addUnit(race->guardian);
    auto d2hero = addUnit(race->leader_1);
    d2hero.name = heroName;
    m_map.replace(d2hero);

    D2Stack heroStack;
    heroStack.uid.second = m_idsHelper.nextId<D2Stack>(&m_map);
    capital.stackId = heroStack.uid.second;
    heroStack.inside = capital.uid;
    heroStack.leaderId = d2hero.uid.second;
    heroStack.stack.unit[0] = d2hero.uid.second;
    heroStack.stack.pos[2] = 0;
    heroStack.posX = capitalX;
    heroStack.posY = capitalY;
    heroStack.aiorder = 1;
    heroStack.move = 35;
    heroStack.owner = player.uid;
    heroStack.subRace = QPair<int, int>(IDataBlock::SubRace, player.uid.second);

    capital.stack.unit[0] = guardian.uid.second;
    capital.stack.pos[2] = 0;

    m_map.replace(d2hero);
    m_map.replace(capital);
    m_map.replace(heroStack);

    updateSubraces();
    return capital;
}

void D2MapEditor::updateSubraces()
{
    m_map.removeAll<D2SubRace>();
    auto players = m_map.getAll<D2Player>();
    D2ScenarioInfo info = m_map.get<D2ScenarioInfo>(0);

    ByteBuffer tmpBuffer;
    tmpBuffer.writeDefaultInt("", players.count());
    for (int i = 0; i < players.count(); ++i)
    {
        QSharedPointer<Grace> race = m_data->get<Grace>(players[i].raceId);
        D2SubRace playerRace;
        playerRace.uid.second = players[i].uid.second;
        playerRace.subrace = race->race_type.key + 1;
        playerRace.playerId = players[i].uid.second;
        playerRace.number = 0;
        playerRace.banner = playerRace.subrace - 1;
        m_map.replace(playerRace);

        info.players[i] = race->race_type.key;
        tmpBuffer.writeDefaultInt("", race->race_type.key);
        tmpBuffer.writeRepitableData('\0', 36);
    }
    m_map.header._playersData = tmpBuffer.GetData();
    m_map.header.offset = (players.count() + 67) * 40;
    qDebug()<<"$$$"<<m_map.header._playersData.size()<<"  players = "<<players.count();
    m_map.replace(info);

    for (int i = 0; i < 8; ++i)
    {
        D2SubRace subRace;
        subRace.subrace = 6 + i;
        subRace.uid.second = players.count() + i;
        subRace.playerId = 0;
        subRace.banner = 5 + i;
        m_map.replace(subRace);
    }

    D2Diplomacy diplomacy = m_map.get<D2Diplomacy>(0);
    diplomacy.entries.clear();
    for (int i = 0; i < players.count(); ++i)
    {
        QSharedPointer<Grace> race1 = m_data->get<Grace>(players[i].raceId);
        for (int k = i + 1; k < players.count(); ++k)
        {
            QSharedPointer<Grace> race2 = m_data->get<Grace>(players[k].raceId);

            D2Diplomacy::DiplomacyEntry elem;
            elem.race1 = race1->race_type.key;//Grace
            elem.race2 = race2->race_type.key;
            elem.relation = 0;
            diplomacy.entries.append(elem);
        }
    }
    qDebug()<<"Diplo entries count = "<<diplomacy.entries.count();
    m_map.replace(diplomacy);
}

const D2MapModel D2MapEditor::map() const
{
    return m_map;
}

void D2MapEditor::setMap(D2MapModel newMap)
{
    m_map = newMap;
    int mapSize = m_map.header.mapSize;
    m_grid.init(mapSize);
    clearIds();

    QList<D2MapBlock> blocks = m_map.getAll<D2MapBlock>();
    foreach (const D2MapBlock & block, blocks)
    {
        int blockX = block.x();
        int blockY = block.y();
        for (int i = 0; i < 32; ++i)
        {
            int x = blockX + i % 8;
            int y = blockY + (int)(i / 8);
            int32_t value = block.blockData[i];
            m_grid.cells[x][y].value = value;
        }
    }
}

void D2MapEditor::clearIds()
{
    m_idsHelper.clear();
}

void D2MapEditor::rename(const QString &name, const QString &author)
{
    D2ScenarioInfo info = m_map.get<D2ScenarioInfo>(0);
    info.name = name;
    info.creator = author;
    m_map.replace(info);
    m_map.header.name = name;
    m_map.header.author = author;
}

bool D2MapEditor::removeItem(D2Item item)
{
    auto itemInfo = m_data->get<GItem>(item.itemType);
    if (itemInfo->item_cat == (int)GItem::Talisman)
    {
        D2TalismanCharges charges = m_map.getAll<D2TalismanCharges>()[0];
        for (int i = 0; i < charges.charges.count(); ++i)
        {
            if (charges.charges[i].talismanId == item.uid.second)
            {
                charges.charges.removeAt(i);
                break;
            }
        }
        m_map.replace(charges);
    }
    m_map.remove(item.uid);

    return true;
}

D2Item D2MapEditor::addItem(const QString &type, int talismanCharges)
{
    D2Item d2item;
    d2item.uid.second = m_idsHelper.nextId<D2Item>(&m_map),
            d2item.itemType = type.toUpper();
    auto iteminfo = m_data->get<GItem>(d2item.itemType);
    if (iteminfo->item_cat == (int)GItem::Talisman)
    {
        D2TalismanCharges charges = m_map.getAll<D2TalismanCharges>()[0];
        if (talismanCharges == -1)
            talismanCharges = m_data->getList<GVars>()[0]->talis_chrg;
        D2TalismanCharges::TalismanChargeInfo info;
        info.talismanId = d2item.uid.second;
        info.count = talismanCharges;
        charges.charges.append(info);
        m_map.replace(charges);
    }
    m_map.addDataBlock(new D2Item(d2item));
    return d2item;
}

D2Unit D2MapEditor::addUnit(const QString &type, int level)
{
    auto unit = m_data->get<Gunit>(type.toLower());
    int resLevel = qMax(level, unit->level);
    D2Unit d2unit;
    d2unit.uid.second = m_idsHelper.nextId<D2Unit>(&m_map);
    d2unit.baseUnit = type.toUpper();
    d2unit.level = resLevel;
    d2unit.hp = calcLeveledValue(unit->hit_point, unit->level, resLevel,
                                 unit->dyn_upg_lv,
                                 unit->dyn_upg1->hit_point,
                                 unit->dyn_upg2->hit_point);


    m_map.addDataBlock(new D2Unit(d2unit));
    return d2unit;
}

D2Unit D2MapEditor::addUnit(const QString &type, int level, int uid)
{
    auto unit = m_data->get<Gunit>(type.toLower());
    int resLevel = qMax(level, unit->level);
    D2Unit d2unit;
    d2unit.uid.second = uid;
    d2unit.baseUnit = type.toUpper();
    d2unit.level = resLevel;
    d2unit.hp = calcLeveledValue(unit->hit_point, unit->level, resLevel,
                                 unit->dyn_upg_lv,
                                 unit->dyn_upg1->hit_point,
                                 unit->dyn_upg2->hit_point);


    m_map.addDataBlock(new D2Unit(d2unit));
    return d2unit;
}

bool D2MapEditor::setVariable(const QString &name, int value)
{
    D2SceneVariables variables = m_map.get<D2SceneVariables>(0);
    for (int i = 0; i < variables.variables.count(); ++i)
    {
        if (variables.variables[i].name == name)
        {
            variables.variables[i].value = value;
            m_map.replace(variables);
            return true;
        }
    }

    return false;
}

bool D2MapEditor::hasVariable(const QString &name)
{
    D2SceneVariables variables = m_map.get<D2SceneVariables>(0);
    for (int i = 0; i < variables.variables.count(); ++i)
    {
        if (variables.variables[i].name == name)
        {
            return true;
        }
    }

    return false;
}

bool D2MapEditor::addVariable(const QString &name, int value)
{
    D2SceneVariables variables = m_map.get<D2SceneVariables>(0);
    D2SceneVariables::SceneVariable variable;
    variable.id = variables.variables.count();
    variable.name = name;
    variable.value = value;
    variables.variables.append(variable);
    m_map.replace(variables);
    return true;
}

int D2MapEditor::getVariable(const QString &name)
{
    D2SceneVariables variables = m_map.get<D2SceneVariables>(0);
    for (int i = 0; i < variables.variables.count(); ++i)
    {
        if (variables.variables[i].name == name)
        {
            return variables.variables[i].value;
        }
    }

    return 0;
}

D2LandMark D2MapEditor::addLandMark(int x, int y, const QString &name)
{
    auto lmark = m_data->get<GLmark>(name);
    if (lmark->mountain)
        return D2LandMark();
    D2LandMark landMark;
    landMark.uid.second = m_idsHelper.nextId<D2LandMark>(&m_map);
    landMark.posX = x;
    landMark.posY = y;
    landMark.baseType = name;
    landMark.desc = lmark->desc_txt->text;
    m_map.addBlock(landMark);
    return landMark;
}

void D2MapEditor::addMine(int x, int y, int resource)
{
    D2Crystal crystal;
    crystal.uid.second = m_idsHelper.nextId<D2Crystal>(&m_map);
    crystal.posX = x;
    crystal.posY = y;
    crystal.resource = resource;
    m_map.addBlock(crystal);
}

void D2MapEditor::addTrainer(int x, int y, int type, const QString &name, const QString &desc)
{
    D2Trainer site;
    site.uid.second = m_idsHelper.nextId<D2Trainer>(&m_map);
    site.posX = x;
    site.posY = y;
    site.image = type;
    site.name = name;
    site.desc = desc;
    m_map.addBlock(site);
}

void D2MapEditor::addMerchant(int x, int y, int type, const QList<D2Merchant::MerchantEntry> &items, const QString &name, const QString &desc)
{
    D2Merchant merchant;
    merchant.uid.second = m_idsHelper.nextId<D2Merchant>(&m_map);
    merchant.posX = x;
    merchant.posY = y;
    merchant.image = type;
    merchant.name = name;
    merchant.desc = desc;
    merchant.items = items;
    m_map.addBlock(merchant);
}

D2Bag D2MapEditor::addBag(int x, int y, int type, int AIpriority, const QList<int> &items)
{
    D2Bag bag;
    bag.uid.second = m_idsHelper.nextId<D2Bag>(&m_map);
    bag.posX = x;
    bag.posY = y;
    bag.image = type;
    bag.items = items;
    bag.AIpriority = AIpriority;
    m_map.addBlock(bag);
    return bag;
}

void D2MapEditor::addMage(int x, int y, int type, const QList<QString> &items, const QString &name, const QString &desc)
{
    D2Mage merchant;
    merchant.uid.second = m_idsHelper.nextId<D2Mage>(&m_map);
    merchant.posX = x;
    merchant.posY = y;
    merchant.spells = items;
    merchant.image = type;
    merchant.name = name;
    merchant.desc = desc;
    m_map.addBlock(merchant);
}

void D2MapEditor::addMercs(int x, int y, int type, const QList<D2Mercs::MercsUnitEntry> units, const QString &name, const QString &desc)
{
    D2Mercs mercs;
    mercs.uid.second = m_idsHelper.nextId<D2Mage>(&m_map);
    mercs.posX = x;
    mercs.posY = y;
    mercs.units = units;
    mercs.image = type;
    mercs.name = name;
    mercs.desc = desc;
    m_map.addBlock(mercs);
}

void D2MapEditor::addMountain(D2Mountains::Mountain mountain)
{
    auto d2Mountains = m_map.get<D2Mountains>(0);
    mountain.id = d2Mountains.mountains.count();
    d2Mountains.mountains.append(mountain);
    for (int i = 0; i < mountain.sizeX; i++)
    {
        for (int k = 0; k < mountain.sizeY; k++)
            m_grid.cells[i + mountain.posX][k + mountain.posY].value = 37;
    }
    m_map.replace(d2Mountains);
}

void D2MapEditor::addMountain(int x, int y, int w, int h, int image)
{
    D2Mountains::Mountain mountain;
    mountain.posX = x;
    mountain.posY = y;
    mountain.sizeX = w;
    mountain.sizeY = h;
    mountain.image = image;
    addMountain(mountain);
}

void D2MapEditor::commitGrid()
{
    int mapSize = m_map.header.mapSize;
    for (int i = 0; i < mapSize; i += 4)
    {
        for (int k = 0; k < mapSize; k += 8)
        {
            QString id = QString::number(i, 16).rightJustified(2, '0') +
                    QString::number(k, 16).rightJustified(2, '0');
            D2MapBlock mapBlock = m_map.get<D2MapBlock>(IDataBlock::getIntId(id));
            mapBlock.uid.second = IDataBlock::getIntId(id);
            for (int q = 0; q < 32; ++q)
            {
                int x = k + q % 8;
                int y = i + (int)(q / 8);
                mapBlock.blockData[q] = m_grid.cells[x][y].value;
            }
            m_map.replace(mapBlock);
        }
    }
    for (int i = 0; i < mapSize; i ++)
    {
        for (int k = 0; k < mapSize; k ++)
        {
            int x = i;
            int y = k;
            if (m_grid.cells[x][y].roadType != -1)
            {
                D2Road road;
                road.roadIndex = m_grid.cells[x][y].roadType;
                road.var = m_grid.cells[x][y].roadVar;
                road.posX = x;
                road.posY = y;
                road.uid.second = m_idsHelper.nextId<D2Road>(&m_map);
                m_map.replace(road);
            }
        }
    }

    D2Plan plan = m_map.get<D2Plan>(0);
    QStringList oldElements;
    foreach (auto & elem, plan.elements) {
        oldElements << QString("%1_%2_%3").arg(elem.posX).arg(elem.posY).arg(elem.id.second);
    }
    std::sort(oldElements.begin(), oldElements.end());
    plan.elements.clear();
    plan.size = m_grid.cells.count();
    {
        QList<D2Capital> items = m_map.getAll<D2Capital>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 5, 5, item.uid);
        }
    }
    {
        QList<D2Village> items = m_map.getAll<D2Village>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 4, 4, item.uid);
        }
    }
    {
        QList<D2Stack> items = m_map.getAll<D2Stack>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 1, 1, item.uid);
        }
    }
    {
        QList<D2LandMark> items = m_map.getAll<D2LandMark>();
        foreach (auto item, items)
        {
            auto lmark = m_data->get<GLmark>(item.baseType);
            addPlanObject(plan, item.posX, item.posY, lmark->cx, lmark->cy, item.uid);
        }
    }
    {
        QList<D2Crystal> items = m_map.getAll<D2Crystal>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 1, 1, item.uid);
        }
    }
    {
        QList<D2Trainer> items = m_map.getAll<D2Trainer>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 3, 3,
                          QPair<int, int>(IDataBlock::Merchant_train, item.uid.second));
        }
    }
    {
        QList<D2Merchant> items = m_map.getAll<D2Merchant>();
        foreach (auto item, items)
        {
            qDebug()<<"############################";
            addPlanObject(plan, item.posX, item.posY, 3, 3,
                          QPair<int, int>(IDataBlock::Merchant_items, item.uid.second));
        }
    }
    {
        QList<D2Mage> items = m_map.getAll<D2Mage>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 3, 3,
                          QPair<int, int>(IDataBlock::Merchant_spells, item.uid.second));
        }
    }
    {
        QList<D2Mercs> items = m_map.getAll<D2Mercs>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 3, 3,
                          QPair<int, int>(IDataBlock::Merchant_mercs, item.uid.second));
        }
    }
    {
        QList<D2Ruin> items = m_map.getAll<D2Ruin>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 3, 3, item.uid);
        }
    }
    {
        QList<D2Bag> items = m_map.getAll<D2Bag>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 1, 1, item.uid);
        }
    }
    {
        QList<D2Road> items = m_map.getAll<D2Road>();
        foreach (auto item, items)
        {
            addPlanObject(plan, item.posX, item.posY, 1, 1, item.uid);
        }
    }
    {
        QList<D2Location> items = m_map.getAll<D2Location>();
        foreach (auto item, items)
        {
            addPlanObject(plan,
                          item.posX,// - item.radius,
                          item.posY,// - item.radius,
                          1,1,//item.radius, item.radius * 2 + 1,
                          item.uid);
        }
    }
    {
        QList<D2Rod> items = m_map.getAll<D2Rod>();
        foreach (auto item, items)
        {
            addPlanObject(plan,
                          item.posX,// - item.radius,
                          item.posY,// - item.radius,
                          1,1,//item.radius, item.radius * 2 + 1,
                          item.uid);
        }
    }
    {
        QList<D2Tomb> items = m_map.getAll<D2Tomb>();
        foreach (auto item, items)
        {
            addPlanObject(plan,
                          item.posX,// - item.radius,
                          item.posY,// - item.radius,
                          1,1,//item.radius, item.radius * 2 + 1,
                          item.uid);
        }
    }
    QStringList newElements;
    foreach (auto & elem, plan.elements) {
        newElements << QString("%1_%2_%3").arg(elem.posX).arg(elem.posY).arg(elem.id.second);
    }
    std::sort(newElements.begin(), newElements.end());

    if (oldElements != newElements && false)
    {
        {
            QFile outFile("old.txt");
            outFile.open(QIODevice::WriteOnly | QIODevice::Append);
            QTextStream ts(&outFile);
            foreach (QString str, oldElements) {
                ts << str << "\n";
            }
            outFile.close();
        }
        {
            QFile outFile("new.txt");
            outFile.open(QIODevice::WriteOnly | QIODevice::Append);
            QTextStream ts(&outFile);
            foreach (QString str, newElements) {
                ts << str << "\n";
            }
            outFile.close();
        }
    }
    m_map.replace(plan);
}

bool D2MapEditor::addPlanObject(D2Plan &plan, int x, int y, int w, int h, const QPair<int, int> &uid)
{
    for (int i = 0; i < w; ++i)
    {
        for (int k = 0; k < h; k++)
        {
            D2Plan::PlanElement element;
            element.id = uid;
            element.posX =  x + k;
            element.posY = y + i;
            plan.elements.append(element);
        }
    }
    return true;
}

const MapGrid &D2MapEditor::grid() const
{
    return m_grid;
}

void D2MapEditor::setGrid(const MapGrid &newGrid)
{
    m_grid = newGrid;
}

void D2MapEditor::updateInfo()
{
    qDebug()<<"updateInfo" <<m_grid.cells.count();
    D2ScenarioInfo info = map().get<D2ScenarioInfo>(0);
    info.mapSize = m_grid.cells.count();
    info.qtyCities = m_map.getAll<D2Village>().count();
    m_map.replace(info);
    m_map.header.author = info.creator;
    m_map.header.name = info.name;
    m_map.header.description = info.desc;
    m_map.header.mapSize = info.mapSize;
}

const IdsHelper &D2MapEditor::idsHelper() const
{
    return m_idsHelper;
}

bool D2MapEditor::removeObject(const QPair<int, int> & uid)
{
    QSharedPointer<IDataBlock> block = m_map.blockById(uid);
    if (block->blockType() == D2Item::type())
    {
        D2Item item = *(static_cast<D2Item*>(block.data()));
        return removeItem(item);
    }
    m_map.remove(uid);
    return true;
}

void D2MapEditor::removeObjects(const QList<QPair<int, int>> &ids)
{
    foreach (const auto & id, ids)
        removeObject(id);
}
