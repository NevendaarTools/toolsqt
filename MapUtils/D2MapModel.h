#ifndef D2MAPMODEL_H
#define D2MAPMODEL_H

#include <QFile>
#include <QDebug>
#include "DataBlock.h"

class D2MapModel
{

public:
    D2MapModel();
    void addDataBlock(IDataBlock * block);
    template <class classT>
    void addBlock(const classT& item)
    {
        addDataBlock(new classT(item));
    }

    bool open(const QString& path);
    bool read(const QByteArray &array);
    bool save(const QString& path);

    template <class classT>
    classT get(int id) const{ return get<classT>(QPair<int, int>(classT::intType(), id));}

    template <class classT>
    classT get(const QPair<int, int> & id) const
    {
        QSharedPointer<IDataBlock> block = blockById(id);
        if (block.isNull())
            return classT();
        return classT(*(static_cast<classT*>(block.data())));
    }
    template <class classT>
    QList<classT> getAll() const
    {
        QList<classT> result;
        foreach(const QSharedPointer<IDataBlock> block, m_blocks)
        {
            if (block->blockType() == classT::type())
            {
                result << classT(*(static_cast<classT*>(block.data())));
            }
        }

        return result;
    }

    QSharedPointer<IDataBlock> blockById(const QPair<int, int> & id) const
    {
        foreach(const QSharedPointer<IDataBlock> block, m_blocks)
        {
            if (block->blockId() == id)
                return block;
        }
        return QSharedPointer<IDataBlock>();
    }
    template <class classT>
    void replace(const classT& item)
    {
        for(int i = 0; i < m_blocks.count(); ++i)
        {
            if (m_blocks[i]->blockId() == item.blockId())
            {
                m_blocks[i] = QSharedPointer<classT>(new classT(item));
                return;
            }
        }
        addBlock<classT>(item);
    }

    template <class classT>
    void removeAll()
    {
        for (int i = m_blocks.count() - 1; i >= 0; --i)
        {
            if (m_blocks[i]->blockType() == classT::type())
            {
                m_blocks.removeAt(i);
            }
        }
    }

    void remove(const QPair<int, int> & id)
    {
        for(int i = 0; i < m_blocks.count(); ++i)
        {
            if (m_blocks[i]->blockId() == id)
            {
                m_blocks.removeAt(i);
                return;
            }
        }
    }

    bool hasBlock(const QPair<int, int> & id)
    {
        for(int i = 0; i < m_blocks.count(); ++i)
        {
            if (m_blocks[i]->blockId() == id)
            {
                return true;
            }
        }
        return false;
    }

    MapHeaderBlock header;

private:
    QHash<QString, QSharedPointer<IDataBlockFactory> > m_factoryBinding;
    QSharedPointer<IDataBlockFactory> m_defaultFactory;

    QSharedPointer<IDataBlockFactory> factoryByHeader(const QString& header);
    QSharedPointer<IDataBlockFactory> factoryByHeader(const QByteArray& header);

    void readBlock(const QByteArray& data, int & index);
    void registerFactory(IDataBlockFactory * factory);
//
    QList<QSharedPointer<IDataBlock> > m_blocks;
};

#endif // D2MAPMODEL_H
