#ifndef DATABLOCK_H
#define DATABLOCK_H
#include <QString>
#include <QVariant>
#include <QBitArray>
#include <QDebug>
#include "../ResourceModel/ByteUtills.h"

class MapHeaderBlock;

class IDataBlock
{
public:
    enum Type
    {
        Invalid = -1,
        Stack = 1,
        Fort,
        Fort_capital,
        Fort_village,
        LandMark,
        Mountain,
        Crystal,
        Ruin,
        Treasure,
        Merchant,
        Merchant_train,
        Merchant_mercs,
        Merchant_items,
        Merchant_spells,
        Location,
        Event,
        Unit,
        Player,
        Diplomacy,
        Item,
        Map,
        MapBlock,
        Fog,
        Plan,
        PlayerBuildings,
        PlayerSpells,
        QuestLog,
        Road,
        ScenarioInfo,
        Variables,
        SpellCast,
        SpellEffects,
        StackDestroed,
        StackTemplate,
        SubRace,
        TalismanCharges,
        TurnSummary,
        Rod,
        Tomb,

        COUNT
    };
    virtual ~IDataBlock(){}
    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header) = 0;
    virtual QByteArray data(const MapHeaderBlock& header) const = 0;
    virtual QString blockType() const  = 0;
    virtual QPair<int, int> blockId() const{return uid;}
    QPair<int, int> uid;

    static int getIntId(const QString & id)
    {
        if (id == "000000")
            return -1;
        QString number = id.right(4);
        bool ok;
        return number.toInt(&ok, 16);
    }
    QString stringId() const
    {
        return QString::number(uid.first, 16).rightJustified(4, '0').toLower() + ":"+
               QString::number(uid.second, 16).rightJustified(4, '0').toLower();
    }
};

class TypeHolder
{
public:
    static TypeHolder* instance() {
        static TypeHolder inst;
        return &inst;
    }

    template <class classT>
    bool registerType()
    {
        for (const auto& item : instance()->m_binding)
        {
            if (item.first == classT::shortType() && item.second == classT::intType())
            {
                return false;
            }
        }
        qDebug() << "Type" << classT::shortType() << " registered!";
        instance()->m_binding.append(QPair<QString, int>(classT::shortType(), classT::intType()));
        return true;
    }

    static IDataBlock::Type TypeByShort(const QString & shortName)
    {
        foreach(const auto & item, instance()->m_binding)
        {
            if (item.first == shortName)
                return (IDataBlock::Type)item.second;
        }
        return IDataBlock::Invalid;
    }
    static QString ShortByType(IDataBlock::Type type)
    {
        for (const auto& item : instance()->m_binding)
        {
            if (static_cast<IDataBlock::Type>(item.second) == type)
                return item.first;
        }
        return QString();
    }
    static QPair<int, int> idFromString(const QString & id)
    {
        QPair<int, int> result;
        result.first = TypeByShort(id.left(2));
        result.second = IDataBlock::getIntId(id);
        return result;
    }
private:
    QList<QPair<QString, int>> m_binding;
};
#define REGISTER_TYPE(classT) static bool classT##_registered = TypeHolder::instance()->registerType<classT>();

static QPair<int, int> readUid(const QByteArray& newData, int & startIndex, const QString & tag)
{
    QString result = readDefaultString(newData, startIndex, tag);
    QPair<int, int> uid{-1, -1};
    if (result == "000000")
        uid.second = -1;
    else
    {
        QString number = result.right(4);
        bool ok;
        uid.second = number.toInt(&ok, 16);
    }

    uid.first = TypeHolder::TypeByShort(result.left(2));
    return uid;
}

class IDataBlockFactory
{
public:
    virtual ~IDataBlockFactory(){}
    virtual IDataBlock * read(const QByteArray& data, int & index, const MapHeaderBlock& header) const = 0;
    virtual QString type() const  = 0;
};

template <class classT>
class Factory : public IDataBlockFactory
{
public:
    virtual IDataBlock * read(const QByteArray& data, int & index, const MapHeaderBlock& header) const
    {
        classT * block = new classT();
        block->read(data, index, header);
        return block;
    }
    virtual QString type() const
    {
        return classT::type();
    }
};

class TagDataBlock : public IDataBlock
{
private:
    QString m_type;
    QString m_objId;
    QByteArray m_data;
public:
    virtual void read(const QByteArray& data, int & index, const MapHeaderBlock& header)
    {
        Q_UNUSED(header);
        static const QByteArray typeStart = QString(".?").toUtf8();
        static const QByteArray typeEnd = QString("@@\0").toUtf8();
        static const QByteArray dataEnd = QString("ENDOBJECT\0").toUtf8();

        int startTypeIndex = data.indexOf(typeStart, index);
        int endTypeIndex = data.indexOf(typeEnd, startTypeIndex);
        m_type = QString::fromUtf8(data.mid(startTypeIndex, endTypeIndex - startTypeIndex));
        m_objId = QString::fromUtf8(data.mid(endTypeIndex + 13 + 4, 10 - 4));//@@\0OBJ_ID\b\0\0\0
        int dataEndIndex = data.indexOf(dataEnd, endTypeIndex + 23);
        m_data = data.mid(index, dataEndIndex + 10 - index);
        index += m_data.size();
    }
    virtual QByteArray data(const MapHeaderBlock& header) const
    {
        Q_UNUSED(header);
        return m_data;
    }
    virtual QString blockType() const override
    {
        return m_type;
    }

    static QString type()
    {
        return "";
    }
};

class ByteBuffer
{
private:
    QList<QByteArray> m_dataList;
    int m_dataSize = 0;
public:
    void write(QByteArray data)
    {
        m_dataList.append(data);
        m_dataSize += data.length();
    }

    void writeBlockHeader(const QString& type, int code)
    {
        QByteArray result;
        result += stringToByteArray("WHAT");
        result += intToByteArray(code, 4);
        result += stringToByteArray(".?AVC" + type + "@@");
        result += QByteArray (1, 0);

        write(result);
    }

    void writeString(const QString& name, const QString& value)
    {
        write(stringToByteArray(name) +
              intToByteArray(value.length() + 1, 4) +
              stringToByteArray(value) + QByteArray (1, 0));
    }

    void writeBegin()
    {
        write(stringToByteArray("BEGOBJECT") + QByteArray (1, 0));
    }

    void writeEnd()
    {
        write(stringToByteArray("ENDOBJECT") + QByteArray (1, 0));
    }

    void writeDefaultString(const QString& name, const QString&  version, const QString&  value)
    {
        if (value == "000000" || value == "G000000000")
        {
            write(stringToByteArray(name ) + make_QByteArray(myarr
                 {'\v', 0, 0, 0, 'G','0','0','0','0','0','0','0','0','0', 0}));
        }
        else
        {
            QByteArray result = stringToByteArray(name);
            result += make_QByteArray(myarr {'\v', 0, 0, 0});
            result += stringToByteArray(version + value);
            result +=  QByteArray (1, 0);
            write(result);
        }
    }

    void writeDefaultString(const QString& name, const QString&  version, IDataBlock::Type type, int id)
    {
        if (id == -1 || type == 0)
            writeDefaultString(name, "000000");
        else
            writeDefaultString(name, version,
                               TypeHolder::ShortByType(type) +
                               QString::number(id, 16).rightJustified(4, '0').toLower());
    }

    void writeDefaultString(const QString& name, const QString&  version, const QPair<int, int> uid)
    {
        writeDefaultString(name, version, (IDataBlock::Type)uid.first, uid.second);
    }

    void writeDefaultString(const QString& name, const QString&  value)
    {
        if (value == "000000" || value == "G000000000")
        {
            write(stringToByteArray(name ) + make_QByteArray(myarr
                 {'\v', 0, 0, 0, 'G','0','0','0','0','0','0','0','0','0', 0}));
        }
        else
        {
            QByteArray result = stringToByteArray(name);
            result += make_QByteArray(myarr {'\v', 0, 0, 0});
            result += stringToByteArray(value);
            result +=  QByteArray (1, 0);
            write(result);
        }
    }

    void writeRepitableData(const char& value, int count)
    {
        QByteArray result(count, value);
        write(result);
    }

    void writeSimpleString(const QString& value)
    {
        write(stringToByteArray(value));
    }

    void writeBool(const QString& name, bool value)
    {
        write(stringToByteArray(name) + intToByteArray(value?1:0, 1));
    }

    void writeDefaultInt(const QString& name, int value)
    {
        write(stringToByteArray(name) + intToByteArray(value, 4));
    }

    void writeInt(const QString& name, int value, int size)
    {
        write(stringToByteArray(name) + intToByteArray(value, size));
    }

    void writeInt(int value, int size, bool bigEndian = false)
    {
        QByteArray result = intToByteArray(value, size);
        if (bigEndian)
            std::reverse(result.begin(), result.end());
        write(result);
    }

    QByteArray GetData()
    {
        QByteArray result;
        foreach(QByteArray data, m_dataList)
        {
            result += data;
        }
        return result;
    }
};

class MapHeaderBlock
{
public:
    QString mapType = "D2EESFISIG";//"MidFile" for old maps
    QString version = "S143";
    QString description = "";
    QString author = "";
    QString name = "my test";
    int offset = 2760;
    bool strategyFirst = false;
    int mapSize = 48;
    int unknown1 = 1;
    int unknown2 = 0;
    int unknown3 = 0;

    QString compaign = "C000CC0001";
    QByteArray _unknownBits;
    QString playerName = "Гоудфрой";
    QByteArray _unknownData;
    int _unknownInt1 = 190;
    QByteArray _unknownPadding;
    QByteArray _unknownData2;
    QByteArray _playersData;
    QByteArray _midgardData;

    void read(const QByteArray& data, int & index)
    {
        static const QByteArray typeEnd = QString("WHAT").toUtf8();

        int endIndex = data.indexOf(typeEnd, index);

        mapType = readStringWithTerm(data, index);
        if (mapType == "D2EESFISIG")
        {
            index = 16;
            offset = readInt(data, index, 4);
            index = 43;
            description = readStringWithTerm(data, index);
            index = 43 + 256;
            author = readStringWithTerm(data, index);
            index = 43 + 256 + 21;
            strategyFirst = readBool (data, index);
            name = readString(data, index, 64).trimmed();
            //index += 64;
            index += 192;
            mapSize = readInt(data, index, 4);
            unknown1 = readInt(data, index, 4);
            unknown2 = readInt(data, index, 4);
            unknown3 = readInt(data, index, 4);
            compaign = readString(data, index, 10);
            index++;//\0
            _unknownBits = data.mid(index, 5);
            index += 5;
            playerName = readString(data, index, 16);
            _unknownData = data.mid(index, 1065 - 6);
            index += 1065 - 6;
            _unknownInt1 = readInt(data, index, 4);
            _unknownData2 = data.mid(index, 250 * 4);
            index += 250 * 4;
            int unknownPaddingSize = readInt(data, index, 4);
            _playersData = data.mid(index, offset - 2668 - 8);
            index += offset - 2668 - 8;

            if (unknownPaddingSize > 0)
                _unknownPadding = data.mid(index, unknownPaddingSize);
        }
        else
        {
            index = 16;
            offset = readInt(data, index, 4);
            index = 42;
            description = readStringWithTerm(data, index);
            index = 42 + 256;
            author = readStringWithTerm(data, index);
            index = 42 + 256 + 21;
            strategyFirst = readBool (data, index);
            name = readStringWithTerm(data, index);
            index = 576;
            mapSize = readInt(data, index, 4);
            _midgardData = data.mid(42 + 256 + 22 + 64, endIndex - 14);
        }
        endIndex -=14;
        version = readString(data, endIndex, 4);

        index =  endIndex + 10;
    }
    QByteArray data(int objCount) const
    {
        ByteBuffer buffer;

        if (mapType == "D2EESFISIG")
        {
            buffer.writeSimpleString(mapType);
            buffer.write(QByteArrayLiteral("\0\0"));
            buffer.writeDefaultInt("", -1);
            buffer.writeDefaultInt("", offset);
            buffer.write(QByteArrayLiteral("\1\0\0\0\x23\0\0\0\0\0\0\0"));
            buffer.writeSimpleString(version + "SC0000");
            buffer.write(QByteArrayLiteral("\0"));
            buffer.writeSimpleString(description);
            buffer.writeRepitableData(0, 256 - description.length());
            buffer.writeSimpleString(author);
            buffer.writeRepitableData(0, 21 - author.length());
            buffer.writeBool("", strategyFirst);
            buffer.writeSimpleString(name);
            buffer.writeRepitableData(0, 64 - name.length());
            buffer.writeRepitableData(0, 192);
            buffer.writeDefaultInt("", mapSize);
            buffer.writeDefaultInt("", unknown1);
            buffer.writeDefaultInt("", unknown2);
            buffer.writeDefaultInt("", unknown3);
            buffer.writeSimpleString(compaign);
            buffer.writeRepitableData(0, 1);
            buffer.write(_unknownBits);
            buffer.writeSimpleString(playerName);
            buffer.writeRepitableData(0, 16 - playerName.length());
            buffer.write(_unknownData);
            buffer.writeDefaultInt("", _unknownInt1);
            buffer.write(_unknownData2);
            if (!_unknownPadding.isEmpty())
                buffer.writeDefaultInt("", _unknownPadding.length() / 8);
            else
                buffer.writeDefaultInt("", 0);

            buffer.write(_playersData);
            if (!_unknownPadding.isEmpty())
                buffer.write(_unknownPadding);

            buffer.writeSimpleString(version);
            buffer.writeSimpleString("OB0000");
            buffer.write(intToByteArray(objCount, 4));
            return buffer.GetData();
        }
        buffer.writeSimpleString(mapType);
        buffer.write(QByteArrayLiteral("\0\0\0\0\0\0\0\0\0"));
        buffer.writeDefaultInt("", offset);
        buffer.writeSimpleString(version + "SC0000");
        buffer.writeRepitableData(0, 12);
        buffer.writeSimpleString(description);
        buffer.writeRepitableData(0, 256 - description.length());
        buffer.writeSimpleString(author);
        buffer.writeRepitableData(0, 21 - author.length());
        buffer.writeBool("",  strategyFirst);
        buffer.writeSimpleString(name);
        buffer.writeRepitableData(0, 64 - name.length());

        buffer.write(_midgardData);
        buffer.writeSimpleString(version);
        buffer.writeSimpleString("OB0000");
        buffer.write(intToByteArray(objCount, 4));
        return buffer.GetData();
    }
    virtual QString blockType() const  { return "MapHeader"; }
    virtual QString objId() const{ return "map"; }

    void emptyInit()
    {
        _unknownBits.resize(5);
        _unknownBits.fill('\0');
        _unknownBits[0] = 1;
        _unknownBits[4] = 1;
        _unknownData.resize(1065 - 6);
        _unknownData.fill('\0');
        _unknownData2.resize(1000);
        _unknownData2.fill('\0');
    }
};

#endif // DATABLOCK_H
