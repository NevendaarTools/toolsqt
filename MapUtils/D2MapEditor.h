#ifndef D2MAPEDITOR_H
#define D2MAPEDITOR_H
#include "IdsHelper.h"
#include "../DBFModel/DBFModel.h"
#include "MapGrid.h"

class D2MapEditor
{

public:
    D2MapEditor(QSharedPointer<DBFModel> data);
    bool open(const QString& path);
    bool save(const QString& path);
    void createMap(int size);
    D2Capital addRace(const QString & raceId,
                 int capitalX, int capitalY,
                 Glord::Category lordCategory,
                 const QString &name, const QString & desc,
                 const QString &fortName,
                 const QString &heroName
                 );
    void updateSubraces();

    const D2MapModel map() const;
    void setMap(D2MapModel newMap);
    void clearIds();
    void rename(const QString & name, const QString & author);
    bool removeItem(D2Item item);
    bool removeObject(const QPair<int, int> & uid);
    void removeObjects(const QList<QPair<int, int>> & ids);
    D2Item addItem(const QString & type, int talismanCharges = -1);
    D2Unit addUnit(const QString & type, int level = 1);
    D2Unit addUnit(const QString &type, int level, int uid);

    bool hasVariable(const QString & name);
    bool addVariable(const QString & name, int value);
    bool setVariable(const QString & name, int value);
    int getVariable(const QString & name);
    D2LandMark addLandMark(int x, int y, const QString & name);
    void addMine(int x, int y, int resource);
    void addTrainer(int x, int y, int type, const QString &name, const QString &desc);
    void addMerchant(int x, int y, int type, const QList<D2Merchant::MerchantEntry> &items,
                     const QString &  name = "",
                     const QString &  desc = "");
    D2Bag addBag(int x, int y, int type, int AIpriority, const QList<int> &items);
    void addMage(int x, int y, int type, const QList<QString> & items,
                 const QString &name = "",
                 const QString &desc = "");

    void addMercs(int x, int y, int type, const QList<D2Mercs::MercsUnitEntry> units,
                  const QString &  name = "",
                  const QString &  desc = "");

    void addMountain(D2Mountains::Mountain mountain);

    void addMountain(int x, int y, int w, int h, int image);

    void commitGrid();

    QSharedPointer<DBFModel> getDbfModel() const {return m_data;}
    const IdsHelper &idsHelper() const;
    template <class classT>
    int nextId()
    {
        return m_idsHelper.nextId<classT>(&m_map);
    }
    template <class classT>
    void replace(const classT& item)
    {
        m_map.replace<classT>(item);
    }
    const MapGrid &grid() const;
    void setGrid(const MapGrid &newGrid);
    void updateInfo();
private:
    bool addPlanObject(D2Plan & plan, int x, int y, int w, int h, const QPair<int, int> & uid);
private:
    MapGrid m_grid;

    D2MapModel m_map;
    QSharedPointer<DBFModel> m_data;
    IdsHelper m_idsHelper;
};

#endif // D2MAPEDITOR_H
