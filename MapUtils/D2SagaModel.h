#ifndef D2SAGAMODEL_H
#define D2SAGAMODEL_H
#include <QString>
#include "D2MapModel.h"
#include "../ResourceModel/GameResource.h"

class D2SagaModel
{
public:
    D2SagaModel();
    QString name() const{return m_path;}
    bool load(const QString& path)
    {
        GameResource res;
        if (!res.read2(path))
        {
            return false;
        }
        desc = res.getResources();
        for (int i = 0; i < desc.count(); ++i)
        {
            QByteArray data = desc[i]->getData(path);
            int bufIndex = 0;
            QString type = readString(data, bufIndex, 8);
            qDebug()<<type;
            if ( type.toUpper() == "D2EESFIS")//D2EESFISIG
            {
                D2MapModel model;
                if (model.read(data))
                {
                    _maps << model;
                }
            }
        }
        m_path = path;
    }

    const int mapCount() const{return _maps.count();}
    D2MapModel getMap(int index) const{return _maps[index];}
private:
    QList<D2MapModel> _maps;
    QList<ResourceDesc *> desc;
    QString m_path;
};

#endif // D2SAGAMODEL_H
