#ifndef IDSHELPER_H
#define IDSHELPER_H
#include <QHash>
#include <QBitArray>
#include "DataBlocks/DataBlocks.h"
#include "D2MapModel.h"


class IdsHelper
{
public:
    IdsHelper() {}
    void clear(){m_available.clear();}
    QList<int> getAvailableSideIds(const D2MapModel * map, int count = 65536)
    {
        QBitArray used(count, false);
        auto list1 = map->getAll<D2Mage>();
        auto list2 = map->getAll<D2Merchant>();
        auto list3 = map->getAll<D2Mercs>();
        auto list4 = map->getAll<D2Trainer>();
        foreach (auto item, list1)
        {
            used.setBit(item.uid.second, true);
        }
        foreach (auto item, list2)
        {
            used.setBit(item.uid.second, true);
        }
        foreach (auto item, list3)
        {
            used.setBit(item.uid.second, true);
        }
        foreach (auto item, list4)
        {
            used.setBit(item.uid.second, true);
        }

        QList<int> result;
        for (int i = 0; i < used.count(); i++)
            if (!used[i])
                result.append(i);

        return result;
    }

    QList<int> getAvailableFortIds(const D2MapModel * map, int count = 65536)
    {
        QBitArray used(count, false);
        auto list1 = map->getAll<D2Capital>();
        auto list2 = map->getAll<D2Village>();
        foreach (auto item, list1)
        {
            used.setBit(item.uid.second, true);
        }
        foreach (auto item, list2)
        {
            used.setBit(item.uid.second, true);
        }

        QList<int> result;
        for (int i = 0; i < used.count(); i++)
            if (!used[i])
                result.append(i);

        return result;
    }

    template <class classT>
    QList<int> getAvailableIds(const D2MapModel * map, int count = 65536)
    {
        int type = classT::intType();
        if (type == IDataBlock::Merchant       ||
            type == IDataBlock::Merchant_train ||
            type == IDataBlock::Merchant_mercs ||
            type == IDataBlock::Merchant_items ||
            type == IDataBlock::Merchant_spells)
            return getAvailableSideIds(map, count);
        if (type == IDataBlock::Fort ||
            type == IDataBlock::Fort_village ||
            type == IDataBlock::Fort_capital)
            return getAvailableFortIds(map, count);
        auto list = map->getAll<classT>();
        QBitArray used(count, false);

        foreach (auto item, list)
        {
            used.setBit(item.uid.second, true);
        }

        QList<int> result;
        for (int i = 0; i < used.count(); i++)
            if (!used[i])
                result.append(i);

        return result;
    }

    template <class classT>
    int nextId(const D2MapModel * map)
    {
        int type = classT::intType();
        if (type == IDataBlock::Merchant_train ||
            type == IDataBlock::Merchant_mercs ||
            type == IDataBlock::Merchant_items ||
            type == IDataBlock::Merchant_spells)
            type = IDataBlock::Merchant;

        if (type == IDataBlock::Fort_village ||
            type == IDataBlock::Fort_capital)
            type = IDataBlock::Fort;

        if (!m_available.contains(type))
            m_available.insert(type, QList<int>());
        if (m_available[type].count() == 0)
            m_available[type] = getAvailableIds<classT>(map);
        int result = m_available[type][0];
        m_available[type].removeFirst();
        return result;
    }
private:
    QHash<int, QList<int>> m_available;
};
#endif // IDSHELPER_H
